import React from 'react'

import './FlexSpacer.scss'

export default ({ className, ...rest }) => (
  <span
    className={`flex-spacer${className ? ' ' + className : ''}`}
    {...rest}
  />
)
