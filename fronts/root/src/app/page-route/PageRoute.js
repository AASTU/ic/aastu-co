import React from 'react'
import { Route } from 'react-router-dom'
import * as qs from 'qs'

export default ({ component, account, ...props }) => {
  if (account && !account._id) {
    window.location.replace(
      '/login?' +
        qs.stringify(
          Object.assign(
            { continue: props.path },
            qs.parse(window.location.search)
          )
        )
    )
    return null
  }

  return (
    <Route
      {...props}
      render={routeProps =>
        React.createElement(
          component,
          Object.assign({ account: account }, routeProps, props)
        )
      }
    />
  )
}
