import React, { Component } from 'react'
import Loadable from 'react-loadable'
import { BrowserRouter, Switch } from 'react-router-dom'

import './App.scss'
import Loading from '../common/loading/Loading'
import BigHead from './big-head/BigHead'
import NavBar from './nav-bar/NavBar'
import PageRoute from './page-route/PageRoute'
import LittleFinger from './little-finger/LittleFinger'

const Intro = Loadable({
  loader: () => import('../pages/intro/Intro'),
  loading: Loading
})
const Stats = Loadable({
  loader: () => import('../pages/stats/Stats'),
  loading: Loading
})
const Partners = Loadable({
  loader: () => import('../pages/partners/Partners'),
  loading: Loading
})
const PartnersView = Loadable({
  loader: () => import('../pages/partners-view/PartnersView'),
  loading: Loading
})
const NotFound = Loadable({
  loader: () => import('../pages/not-found/NotFound'),
  loading: Loading
})

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      problem: null,
      isNavOpen: false
    }

    this.openNav = this.openNav.bind(this)
    this.closeNav = this.closeNav.bind(this)
  }

  render() {
    const { isNavOpen, problem } = this.state

    return problem ? (
      <Loading delay problem={problem} />
    ) : (
      <BrowserRouter>
        <div className={'bg'}>
          <BigHead onNavOpen={this.openNav} onLogout={this.logout} />

          <NavBar
            onClose={this.closeNav}
            onNavClose={this.closeNav}
            onLogout={this.logout}
            open={isNavOpen}
          />

          <div
            style={{
              minHeight: window.innerHeight ? window.innerHeight - 140 : 420
            }}
          >
            <Switch>
              <PageRoute exact path={'/'} component={Intro} />
              <PageRoute exact path={'/stats'} component={Stats} />
              <PageRoute exact path={'/partners'} component={Partners} />
              <PageRoute path={'/partners/:_id'} component={PartnersView} />
              <PageRoute component={NotFound} />
            </Switch>
          </div>

          <LittleFinger />
        </div>
      </BrowserRouter>
    )
  }

  openNav() {
    this.setState({ isNavOpen: true })
  }

  closeNav() {
    this.setState({ isNavOpen: false })
  }
}
