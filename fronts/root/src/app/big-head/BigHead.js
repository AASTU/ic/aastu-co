import React, { Component } from 'react'
import FontAwesome from 'react-fontawesome'
import './BigHead.scss'

import Button from '../../common/button/Button'
import Logo from '../../common/logo/Logo'
import MenuDrop from '../../common/menu-drop/MenuDrop'
import MenuItem from '../../common/menu-item/MenuItem'
import Content from '../../common/content/Content'

// todo: no login for this root app
export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showMenu: false
    }

    this.closeMenuDrop = this.closeMenuDrop.bind(this)
  }

  render() {
    const { onNavOpen } = this.props

    return (
      <div className={'header-bg'}>
        <Content className={'header'}>
          <span className={'header-left'}>
            <Button
              className={'menu-btn header-btn header-box-size'}
              onClick={onNavOpen}
            >
              <FontAwesome name={'bars'} />
            </Button>
          </span>

          <Logo className={'logo-size'} to={'/'} />

          <span className={'header-right'}>
            <div className={'header-box-size'}>
              <Button
                className={'menu-btn header-btn header-box-size'}
                onClick={() =>
                  this.setState({ showMenu: !this.state.showMenu })
                }
              >
                <FontAwesome name="ellipsis-v" />
              </Button>
              <MenuDrop
                align={'right'}
                anchorOffset={56}
                onClose={this.closeMenuDrop}
                open={this.state.showMenu}
                size={'L'}
              >
                <MenuItem
                  to={'/data/goto/app/graduate'}
                  onClick={this.closeMenuDrop}
                >
                  Graduate Login
                </MenuItem>
                <MenuItem
                  to={'/data/goto/app/company'}
                  onClick={this.closeMenuDrop}
                >
                  Company Login
                </MenuItem>
                <MenuItem
                  to={'/data/goto/app/department'}
                  onClick={this.closeMenuDrop}
                >
                  Department Login
                </MenuItem>
                <MenuItem
                  to={'/data/goto/app/admin'}
                  onClick={this.closeMenuDrop}
                >
                  Admin Login
                </MenuItem>
              </MenuDrop>
            </div>
          </span>
        </Content>
      </div>
    )
  }

  closeMenuDrop() {
    this.setState({ showMenu: false })
  }
}
