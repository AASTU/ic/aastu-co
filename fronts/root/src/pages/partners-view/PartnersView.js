import React, { Component } from 'react'
import axios from 'axios'
import FontAwesome from 'react-fontawesome'
import './partnersView.scss'
import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Anchor from '../../common/anchor/Anchor'

export default class PartnersView extends Component {
  constructor(props) {
    super(props)

    this.state = {
      company: {},
      company_pic: undefined
    }
  }

  componentDidMount() {
    axios
      .get(`/data/company/${this.props.match.params._id}`)
      .then(res => Promise.resolve(res.data))
      .then(data => {
        if (data.success) {
          this.setState({
            company: data.company
          })
        }
      })
  }

  render() {
    const company = this.state.company
    return (
      <Page>
        <div className={'flex'}>
          <Content
            className={'center inline-block padding-very-big'}
            size={'M'}
          >
            <Block>
              <img
                src={`/data/company/${this.props.match.params._id}/picture`}
                width={'70%'}
                height={'70%'}
                alt=""
              />
            </Block>
            <Content size={'M'} className={'inline-block margin-left-none'}>
              <Block>
                <div className={'font-XL center margin-big'}>
                  {company.name}
                </div>
                <div>
                  <FontAwesome name={'envelope'} />
                  <span className={'padding-normal'}>{company.email}</span>
                </div>
                <div>
                  <FontAwesome name={'phone-square'} />
                  <span className={'padding-normal'}>
                    +{company.phone ? company.phone.code : ''}{' '}
                    {company.phone ? company.phone.number : ''}{' '}
                  </span>
                </div>
                <div>
                  <FontAwesome name={'share-square-o'} />
                  {company.links
                    ? company.links.map((link, i) => (
                        <a className={'padding-normal'} key={i} href={link}>
                          {link}
                        </a>
                      ))
                    : ''}
                </div>
                <div>
                  <FontAwesome name={'map-marker'} />
                  <span className={'padding-normal'}>{company.address}</span>
                </div>
                <div>
                  <FontAwesome name={'info-circle'} />
                  <span className={'padding-normal'}>
                    {company.contactInfo}
                  </span>
                </div>
                <div className={''}>
                  <p className={''}>
                    <div className={'font-L bolder '}>about us </div>
                    {company.description}
                  </p>
                </div>
              </Block>
            </Content>
          </Content>
        </div>
      </Page>
    )
  }
}
