import React, { Component } from 'react'

import Page from '../../common/page/Page'
import Barchart from './Barchart'
import DashBoard from './DashBoard'
import Block from '../../common/block/Block'

import './card.css'

export default class extends Component {
  render() {
    return (
      <Page>
        <DashBoard />
        <Block className={''}>
          <Barchart />
        </Block>
      </Page>
    )
  }
}
