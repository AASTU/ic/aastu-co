import React, { Component } from 'react'

import Content from '../../common/content/Content'
import Block from '../../common/block/Block'

import grad2 from '../../assets/icons/grad2.svg'
import company from '../../assets/icons/users.svg'
import engineer from '../../assets/icons/engineer.svg'

import './card.css'

export default class extends Component {
  render() {
    return (
      <Block className={'flex center '}>
        <Content className={'parent inline-block margin-right-big'}>
          <Content className={'child bg-primary'}>
            <img alt={'grad2'} src={grad2} />
          </Content>
          <Block
            className={
              'float-right card-width card-height padding-top-very-big'
            }
          >
            <div className={'center'}>
              <p>
                over the past six years 25520{' '}
                <span className={'font-L'}>Graduates.</span>
              </p>
            </div>
          </Block>
        </Content>

        <Content className={'parent inline-block margin-right-big'}>
          <Content className={'child bg-yellowish'}>
            <img alt={'Pic'} src={engineer} />
          </Content>
          <Block
            className={
              'float-right card-width card-height padding-top-very-big'
            }
          >
            <div className={'center'}>
              <p>
                out of 25520 15000 graduates are{' '}
                <span className={'font-L'}>Employed.</span>
              </p>
            </div>
          </Block>
        </Content>

        <Content className={'parent inline-block margin-right-big'}>
          <Content className={'child .bg-reddish'}>
            <img alt={'Pic'} src={company} />
          </Content>
          <Block
            className={
              'float-right card-width card-height padding-top-very-big'
            }
          >
            <div className={'center'}>
              <p>
                working with different companies:{' '}
                <span className={'font-L'}>partnership.</span>
              </p>
            </div>
          </Block>
        </Content>
      </Block>
    )
  }
}
