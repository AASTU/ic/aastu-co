import React, { Component } from 'react'
import * as qs from 'qs'
import axios from 'axios'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import {
  Bar,
  BarChart,
  CartesianGrid,
  Cell,
  LabelList,
  Legend,
  Pie,
  PieChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis
} from 'recharts'
import Input from '../../common/input/Input'
import './stats.css'

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#eee']
const COLORS1 = ['#f028FE', '#eaC493', '#FFCC28', '#eee']

export default class Barchart extends Component {
  constructor(props) {
    super(props)

    this.state = {
      departments: [],
      companies: [],
      data: {
        general: [],
        jopStatus: [],
        jopType: [],
        selfEmployed: []
      },
      temp: {},
      selected: {
        department_id: undefined,
        gender: undefined,
        company_id: undefined
      },
      problem: undefined
    }
  }

  componentDidMount() {
    axios
      .get('/data/department/all')
      .then(res => Promise.resolve(res.data))
      .then(data => {
        if (data.success) {
          this.setState({
            departments: this.state.departments.concat(data.departments)
          })
        }
      })
      .catch(e => {
        this.setState({
          problem: e
        })
      })
    axios
      .get('/data/company/all')
      .then(res => Promise.resolve(res.data))
      .then(data => {
        if (data.success) {
          this.setState({
            companies: this.state.companies.concat(data.companies)
          })
        }
      })

    let data = this.state.selected
    this.clean(data)
    this.fetchStats(data)
  }

  handleChange = e => {
    let data = {
      department_id: e.target.value === 'all' ? undefined : e.target.value,
      gender: this.state.selected.gender,
      company_id: this.state.selected.company_id
    }
    this.clean(data)
    this.setState({
      selected: data
    })
    this.fetchStats(data)
  }
  handleChangeOnCompany = e => {
    let data = {
      department_id: this.state.selected.department_id,
      gender: this.state.selected.gender,
      company_id: e.target.value === 'all' ? undefined : e.target.value
    }
    this.clean(data)
    this.setState({
      selected: data
    })
    this.fetchStats(data)
  }
  radioChange = e => {
    let data = {
      department_id: this.state.selected.department_id,
      gender: e.target.value === 'all' ? undefined : e.target.value,
      company_id: this.state.selected.company_id
    }
    this.clean(data)
    this.setState({
      selected: data
    })
    console.log(data)
    this.fetchStats(data)
  }

  render() {
    return (
      <div>
        <Content size={'XL'} className={'center stick'}>
          <Block first last>
            <span className={'margin-normal'}>Department</span>
            <select onChange={this.handleChange}>
              <option value={'all'}>{'ALL'}</option>
              {this.state.departments.map((department, i) => (
                <option key={i} value={department._id}>
                  {department.name}
                </option>
              ))}
            </select>
            <span className={'margin-normal'}>company</span>
            <select onChange={this.handleChangeOnCompany}>
              <option value={'all'}>{'ALL'}</option>
              {this.state.companies.map((company, i) => (
                <option key={i} value={company._id}>
                  {company.name}
                </option>
              ))}
            </select>
            <span className={'padding-normal'}>
              Male
              <Input
                type="radio"
                name={'gender'}
                value={'M'}
                onChange={this.radioChange}
              />
              Female
              <Input
                type="radio"
                name={'gender'}
                value={'F'}
                onChange={this.radioChange}
              />
              Both
              <Input
                type="radio"
                name={'gender'}
                value={'all'}
                onChange={this.radioChange}
              />
            </span>
          </Block>
        </Content>
        <Content size={'XL'} className={'center'}>
          <Block first last>
            <ResponsiveContainer width="100%" height={400}>
              <BarChart
                className={'margin-none'}
                margin={{ top: 0, right: 50, left: 0, bottom: 0 }}
                data={this.state.data.general}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="total" fill="#8884d8" minPointSize={5} />
                <Bar dataKey="learning" fill="#82ca9d" minPointSize={10} />
              </BarChart>
            </ResponsiveContainer>
          </Block>
        </Content>
        <Block first last>
          <Content className={'center middle'} size={'XL'}>
            <div className={'flex'}>
              <Content
                size={'S'}
                className={'inline-block margin-right-normal'}
              >
                <ResponsiveContainer width="100%" height={400}>
                  <PieChart>
                    <Pie
                      data={this.state.data.jopStatus}
                      cx="50%"
                      cy="50%"
                      innerRadius={40}
                      outerRadius={100}
                      label
                      fill="#82ca9d"
                    >
                      <LabelList
                        angle="-45"
                        className={'fg-primary'}
                        dataKey="name"
                        position="insideLeft"
                      />
                      {this.state.data.jopStatus.map((entry, index) => (
                        <Cell fill={COLORS1[index % COLORS1.length]} />
                      ))}
                    </Pie>
                    <Tooltip />
                    <Legend verticalAlign="top" height={36} />
                  </PieChart>
                </ResponsiveContainer>
              </Content>

              <Content size={'S'} className={'inline-block margin-left-normal'}>
                <ResponsiveContainer width="100%" height={400}>
                  <PieChart>
                    <Pie
                      data={this.state.data.jopType}
                      cx="50%"
                      cy="50%"
                      fill="#8884d8"
                      outerRadius={150}
                      label
                    >
                      <LabelList
                        dataKey="name"
                        angle="45"
                        className={'fg-primary'}
                        position="insideLeft"
                      />
                      {this.state.data.jopType.map((entry, index) => (
                        <Cell fill={COLORS[index % COLORS.length]} />
                      ))}
                    </Pie>
                    <Tooltip />
                    <Legend verticalAlign="top" height={36} />
                  </PieChart>
                </ResponsiveContainer>
              </Content>
            </div>
          </Content>
        </Block>
        <Block>
          <Content className={'center middle'} size={'XL'}>
            <Block first last>
              <ResponsiveContainer width={'100%'} height={200}>
                <BarChart
                  data={this.state.data.selfEmployed}
                  margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
                  layout={'vertical'}
                >
                  <XAxis type="number" />
                  <YAxis type={'category'} hide />
                  <Tooltip />
                  <Legend />
                  <Bar dataKey="byField" stackId="a" fill="#0088FE">
                    <LabelList dataKey="byField" position="top" />
                  </Bar>
                  <Bar dataKey="other" stackId="a" fill="#82ca9d">
                    <LabelList dataKey="other" position="top" />
                  </Bar>
                </BarChart>
              </ResponsiveContainer>
            </Block>
          </Content>
        </Block>
      </div>
    )
  }
  fetchStats = data => {
    axios
      .get(`/data/stats?${qs.stringify(data)}`)
      .then(res => Promise.resolve(res.data))
      .then(data => {
        if (data.success) {
          this.setState({
            temp: data.stats
          })
          this.initialReformat()
        }
      })
      .catch(e => {
        this.setState({
          problem: e
        })
      })
  }

  initialReformat = () => {
    let temp = this.state.temp
    let selfEmployed = {
      name: 'Self-Employed',
      total: temp.selfEmployed.total,
      learning: temp.selfEmployed.learning
    }
    let companyEmployed = {
      name: 'Company-Employed',
      total: temp.companyEmployed.total,
      learning: temp.companyEmployed.learning
    }
    let unemployed = {
      name: 'Unemployed',
      total: temp.unemployed.total,
      learning: temp.unemployed.learning
    }
    let tempStat = []
    tempStat.push(selfEmployed, companyEmployed, unemployed)

    //company
    let contract = {
      name: 'Contract',
      value: temp.companyEmployed.jobType.contract
    }
    let permanent = {
      name: 'Permanent',
      value: temp.companyEmployed.jobType.permanent
    }
    let intern = {
      name: 'Intern',
      value: temp.companyEmployed.jobType.intern
    }

    let typeTemp = []
    typeTemp.push(contract, permanent, intern)

    //jopStatus
    let fullTime = {
      name: 'Full time',
      value: temp.companyEmployed.jobStatus.fullTime
    }
    let partTime = {
      name: 'Part time',
      value: temp.companyEmployed.jobStatus.partTime
    }
    let perDime = {
      name: 'PerDiem',
      value: temp.companyEmployed.jobStatus.perDime
    }
    let statusTemp = []
    statusTemp.push(fullTime, partTime, perDime)

    let self = {
      name: 'selfEmployed',
      byField: temp.selfEmployed.byField,
      other: temp.selfEmployed.total - temp.selfEmployed.byField
    }
    let byField = []
    byField.push(self)

    this.setState({
      data: {
        general: tempStat,
        jopStatus: statusTemp,
        jopType: typeTemp,
        selfEmployed: byField
      }
    })
  }

  clean = obj => {
    for (let o in obj) {
      if (obj[o] === null || obj[o] === undefined) {
        delete obj[o]
      }
    }
  }
}
