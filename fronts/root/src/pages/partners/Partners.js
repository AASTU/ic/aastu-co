import React, { Component } from 'react'
import axios from 'axios'

import './Partners.scss'
import Page from '../../common/page/Page'
import Block from '../../common/block/Block'
import Content from '../../common/card/Card'
import Anchor from '../../common/anchor/Anchor'

export default class extends Component {
  state = {
    companies: []
  }

  componentDidMount() {
    axios
      .get('/data/company/all')
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success) return console.error(data.problem)
        this.setState({ companies: data.companies || [] })
      })
      .catch(console.error)
  }

  render() {
    const { companies } = this.state

    return (
      <Page top={'adaptive'}>
        <Content size={'XXL'}>
          <Block first last>
            <h2 className={'center'}>Our Partner Companies</h2>
          </Block>

          {!companies.length ? (
            <Block last className={'center'}>
              We haven't partnered with any company yet.
            </Block>
          ) : (
            <Block last className={'center'}>
              {companies.map((company, i) => this.companyRound(i, company))}
            </Block>
          )}
        </Content>
      </Page>
    )
  }

  companyRound(i, company) {
    return (
      <Anchor to={`/partners/${company._id}`}>
        <img
          key={i}
          alt={company.name}
          className={'company-logo'}
          src={`/data/company/${company._id}/picture`}
          title={company.name}
        />
      </Anchor>
    )
  }
}
