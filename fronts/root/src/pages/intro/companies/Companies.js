import React, { Component } from 'react'
import axios from 'axios'

import './Companies.scss'
import Content from '../../../common/content/Content'
import Block from '../../../common/block/Block'
import Anchor from '../../../common/anchor/Anchor'

export default class extends Component {
  constructor() {
    super()
    this.state = {
      companies: []
    }
  }

  componentDidMount() {
    this.fetchCompanies()
  }

  render() {
    let companies = this.state.companies
    let companyList = companies.map((company, index) => (
      <Content className={'company-round margin-big'}>
        <Anchor to={`/partners/${company._id}`}>
          <img
            alt={company.name}
            src={`/data/company/${company._id}/picture`}
            className={'company-logo'}
            title={company.name}
          />
        </Anchor>
      </Content>
    ))
    console.log(companies)
    return (
      <Content transparent size={'XXL'}>
        <Block first last className={'center'}>
          {companyList}
        </Block>
      </Content>
    )
  }

  fetchCompanies = () => {
    axios.get('/data/company/list/10/start/0').then(res => {
      this.setState({
        companies: this.state.companies.concat(res.data.companies)
      })
    })
  }
}
