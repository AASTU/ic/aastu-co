import React, { Component } from 'react'

import '../../../node_modules/react-image-gallery/styles/css/image-gallery.css'
import './Intro.scss'
import graph from '../../assets/images/graph.svg'
import graduation from '../../assets/images/graduation.svg'
import aastuLogo from '../../assets/images/aastuLogo.png'

import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Anchor from '../../common/anchor/Anchor'
import Button from '../../common/button/Button'

import IntroSlide from './intro-slide/IntroSlide'
import Companies from './companies/Companies'

export default class extends Component {
  render() {
    return (
      <div>
        <IntroSlide />

        <Block first>
          <br />
          <br />
          <h1 className={'center bold fg-primary'}>AASTU Co.</h1>
        </Block>
        <Block>
          <p className={'center italic light'}>
            Creating partnership between AASTU and important companies.
          </p>
          <p className={'center italic light'}>
            Job opportunities for graduates at partner companies.
          </p>
          <p className={'center italic light'}>
            Real-time alumni statistics for everyone.
          </p>
        </Block>

        <Block first last className={'center'}>
          <Anchor
            style={{ textDecoration: 'none' }}
            className={'top margin-normal'}
            to={'/stats'}
          >
            <Content size={'XS'}>
              <Block first className={'center'}>
                <img alt={'Statistics'} src={graph} className={'icon-effect'} />
              </Block>
              <Block last>
                Statistics of AASTU alumni, including work and education
                statuses with many filters.
              </Block>
            </Content>
          </Anchor>

          <Anchor
            style={{ textDecoration: 'none' }}
            className={'top margin-normal'}
            href={'http://www.aastu.edu.et/'}
            target={'_blank'}
          >
            <Content size={'XS'}>
              <Block first className={'center'}>
                <img
                  alt={'Addis Ababa Science and Technology University'}
                  src={aastuLogo}
                  className={'icon-effect'}
                />
              </Block>
              <Block last className={'center'}>
                Visit Addis Ababa Science and Technology University's (AASTU's)
                official website.
              </Block>
            </Content>
          </Anchor>

          <Anchor
            style={{ textDecoration: 'none' }}
            className={'top margin-normal'}
            to={'/partners'}
          >
            {/* ...todo */}
            <Content size={'XS'}>
              <Block first className={'center'}>
                <img
                  style={{ paddingLeft: '15px' }}
                  width={'170px'}
                  alt={'Graph'}
                  src={graduation}
                  className={'icon-effect'}
                />
              </Block>
              <Block last className={'center'}>
                Designed to partner graduates with companies, and provide alumni
                stats for everyone.
              </Block>
            </Content>
          </Anchor>
        </Block>

        <Block className={'parallax'} />

        <Block
          className={'padding-very-big font-XXL center bg-primary fg-whitish'}
        >
          Who are working with us?
        </Block>

        <Companies />

        <Block className={'center'}>
          <Anchor to={'/partners'}>
            <Button>More partners</Button>
          </Anchor>
        </Block>
      </div>
    )
  }
}
