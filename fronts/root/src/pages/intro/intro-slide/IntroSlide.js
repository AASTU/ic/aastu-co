import React, { Component } from 'react'
import ImageGallery from 'react-image-gallery'

import slide1 from '../../../assets/images/slides/1.jpeg'
import slide2 from '../../../assets/images/slides/2.jpeg'
import slide3 from '../../../assets/images/slides/3.jpeg'
import slide4 from '../../../assets/images/slides/4.jpeg'
import slide5 from '../../../assets/images/slides/5.jpeg'
import Content from '../../../common/content/Content'

const images = [
  { original: slide1 },
  { original: slide2 },
  { original: slide3 },
  { original: slide4 },
  { original: slide5 }
]

export default class extends Component {
  render() {
    return (
      <Content style={{ borderRadius: 0 }}>
        <ImageGallery
          showThumbnails={false}
          autoPlay={true}
          showFullscreenButton={false}
          showPlayButton={false}
          showBullets={false}
          showNav={false}
          items={images}
        />
      </Content>
    )
  }
}
