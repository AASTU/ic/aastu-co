import React, { Component } from 'react'
import axios from 'axios'

import Page from '../../common/page/Page'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Input from '../../common/input/Input'
import TextArea from '../../common/text-area/TextArea'
import Button from '../../common/button/Button'

// todo: fix targetDepartments selection
export default class extends Component {
  state = {
    problem: undefined,
    view: undefined,
    announcement: {}
  }

  componentDidMount() {
    this.fetchState()
  }

  render() {
    const { problem, view } = this.state

    return (
      <Page>
        {(!problem || (problem && problem.handleInView)) &&
        typeof view === 'function' ? (
          view()
        ) : problem ? (
          <Warning problem={problem} />
        ) : (
          <Loading delay />
        )}
      </Page>
    )
  }

  inputView = () => {
    const { problem, announcement } = this.state

    return (
      <Content size={'M'}>
        {problem ? (
          <Block first>
            <Warning
              bomb={() => this.setState({ problem: undefined })}
              problem={problem}
              shy
            />
          </Block>
        ) : null}

        <Block first>
          <h2>Edit This Announcement</h2>
        </Block>

        <form
          method={'PUT'}
          action={`/data/announcement/${announcement._id}/edit`}
          onSubmit={this.onSubmit}
        >
          <Block first className={'font-XS bold uppercase'}>
            Basic Information:
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'title'}
              placeholder={'Announcement Title *'}
              defaultValue={announcement.title}
            />
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'description'}
              placeholder={'Announcement Description *'}
              defaultValue={announcement.description}
            />
          </Block>

          <Block first className={'font-XS bold uppercase'}>
            Optional, Event Information:
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'time'}
              placeholder={'Event Date and Time'}
              type={'datetime-local'}
              defaultValue={`${new Date(announcement.time).getFullYear()}-${(
                '0' +
                (new Date(announcement.time).getMonth() + 1)
              ).slice(-2)}-${(
                '0' + new Date(announcement.time).getDate()
              ).slice(-2)}`}
            />
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'location'}
              placeholder={'Event Location'}
              defaultValue={announcement.location}
              type={'text'}
            />
          </Block>

          <Block last className={'right'}>
            <Button to={'/'}>Cancel</Button>
            <span className={'padding-left-small'} />
            <Button type={'submit'}>Save Changes</Button>
          </Block>
        </form>
      </Content>
    )
  }

  onTargetChange = e => {
    this.setState({ target: e.target.checked })
  }

  fetchState = () => {
    axios.get(`/data/announcement/${this.props.match.params._id}`).then(res => {
      this.setState({
        view: this.inputView,
        announcement: res.data.announcement
      })
    })
  }

  onSubmit = e => {
    e.preventDefault()

    const { title, description, time, location } = e.target

    this.setState({ view: null })
    axios
      .put(
        `/data/announcement/${this.props.match.params._id}`,
        {
          title: title.value,
          description: description.value,
          time: time.value,
          location: location.value
        },
        { withCredentials: true }
      )
      .then(resp => resp.data)
      .then(data => {
        if (!data.success)
          return this.setState({
            problem: this.setState({ problem: data.problem })
          })

        this.setState({
          problem: null,
          announcement: data.announcement,
          view: this.inputView
        })
        this.props.history.push('/announcements')
      })
      .catch(e => this.setState({ problem: e }))
  }
}
