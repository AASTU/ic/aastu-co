import React from 'react'
import axios from 'axios'

import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Page from '../../common/page/Page'
import Anchor from '../../common/anchor/Anchor'
import Card from '../../common/card/Card'
import Button from '../../common/button/Button'

export default class extends React.Component {
  constructor() {
    super()
    this.state = {
      vacancies: [],
      problem: undefined
    }

    this.vacancyList = this.vacancyList.bind(this)
  }
  componentDidMount() {
    this.fetchVacancies()
  }

  render() {
    let { vacancies } = this.state

    return !vacancies ? null : (
      <Page className={'center'}>
        {vacancies.map((vac, i) => this.vacancyList(vac))}
      </Page>
    )
  }

  vacancyList(vac) {
    return (
      <Card
        size={'S'}
        className={'inline-block margin-normal'}
        title={
          <h3 className={'fg-primary'}>
            {vac.jobTitle.substring(0, 20).toUpperCase()}
          </h3>
        }
        subtitle={[
          <h4>Location @ {vac.jobLocation}</h4>,
          <h4>Salary {vac.salary}</h4>,
          <h4>Quantity {vac.quantity}</h4>
        ]}
        actions={
          <Button to={`/vacancy/${vac._id}`} primary>
            More
          </Button>
        }
      >
        <p className={'fg-blackish'}>{vac.description.substring(0, 100)}...</p>
        <h3 className={'fg-blackish'}>
          Applicants <span className={'fg-primary'}>&#9733;</span>
          {vac.applicants.length !== 0 ? vac.applicants.length : ''}
        </h3>
      </Card>
    )
  }

  fetchVacancies = () => {
    axios
      .get('data/vacancy/list/10/start/0')
      .then(res => {
        if (res.data.success) {
          this.setState({
            vacancies: this.state.vacancies.concat(res.data.vacancies)
          })
        } else {
          return this.setState({
            problem: Object.assign(res.data.problem, { handleInView: true })
          })
        }
      })
      .catch(e => {
        this.setState({
          problem: e
        })
      })
  }
}
