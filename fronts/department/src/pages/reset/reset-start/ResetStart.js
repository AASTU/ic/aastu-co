import React, { Component } from 'react'
import axios from 'axios'
import FontAwesome from 'react-fontawesome'

import Warning from '../../../common/warning/Warning'
import Loading from '../../../common/loading/Loading'
import Block from '../../../common/block/Block'
import Input from '../../../common/input/Input'
import Button from '../../../common/button/Button'

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      problem: undefined,
      view: undefined
    }

    this.inputView = this.inputView.bind(this)
    this.sentView = this.sentView.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount() {
    this.setState({ view: this.inputView })
  }

  render() {
    const { problem, view } = this.state

    return (!problem || (problem && problem.handleInView)) &&
      typeof view === 'function' ? (
      view()
    ) : problem ? (
      <Warning problem={problem} />
    ) : (
      <Loading delay />
    )
  }

  inputView() {
    return (
      <form
        method={'POST'}
        action={'/data/auth/reset/start'}
        onSubmit={this.onSubmit}
      >
        {this.state.problem ? (
          <Block first>
            <Warning
              bomb
              problem={this.state.problem}
              shy={() => this.setState({ problem: undefined })}
            />
          </Block>
        ) : null}

        <Block first>
          <div className={'font-L bold margin-bottom-normal'}>
            Reset Your Password?
          </div>
          <div className={'font-S fg-blackish'}>
            If you forgot your account's password, we can send an email to your
            account's verified email address to allow you to set a new password.
          </div>
        </Block>

        <Block>
          <Input
            className={'full-width'}
            name={'email'}
            placeholder={'Email Address'}
            type={'email'}
          />
        </Block>

        <Block last className={'right'}>
          <Button type={'submit'}>Send Email</Button>
        </Block>
      </form>
    )
  }

  sentView() {
    return (
      <div>
        <Block first className={'font-L bold'}>
          <FontAwesome name={'check'} /> Verification Sent
        </Block>
        <Block last className={'font-L light fg-blackish'}>
          We've sent you an email {this.email ? `at ${this.email}` : null}.
          Please check your inbox and click on the link we've sent you, to get
          back here and finish the Password Reset process.
        </Block>
      </div>
    )
  }

  onSubmit(e) {
    e.preventDefault()

    const { action, email } = e.target

    if (!email.value)
      return this.setState({
        problem: {
          handleInView: true,
          message: 'Email field is required.'
        }
      })

    this.setState({ view: null })

    axios
      .post(action, {
        email: email.value
      })
      .then(resp => resp.data)
      .then(data => {
        if (!data.success)
          this.setState({
            handleInView: true,
            problem: data.problem
          })
        else {
          this.email = email.value
          this.setState({ view: this.sentView })
        }
      })
      .catch(err =>
        this.setState({
          handleInView: true,
          problem: err
        })
      )
  }
}
