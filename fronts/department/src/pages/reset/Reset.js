import React from 'react'
import * as qs from 'qs'

import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import ResetStart from './reset-start/ResetStart'
import ResetFinish from './reset-finish/ResetFinish'

export default props => {
  const { email, serial, view } = qs.parse(window.location.search, {
    ignoreQueryPrefix: true
  })

  return (
    <Page top={'adaptive'}>
      <Content size={'S'}>
        {view === 'finish' && email && serial ? (
          <ResetFinish {...props} email={email} serial={serial} />
        ) : (
          <ResetStart {...props} />
        )}
      </Content>
    </Page>
  )
}
