import React, { Component } from 'react'
import axios from 'axios'

import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Button from '../../common/button/Button'
import AccountPic from '../../common/account-pic/AccountPic'
import Input from '../../common/input/Input'
import FlexSpacer from '../../common/flex-spacer/FlexSpacer'

export default class extends Component {
  state = {
    graduate: {
      name: {},
      phone: {},
      survey: {}
    }
  }

  componentDidMount() {
    this.fetchGraduate()
  }

  render() {
    let graduate = this.state.graduate
    return (
      <Page>
        <Content className={'center'} size={'M'}>
          <Block>
            <AccountPic size={'X3L'} />
          </Block>
          <Block>
            <div className={'flex'}>
              <label>First Name</label>
              <FlexSpacer />
              <Input type={'text'} value={graduate.name.first} />
            </div>
          </Block>
          <Block>
            <div className={'flex'}>
              <label>Last Name</label>
              <FlexSpacer />
              <Input type={'text'} value={graduate.name.last} />
            </div>
          </Block>
          <Block>
            <div className={'flex'}>
              <label>Email Address</label>
              <FlexSpacer />
              <Input type={'text'} value={graduate.email} />
            </div>
          </Block>
          <Block>
            <div className={'flex'}>
              <label>Birthday</label>
              <FlexSpacer />
              <Input
                type={'text'}
                value={new Date(graduate.birthdate).toLocaleDateString()}
              />
            </div>
          </Block>
          <Block>
            <div className={'flex'}>
              <label>Gender</label>
              <FlexSpacer />
              <Input type={'text'} value={graduate.gender} />
            </div>
          </Block>
          <Block>
            <div className={'flex'}>
              <label>Phone Number</label>
              <FlexSpacer />
              <Input
                type={'text'}
                value={`+${graduate.phone.code}${graduate.phone.number}`}
              />
            </div>
          </Block>
          <Block>
            <div className={'flex'}>
              <label>Graduation Year</label>
              <FlexSpacer />
              <Input type={'text'} value={graduate.survey.graduationYear} />
            </div>
          </Block>
          <Block>
            <div className={'flex'}>
              <label>Learning</label>
              <FlexSpacer />
              <Input
                type={'text'}
                value={graduate.survey.learning ? `Yes` : `No`}
              />
            </div>
          </Block>
          <Block>
            <div className={'flex'}>
              <label>Status</label>
              <FlexSpacer />
              <Input type={'text'} value={graduate.survey.status} />
            </div>
          </Block>
          {graduate.survey.status === `COMPANY_EMPLOYED` ? (
            <Block>
              <div className={'flex'}>
                <label>Self Employed By Field</label>
                <FlexSpacer />
                <Input
                  type={'text'}
                  value={graduate.survey.selfEmployedByField ? `Yes` : `No`}
                />
              </div>
            </Block>
          ) : null}
          {graduate.survey.status === `COMPANY_EMPLOYED` ? (
            <Block>
              <div className={'flex'}>
                <label>Job Type</label>
                <FlexSpacer />
                <Input type={'text'} value={graduate.survey.jobType} />
              </div>
            </Block>
          ) : null}
          {graduate.survey.status === `COMPANY_EMPLOYED` ? (
            <Block>
              <div className={'flex'}>
                <label>Job Status</label>
                <FlexSpacer />
                <Input type={'text'} value={graduate.survey.jobStatus} />
              </div>
            </Block>
          ) : null}
        </Content>
      </Page>
    )
  }

  fetchGraduate = () => {
    axios.get(`/data/graduate/${this.props.match.params._id}`).then(res => {
      this.setState({
        graduate: res.data.graduate
      })
    })
  }
}
