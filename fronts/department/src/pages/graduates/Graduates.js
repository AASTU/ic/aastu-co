import React from 'react'
import axios from 'axios'

import Page from '../../common/page/Page'
import Card from '../../common/card/Card'
import Button from '../../common/button/Button'

export default class extends React.Component {
  constructor() {
    super()
    this.state = {
      graduates: []
    }
  }
  componentDidMount() {
    this.fetchGraduates()
  }
  render() {
    let graduates = this.state.graduates
    let graduateList = graduates.map((graduates, index) => (
      <Card
        key={index}
        className={'margin-normal inline-block'}
        size={'M'}
        title={graduates.name.first + ' ' + graduates.name.last}
        subtitle={
          <div>
            {graduates.email}
            <br />
            {graduates.gender}
          </div>
        }
      >
        <Button
          className={'fg-white bg-primary'}
          to={`/graduates/${graduates._id}`}
        >
          See More
        </Button>
      </Card>
    ))
    return <Page top={'adaptive'}>{graduateList}</Page>
  }
  fetchGraduates = () => {
    axios.get(`data/graduate/list/6/start/0`).then(res => {
      this.setState({
        graduates: this.state.graduates.concat(res.data.graduates)
      })
    })
  }
}
