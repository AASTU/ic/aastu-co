import React from 'react'

import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'

export default () => (
  <Page top={'adaptive'}>
    <Content size={'M'}>
      <Block first last>
        todo
      </Block>
    </Content>
  </Page>
)
