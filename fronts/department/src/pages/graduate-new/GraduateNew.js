import React, { Component } from 'react'
import FontAwesome from 'react-fontawesome'
import axios from 'axios'

import Page from '../../common/page/Page'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import TextArea from '../../common/text-area/TextArea'
import FlexSpacer from '../../common/flex-spacer/FlexSpacer'
import Button from '../../common/button/Button'

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      problem: undefined,
      view: undefined,
      emails: []
    }

    this.inputView = this.inputView.bind(this)
    this.progressView = this.progressView.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount() {
    this.setState({ view: this.inputView })
  }

  render() {
    const { problem, view } = this.state

    return (
      <Page>
        {(!problem || (problem && problem.handleInView)) &&
        typeof view === 'function' ? (
          view()
        ) : problem ? (
          <Warning problem={problem} />
        ) : (
          <Loading delay />
        )}
      </Page>
    )
  }

  inputView() {
    const { problem } = this.state

    return (
      <Content size={'M'}>
        {problem ? (
          <Block first>
            <Warning
              bomb={() => this.setState({ problem: undefined })}
              problem={problem}
              shy
            />
          </Block>
        ) : null}

        <Block first>
          <h2>Add Graduates</h2>
        </Block>
        <Block className={'font-S uppercase fg-blackish'}>
          As the department, you can authorize graduates to the system using
          their email addresses.
        </Block>
        <Block className={'font-S uppercase fg-blackish'}>
          Graduates you authorize will immediately receive an email containing a
          link that allows them to register. These links will be valid only for
          48 hours since the time they were sent.
        </Block>
        <Block className={'font-S uppercase fg-blackish'}>
          To authorize graduates, enter their email addresses below (one per
          line).
        </Block>

        <form
          method={'POST'}
          action={'/data/graduate/new'}
          onSubmit={this.onSubmit}
        >
          <Block>
            <TextArea
              className={'full-width'}
              name={'emails'}
              placeholder={"Graduates' emails, one per line..."}
              style={{ minHeight: 140 }}
            />
          </Block>

          <Block last>
            <div className={'flex'}>
              <Button type={'reset'}>Clear</Button>

              <FlexSpacer />

              <Button to={'/'} type={'button'}>
                Cancel
              </Button>
              <span className={'padding-horizontal-small'} />
              <Button type={'submit'}>Authorize</Button>
            </div>
          </Block>
        </form>
      </Content>
    )
  }

  progressView() {
    const { problem, emails } = this.state

    return (
      <Content size={'M'} transparent>
        {problem ? (
          <Block first>
            <Warning
              bomb={() => this.setState({ problem: undefined })}
              problem={problem}
              shy
            />
          </Block>
        ) : null}

        <Block>
          {emails.map((email, i) => (
            <Content key={i} className={'margin-normal'}>
              {email.success === false ? (
                <Warning problem={email.problem} />
              ) : email.success === true ? (
                <Block first last>
                  <span className={'fg-primary'}>
                    <FontAwesome name={'check-circle'} />
                  </span>
                  <span className={'padding-left-big'} />Sent to {email.address}
                </Block>
              ) : (
                <Block first last>
                  <FontAwesome name={'hourglass-half'} />
                  <span className={'padding-left-big'} />Sending to{' '}
                  {email.address}...
                </Block>
              )}
            </Content>
          ))}
        </Block>
      </Content>
    )
  }

  onSubmit(e) {
    e.preventDefault()

    const { emails } = this.state
    for (const address of e.target.emails.value.split('\n')) {
      emails.push({ address: address, success: null })

      axios
        .post(e.target.action, { email: address })
        .then(resp => Promise.resolve(resp.data))
        .then(data => {
          const { emails } = this.state
          for (const [i, email] of emails.entries()) {
            if (email.address === address) {
              emails[i].success = data.success
              emails[i].problem = data.problem
            }
          }
          this.setState({ emails: emails })
        })
        .catch(console.error)
    }
    this.setState({
      view: this.progressView,
      emails: emails
    })
  }
}
