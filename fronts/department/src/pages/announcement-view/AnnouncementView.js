import React, { Component } from 'react'
import axios from 'axios'

import './view.css'

import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Button from '../../common/button/Button'

import pic from '../../assets/images/studentGrad.jpg'
import FlexSpacer from '../../common/flex-spacer/FlexSpacer'

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      announcement: {},
      file: {}
    }
    this.fetchAnnounce = this.fetchAnnounce.bind(this)
    this.fetchFile = this.fetchFile.bind(this)
  }

  componentDidMount() {
    this.fetchAnnounce()
    this.fetchFile()
  }

  deleteAnnouncement = e => {
    if (window.confirm('Are you sure you want to delete this announcement?')) {
      axios
        .delete(`/data/announcement/${this.props.match.params._id}`)
        .then(res => {
          if (res.data.success) {
            this.props.history.push('/announcements')
            window.alert('Successfully deleted announcement')
          } else {
            console.log(res.data.problem)
          }
        })
        .catch(err => {
          console.log(err.message)
        })
    }
  }
  render() {
    return (
      <Page className={'center'}>
        <Content size={'L'} className={''}>
          <Block first last className={'full-width'}>
            <div>
              <img
                src={`/data/announcement/${
                  this.props.match.params._id
                }/attachment`}
                alt="cannot load picture"
              />
            </div>
          </Block>
          <Block>
            <h1> {this.state.announcement.title} </h1>
            <div>
              <h3> {this.state.announcement.location}</h3>
              <h5>
                {' '}
                {new Date(this.state.announcement.time).toLocaleDateString()}
              </h5>
              <h5>
                {' '}
                {new Date(this.state.announcement.time).toLocaleTimeString()}
              </h5>
            </div>
          </Block>
          <Block>
            <div>
              <p>{this.state.announcement.description}</p>
            </div>
          </Block>
          <Block className={'center'} last>
            <div className={'flex'}>
              <h3 className={'light'}>
                Interested &nbsp; &#9733;{
                  this.state.announcement.interestedCount
                }
              </h3>
              <FlexSpacer />
              <Button to={`/announcement/${this.props.match.params._id}/edit`}>
                Edit
              </Button>
              <span className={'padding-left-small'} />
              <Button onClick={this.deleteAnnouncement}>Delete</Button>
            </div>
          </Block>
        </Content>
      </Page>
    )
  }

  fetchAnnounce() {
    axios
      .get(`/data/announcement/${this.props.match.params._id}`)
      .then(response => {
        this.setState({
          announcement: response.data.announcement
        })
      })
      .catch(err => {
        this.setState({
          problem: err
        })
      })
  }

  fetchFile() {
    axios
      .get(`/data/announcement/${this.props.match.params._id}/attachment`)
      .then(response => {
        this.setState({
          file: response.data
        })
      })
      .catch(err => {
        this.setState({
          problem: err
        })
      })
  }
}
