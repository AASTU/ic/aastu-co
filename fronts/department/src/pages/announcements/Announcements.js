import React from 'react'

import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import axios from 'axios/index'
import Anchor from '../../common/anchor/Anchor'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'
import Card from '../../common/card/Card'
import Button from '../../common/button/Button'

export default class extends React.Component {
  constructor() {
    super()
    this.state = {
      announcements: [],
      problem: undefined,
      view: undefined
    }

    this.announcementsList = this.announcementsList.bind(this)
  }

  componentDidMount() {
    this.fetchAnnouncements()
  }
  render() {
    let { announcements } = this.state

    return !announcements ? null : (
      <Page className={'center'}>
        {announcements.map((ann, i) => this.announcementsList(ann))}
      </Page>
    )
  }

  announcementsList(ann) {
    return (
      <Card
        size={'S'}
        className={'inline-block margin-normal'}
        title={
          <h3 className={'fg-primary'}>
            {ann.title.substring(0, 20).toUpperCase()}
          </h3>
        }
        subtitle={[
          <h4>{ann.location}</h4>,
          <h4>{new Date(ann.time).toLocaleDateString()}</h4>,
          <span>@{new Date(ann.time).toLocaleTimeString()}</span>
        ]}
        actions={
          <Button to={`/announcement/${ann._id}`} primary>
            View
          </Button>
        }
        style={{
          minHeight: 330
        }}
      >
        <p className={'fg-blackish'}>{ann.description.substring(0, 100)}...</p>
        <h3 className={'fg-blackish'}>
          Interested <span className={'fg-primary'}>&#9733;</span>
          {ann.interestedCount !== 0 ? ann.interestedCount : ''}
        </h3>
      </Card>
    )
  }

  fetchAnnouncements = () => {
    axios
      .get('/data/announcement/list/8/start/0')
      .then(res => {
        if (res.data.success) {
          this.setState({
            announcements: this.state.announcements.concat(
              res.data.announcements
            )
          })
        } else {
          return this.setState({
            problem: Object.assign(res.data.problem, { handleInView: true })
          })
        }
      })
      .catch(e => {
        this.setState({
          problem: e
        })
      })
  }
}
