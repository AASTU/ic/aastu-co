import React, { Component, createRef } from 'react'
import FontAwesome from 'react-fontawesome'
import axios from 'axios'

import './Account.scss'
import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Input from '../../common/input/Input'
import TextArea from '../../common/text-area/TextArea'
import Button from '../../common/button/Button'
import Anchor from '../../common/anchor/Anchor'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'
import AccountPic from '../../common/account-pic/AccountPic'

export default class extends Component {
  state = {
    problem: undefined,
    view: undefined,
    picUploading: false
  }

  picUploadBtn = createRef()
  picUploadInput = createRef()

  componentDidMount = () => {
    this.setState({ view: this.inputView })
  }

  render = () => {
    const { problem, view } = this.state

    return (
      <Page>
        {(!problem || (problem && problem.handleInView)) &&
        typeof view === 'function' ? (
          view()
        ) : problem ? (
          <Warning problem={problem} />
        ) : (
          <Loading delay />
        )}
      </Page>
    )
  }

  inputView = () => {
    const { account } = this.props
    const { problem } = this.state

    return (
      <Content size={'M'} className={'padding-normal'}>
        {problem ? (
          <Block first>
            <Warning
              bomb={() => this.setState({ problem: undefined })}
              problem={problem}
              shy
            />
          </Block>
        ) : null}

        <div style={{ display: 'none' }}>
          <form
            method={'POST'}
            action={'/data/account/me/picture'}
            encType={'multipart/form-data'}
            onSubmit={this.onPictureUpload}
          >
            <input
              type={'file'}
              name={'picture'}
              onChange={() => this.picUploadBtn.current.click()}
              ref={this.picUploadInput}
            />
            <button type={'submit'} ref={this.picUploadBtn}>
              Upload
            </button>
          </form>
        </div>

        <Block first className={'center'}>
          {this.state.picUploading ? (
            <Loading />
          ) : (
            <AccountPic
              src={`/data/account/me/picture?${new Date().getTime()}`}
              className={'account-pic'}
              size={'X3L'}
            >
              <Anchor
                onClick={() => this.picUploadInput.current.click()}
                className={'change-pic-anchor'}
                title={'Change Picture'}
              >
                <span>
                  <FontAwesome name={'camera'} />
                </span>
              </Anchor>
            </AccountPic>
          )}
        </Block>

        <Block>
          <h3 className={'center'}>Account Settings</h3>
        </Block>

        <form
          method={'POST'}
          action={`/data/account/me`}
          onSubmit={this.onSubmit}
        >
          <Block first className={'font-S light italic'}>
            Basic Information
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'name'}
              placeholder={'Department Name *'}
              defaultValue={account.name}
            />
          </Block>
          <Block>
            <div className={'flex'}>
              <Input
                className={'full-width margin-right-small'}
                name={'headNameFirst'}
                placeholder={"Head's First Name *"}
                defaultValue={account.headName.first}
              />
              <Input
                className={'full-width margin-left-small'}
                name={'headNameLast'}
                placeholder={"Head's Last Name *"}
                defaultValue={account.headName.last}
              />
            </div>
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'collegeName'}
              placeholder={'College/School Name *'}
              defaultValue={account.collegeName}
            />
          </Block>

          <Block first className={'font-S light italic'}>
            Contact Information
          </Block>
          <Block>
            <div className={'flex'}>
              <Input
                name={'phoneCode'}
                placeholder={'Code *'}
                style={{ width: 56 }}
                defaultValue={account.phone.code}
              />
              <Input
                className={'full-width margin-left-normal'}
                name={'phoneNumber'}
                placeholder={'Phone Number *'}
                defaultValue={account.phone.number}
              />
            </div>
          </Block>
          <Block>
            <TextArea
              className={'full-width'}
              name={'contactInfo'}
              placeholder={'Additional Contact Information'}
              defaultValue={account.contactInfo}
            />
          </Block>

          <Block first className={'font-S light italic'}>
            More Detail
          </Block>
          <Block>
            <TextArea
              placeholder={'Brief Department Description'}
              name={'description'}
              className={'full-width'}
              style={{ minHeight: 220 }}
              defaultValue={account.description}
            />
          </Block>

          <Block first className={'font-S light italic'}>
            Login Credentials
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'email'}
              placeholder={'Email (this is public) *'}
              type={'email'}
              defaultValue={account.email}
              disabled
            />
          </Block>
          <Block className={'center'}>
            <Input
              className={'full-width'}
              name={'password'}
              placeholder={'New Password'}
              type={'password'}
            />
          </Block>
          <Block className={'center'}>
            <Input
              className={'full-width'}
              name={'passwordConfirmation'}
              placeholder={'Confirm New Password'}
              type={'password'}
            />
          </Block>

          <Block first last className={'right'}>
            <Button to={'/'} type={'button'}>
              Cancel
            </Button>
            <span className={'padding-left-small'} />
            <Button type={'submit'}>Save Changes</Button>
          </Block>
        </form>
      </Content>
    )
  }

  onSubmit = e => {
    e.preventDefault()

    const {
      name,
      headNameFirst,
      headNameLast,
      collegeName,
      phoneCode,
      phoneNumber,
      contactInfo,
      description,
      email,
      password,
      passwordConfirmation
    } = e.target

    if (password.value !== passwordConfirmation.value) {
      this.setState({
        problem: {
          handleInView: true,
          message: 'Password does not match confirmation.'
        }
      })
      return false
    }

    axios
      .put(
        e.target.action,
        {
          name: name.value,
          headName: {
            first: headNameFirst.value,
            last: headNameLast.value
          },
          collegeName: collegeName.value,
          phone: {
            code: phoneCode.value,
            number: phoneNumber.value
          },
          contactInfo: contactInfo.value,
          description: description.value,
          email: email.value,
          password: password.value
        },
        { withCredentials: true }
      )
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success)
          return this.setState({
            problem: Object.assign(data.problem, { handleInView: true }),
            view: this.inputView
          })

        this.setState({ problem: null, view: null })
        window.location.href = '/'
      })
      .catch(err => this.setState({ problem: err || 'Unknown error.' }))
  }

  onPictureUpload = e => {
    e.preventDefault()

    const { action, picture } = e.target

    const data = new FormData()
    data.append('picture', picture.files[0])

    this.setState({ picUploading: true })

    axios
      .post(action, data, {
        headers: { 'Content-Type': 'multipart/form-data' },
        withCredentials: true
      })
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success)
          return this.setState({
            problem: Object.assign(data.problem, { handleInView: true }),
            view: this.inputView
          })

        this.setState({ picUploading: false })
      })
      .catch(err => this.setState({ problem: err || 'Picture upload error.' }))

    return false
  }
}
