import React, { Component } from 'react'
import axios from 'axios'

import Page from '../../common/page/Page'
import Card from '../../common/card/Card'
import Block from '../../common/block/Block'
import Content from '../../common/content/Content'
import FlexSpacer from '../../common/flex-spacer/FlexSpacer'
import Button from '../../common/button/Button'
import Anchor from '../../common/anchor/Anchor'

export default class extends Component {
  state = {
    problem: undefined,
    vacancy: {
      company: {},
      applicants: []
    }
  }

  componentDidMount() {
    this.fetchState()
  }

  render() {
    const { vacancy } = this.state

    const al = vacancy.applicants.length

    return (
      <Page className={'center'}>
        <Content transparent size={'M'}>
          <h3 className={'light right'}>Vacancy Detail</h3>
        </Content>

        <Card
          size={'M'}
          title={
            <h2 className={'padding-none margin-none'}>{vacancy.jobTitle}</h2>
          }
          subtitle={[
            `Posted by ${vacancy.company.name} on ${new Date(
              vacancy.createdOn
            ).toLocaleDateString()}.`
          ]}
        >
          <div className={'padding-bottom-normal'}>
            {vacancy.quantity} vacant position{vacancy.quantity !== 1
              ? 's'
              : ''}{' '}
            available in {vacancy.jobLocation}.
            <br />
            There {al === 1 ? 'is' : 'are'} {al} applicant{al === 1 ? '' : 's'},
            so far.
          </div>

          <div className={'padding-normal'}>
            <span className={'light'}>Salary:</span> {vacancy.salary}
          </div>
          <div className={'padding-normal'}>
            <span className={'light'}>Job Type:</span>{' '}
            {vacancy.jobType === 'EMPLOYEE'
              ? 'Employee'
              : vacancy.jobType === 'CONTRACT'
                ? 'Contract'
                : vacancy.jobType === 'INTERN' ? 'Internship' : vacancy.jobType}
          </div>
          <div className={'padding-normal'}>
            <span className={'light'}>Job Status:</span>{' '}
            {vacancy.jobStatus === 'FULL_TIME'
              ? 'Full Time'
              : vacancy.jobStatus === 'PART_TIME'
                ? 'Part Time'
                : vacancy.jobStatus === 'PER_DIEM'
                  ? 'Per Dime'
                  : vacancy.jobStatus}
          </div>

          <div className={'padding-top-big light'}>About the Job:</div>
          {!vacancy.description ? null : (
            <div className={'padding-normal'}>
              {vacancy.description.split('\n').map((line, i) => (
                <div key={i} className={'padding-bottom-small'}>
                  {line}
                </div>
              ))}
            </div>
          )}
        </Card>

        {!vacancy.applicants.length ? (
          <Content size={'M'} className={'margin-top-normal'}>
            <Block first last className={'italic light'}>
              No applicants for this vacancy post, yet.
            </Block>
          </Content>
        ) : (
          vacancy.applicants.map((a, index) => (
            <Content key={index} size={'M'} className={'margin-top-normal'}>
              <Block first last>
                <div className={'flex'}>
                  <div>
                    <div className={'font-L light'}>
                      <Anchor onClick={() => this.downloadCV(a.applicant._id)}>
                        {' '}
                        {a.applicant.name.first} {a.applicant.name.last}
                      </Anchor>
                    </div>
                    <div>{a.applicant.email}</div>
                  </div>
                  <FlexSpacer />
                  <Button onClick={() => this.recommend(vacancy, a)}>
                    {a.recommended ? 'Undo Recommend' : 'Recommend'}
                  </Button>
                </div>
              </Block>
            </Content>
          ))
        )}
      </Page>
    )
  }

  fetchState = () => {
    axios
      .get(`/data/vacancy/${this.props.match.params._id}`)
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success) return this.setState({ problem: data.problem })

        if (data.vacancy)
          this.setState({
            vacancy: data.vacancy
          })
      })
      .catch(err => this.setState({ problem: err }))
  }

  recommend = (vacancy, a) => {
    // todo: better UI
    if (
      window.confirm(
        `Are you sure you want to ${
          a.recommended
            ? 'undo'
            : 'recommend ' +
              a.applicant.name.first +
              ' ' +
              a.applicant.name.last
        }?`
      )
    ) {
      axios
        .put(`/data/vacancy/${vacancy._id}/recommend/${a.applicant._id}`)
        .then(resp => Promise.resolve(resp.data))
        .then(data => {
          if (!data.success) return this.setState({ problem: data.problem })

          this.fetchState()
        })
        .catch(err => this.setState({ problem: err }))
    }
  }

  downloadCV = id => {
    axios({
      url: `/data/graduate/${id}/cv/download`,
      method: 'GET',
      responseType: 'blob'
    }).then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]))
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', 'cv.pdf')
      document.body.appendChild(link)
      link.click()
    })
  }
}
