import React, { Component } from 'react'
import FontAwesome from 'react-fontawesome'
import axios from 'axios'

import Page from '../../common/page/Page'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Input from '../../common/input/Input'
import TextArea from '../../common/text-area/TextArea'
import Button from '../../common/button/Button'
import FlexSpacer from '../../common/flex-spacer/FlexSpacer'

// todo: Next -> to add profile pics (use axios instead of <form>)
export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      problem: undefined,
      view: undefined,
      announcement: undefined
    }

    this.inputView = this.inputView.bind(this)
    this.attachView = this.attachView.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.onUpload = this.onUpload.bind(this)
  }

  componentDidMount() {
    this.setState({ view: this.inputView })
  }

  render() {
    const { problem, view } = this.state

    return (
      <Page>
        {(!problem || (problem && problem.handleInView)) &&
        typeof view === 'function' ? (
          view()
        ) : problem ? (
          <Warning problem={problem} />
        ) : (
          <Loading delay />
        )}
      </Page>
    )
  }

  inputView() {
    const { problem } = this.state

    return (
      <Content size={'M'}>
        {problem ? (
          <Block first>
            <Warning
              bomb={() => this.setState({ problem: undefined })}
              problem={problem}
              shy
            />
          </Block>
        ) : null}

        <Block first>
          <h2>Add a New Announcement</h2>
        </Block>

        <form
          method={'POST'}
          action={'/data/announcement/new'}
          onSubmit={this.onSubmit}
        >
          <Block first className={'font-XS bold uppercase'}>
            Basic Information:
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'title'}
              placeholder={'Announcement Title *'}
            />
          </Block>
          <Block>
            <TextArea
              className={'full-width'}
              name={'description'}
              placeholder={'Announcement Description *'}
            />
          </Block>

          <Block first className={'font-XS bold uppercase'}>
            Optional, Event Information:
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'time'}
              type={'datetime-local'}
            />
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'location'}
              placeholder={'Location'}
              type={'location'}
            />
          </Block>

          <Block last>
            <div className={'flex'}>
              <Button type={'reset'}>Clear</Button>

              <FlexSpacer />

              <Button to={'/'}>Cancel</Button>
              <span className={'padding-left-small'} />
              <Button type={'submit'}>
                Add<span className={'padding-left-normal'} />
                <FontAwesome name={'arrow-right'} />
              </Button>
            </div>
          </Block>
        </form>
      </Content>
    )
  }

  attachView() {
    const { announcement } = this.state

    return (
      <Content size={'M'}>
        <form
          method={'POST'}
          action={`/data/announcement/${announcement._id}/attachment/new`}
          encType={'multipart/form-data'}
          onSubmit={this.onUpload}
        >
          <Block first>
            Optionally, Add an Attachment for "{announcement.title}"
          </Block>
          <Block>
            <Input name={'attachment'} type={'file'} />
          </Block>
          <Block last>
            <Button type={'submit'}>Upload</Button>
          </Block>
        </form>
      </Content>
    )
  }

  onSubmit(e) {
    e.preventDefault()

    const { title, description, time, location } = e.target

    this.setState({ view: null })

    axios
      .post(
        e.target.action,
        {
          title: title.value,
          description: description.value,
          time: time.value,
          location: location.value
        },
        { withCredentials: true }
      )
      .then(resp => resp.data)
      .then(data => {
        if (!data.success)
          this.setState({
            problem: Object.assign(data.problem, { handleInView: true }),
            view: this.inputView
          })
        else
          this.setState({
            problem: null,
            view: this.attachView,
            announcement: data.announcement
          })
      })
      .catch(err => this.setState({ problem: err }))
  }

  onUpload(e) {
    e.preventDefault()

    const { attachment } = e.target

    let formData = new FormData()

    formData.append('attachment', attachment.files[0])

    axios
      .post(e.target.action, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        withCredentials: true
      })
      .then(response => {
        console.log('response from axios', response)
        if (!response.data.success)
          this.setState({
            problem: Object.assign(response.data.problem),
            view: this.attachView
          })
        else
          this.setState({
            problem: null,
            view: this.inputView
          })
      })
      .catch(err => this.setState({ problem: err }))
  }
}
