import React, { Component, createRef } from 'react'
import * as qs from 'qs'

import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Warning from '../../common/warning/Warning'
import Logo from '../../common/logo/Logo'
import Input from '../../common/input/Input'
import Button from '../../common/button/Button'
import Anchor from '../../common/anchor/Anchor'

export default class extends Component {
  email = createRef()

  constructor(props) {
    super(props)

    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount() {
    this.email.current.focus()
  }

  render() {
    const query =
      qs.parse(window.location.search, { ignoreQueryPrefix: true }) || {}

    return (
      <Page top={'adaptive'}>
        <Content size={'S'}>
          <form
            method={'POST'}
            action={'/data/auth/login?' + qs.stringify(query)}
            onSubmit={this.onSubmit}
          >
            {query.problem && query.problem.code === 'WRONG_CREDENTIALS' ? (
              <Block first>
                <Warning shy bomb problem={'Wrong email or password.'} />
              </Block>
            ) : null}

            <Block className={'center'} first>
              <Logo size={'X5L'} />
            </Block>

            <Block className={'center'}>
              <Input
                className={'full-width'}
                name={'email'}
                placeholder={'Email'}
                inputRef={this.email}
              />
            </Block>

            <Block className={'center'}>
              <Input
                className={'full-width'}
                name={'password'}
                placeholder={'Password'}
                type={'password'}
              />
            </Block>

            <Block>
              <Button className={'full-width'} type={'submit'}>
                Login
              </Button>
            </Block>

            <Block last className={'font-S'}>
              <Anchor to={'/reset'}>Reset password?</Anchor>
            </Block>
          </form>
        </Content>
      </Page>
    )
  }

  onSubmit(e) {
    e.preventDefault()
    e.target.submit()

    const { onSubmit } = this.props
    setTimeout(() => !(typeof onSubmit === 'function') || onSubmit(), 0)
  }
}
