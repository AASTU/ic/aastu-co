import React from 'react'
import axios from 'axios'
import Card from '../../common/card/Card'
import Page from '../../common/page/Page'
import Button from '../../common/button/Button'
import Content from '../../common/content/Content'

export default class extends React.Component {
  state = {
    departments: [],
    headName: {}
  }

  componentDidMount() {
    this.fetchDepartments()
  }

  deleteDepartment = departmentId => {
    if (window.confirm('Are you sure you want to delete this department?')) {
      axios
        .delete(`/data/department/${departmentId}`)
        .then(res => {
          if (res.data.success) {
            this.props.history.push(`/departments`)
            window.alert('Successfully deleted department')
          } else {
            console.log(res.data.problem)
          }
        })
        .catch(err => {
          console.log(err.message)
        })
    }
  }

  render() {
    let departments = this.state.departments
    let departmentList = departments.map((department, index) => (
      <Card
        key={index}
        size={'S'}
        className={'inline-block margin-normal'}
        title={
          <h3 className={'fg-primary'}>{department.name.toUpperCase()}</h3>
        }
        subtitle={[
          <h3>{department.headName.first}</h3>,
          <h3>{department.headName.last}</h3>,
          <h3>{department.email}</h3>
        ]}
        actions={[
          <Button
            onClick={() => this.deleteDepartment(department._id)}
            style={{ backgroundColor: '#e23939', color: '#fff' }}
          >
            Delete
          </Button>
        ]}
      />
    ))
    return <Page className={'center'}>{departmentList}</Page>
  }

  fetchDepartments = () => {
    axios.get(`/data/department/all`).then(res => {
      this.setState({
        departments: this.state.departments.concat(res.data.departments)
      })
    })
  }
}
