import React from 'react'
import axios from 'axios'
import Card from '../../common/card/Card'
import Page from '../../common/page/Page'
import Button from '../../common/button/Button'
import Content from '../../common/content/Content'

export default class extends React.Component {
  state = {
    companies: []
  }

  componentDidMount() {
    this.fetchCompanies()
  }

  deleteCompany = companyId => {
    if (window.confirm('Are you sure you want to delete this company?')) {
      axios
        .delete(`/data/company/${companyId}`)
        .then(res => {
          if (res.data.success) {
            this.props.history.push(`/`)
            window.alert('Successfully deleted Company')
          } else {
            console.log(res.data.problem)
          }
        })
        .catch(err => {
          console.log(err.message)
        })
    }
  }

  render() {
    let companies = this.state.companies
    let companyList = companies.map((company, index) => (
      <Card
        key={index}
        size={'S'}
        className={'inline-block margin-normal'}
        title={<h3 className={'fg-primary'}>{company.name.toUpperCase()}</h3>}
        subtitle={[<h3>{company.email}</h3>]}
        actions={[
          <Button
            onClick={() => this.deleteCompany(company._id)}
            style={{ backgroundColor: '#e23939', color: '#fff' }}
          >
            Delete
          </Button>
        ]}
      />
    ))
    return <Page className={'center'}>{companyList}</Page>
  }

  fetchCompanies = () => {
    axios.get(`/data/company/list/8/start/0`).then(res => {
      this.setState({
        companies: this.state.companies.concat(res.data.companies)
      })
    })
  }
}
