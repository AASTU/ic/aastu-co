import React, { Component } from 'react'
import axios from 'axios'

import './Account.scss'
import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Logo from '../../common/logo/Logo'
import Input from '../../common/input/Input'
import Button from '../../common/button/Button'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'

// todo: fixme: submitting by Enter key malfunctions
export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      problem: undefined,
      view: undefined
    }

    this.onSubmit = this.onSubmit.bind(this)
    this.inputView = this.inputView.bind(this)
  }

  componentDidMount() {
    this.setState({ view: this.inputView })
  }

  render() {
    const { problem, view } = this.state

    return (
      <Page>
        {(!problem || (problem && problem.handleInView)) &&
        typeof view === 'function' ? (
          view()
        ) : problem ? (
          <Warning problem={problem} />
        ) : (
          <Loading delay />
        )}
      </Page>
    )
  }

  inputView() {
    const { account } = this.props
    const { problem } = this.state

    return (
      <Content size={'M'}>
        {problem ? (
          <Block first>
            <Warning
              bomb={() => this.setState({ problem: undefined })}
              problem={problem}
              shy
            />
          </Block>
        ) : null}

        <Block first className={'center'}>
          <Logo size={'X3L'} />
        </Block>

        <Block>
          <h3 className={'center'}>Account Settings</h3>
        </Block>

        <form
          method={'POST'}
          action={`/data/account/me`}
          onSubmit={this.onSubmit}
        >
          <Block first className={'font-S light italic'}>
            Login Credentials
          </Block>
          <Block className={'center'}>
            <Input
              className={'full-width'}
              name={'password'}
              placeholder={'New Password'}
              type={'password'}
            />
          </Block>
          <Block className={'center'}>
            <Input
              className={'full-width'}
              name={'passwordConfirmation'}
              placeholder={'Confirm New Password'}
              type={'password'}
            />
          </Block>

          <Block first last className={'right'}>
            <Button to={'/'} type={'button'}>
              Cancel
            </Button>
            <span className={'padding-left-small'} />
            <Button type={'submit'}>Save Changes</Button>
          </Block>
        </form>
      </Content>
    )
  }

  onSubmit(e) {
    e.preventDefault()

    const { password, passwordConfirmation } = e.target

    if (password.value !== passwordConfirmation.value) {
      this.setState({
        problem: {
          handleInView: true,
          message: 'Password does not match confirmation.'
        }
      })
      return false
    }

    axios
      .put(
        e.target.action,
        {
          password: password.value
        },
        { withCredentials: true }
      )
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success)
          return this.setState({
            problem: Object.assign(data.problem, { handleInView: true }),
            view: this.inputView
          })

        this.setState({ problem: null, view: null })
        window.location.href = '/'
      })
      .catch(err => this.setState({ problem: err || 'Unknown error.' }))
  }
}
