import React, { Component } from 'react'

import './NavBar.scss'
import LeftDrawer from '../../common/left-drawer/LeftDrawer'
import Block from '../../common/block/Block'
import Logo from '../../common/logo/Logo'
import Anchor from '../../common/anchor/Anchor'

export default class extends Component {
  links = [
    { to: '/login', title: 'Login', auth: false },
    { to: '/', title: 'Companies', auth: true },
    { to: '/company/new', title: 'New Company', auth: true },
    { to: '/departments', title: 'Departments', auth: true },
    { to: '/department/new', title: 'New Department', auth: true },
    { to: '/account', title: 'Account Settings', auth: true }
  ]

  render() {
    const { account, onNavClose, ...rest } = this.props

    return (
      <LeftDrawer size={'M'} {...rest}>
        <Block first className={'center'}>
          <Logo size={'S'} />
        </Block>
        <Block last className={'font-L light center'}>
          AASTU Co. Admin
        </Block>

        {this.links.map(
          (link, i) =>
            (!account && link.auth !== true) ||
            (account && link.auth !== false) ? (
              <Anchor
                key={i}
                className={'block padding-none full-width center nav-bar-link'}
                onClick={onNavClose}
                to={link.to}
              >
                <Block
                  className={'padding-vertical-big padding-horizontal-small'}
                >
                  {link.title}
                </Block>
              </Anchor>
            ) : null
        )}
      </LeftDrawer>
    )
  }
}
