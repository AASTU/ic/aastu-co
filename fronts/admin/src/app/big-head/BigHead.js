import React, { Component } from 'react'
import FontAwesome from 'react-fontawesome'
import './BigHead.scss'

import Button from '../../common/button/Button'
import Logo from '../../common/logo/Logo'
import Anchor from '../../common/anchor/Anchor'
import AccountPic from '../../common/account-pic/AccountPic'
import MenuDrop from '../../common/menu-drop/MenuDrop'
import MenuItem from '../../common/menu-item/MenuItem'
import Content from '../../common/content/Content'

export default class extends Component {
  state = {
    showMenu: false
  }

  render() {
    const { account, onLogout, onNavOpen } = this.props

    return (
      <div className={'header-bg'}>
        <Content className={'header'}>
          <span className={'header-left'}>
            <Button
              className={'menu-btn header-btn header-box-size'}
              onClick={onNavOpen}
            >
              <FontAwesome name={'bars'} />
            </Button>
          </span>

          <Logo className={'logo-size'} to={'/'} />

          <span className={'header-right'}>
            {account ? (
              <span>
                <Anchor
                  className={'header-text margin-right-small'}
                  to={'/account'}
                >
                  Admin.
                </Anchor>
                <div className={'header-box-size'}>
                  <Button
                    className={'account-menu-btn header-btn header-box-size'}
                    onClick={() =>
                      this.setState({ showMenu: !this.state.showMenu })
                    }
                  >
                    <AccountPic
                      src={'/favicon.ico'}
                      className={'account-pic-size'}
                    />
                  </Button>
                  <MenuDrop
                    align={'right'}
                    anchorOffset={56}
                    onClose={() => this.setState({ showMenu: false })}
                    open={this.state.showMenu}
                    size={'L'}
                  >
                    <MenuItem
                      to={'/account'}
                      onClick={() => this.setState({ showMenu: false })}
                    >
                      <FontAwesome name={'user'} />&nbsp;&nbsp;&nbsp;Account
                      Settings
                    </MenuItem>
                    <MenuItem onClick={onLogout}>
                      <FontAwesome name={'sign-out'} />&nbsp;&nbsp;&nbsp;Logout
                    </MenuItem>
                  </MenuDrop>
                </div>
              </span>
            ) : (
              <Button className={'header-btn'} to={'/login'}>
                Login
              </Button>
            )}
          </span>
        </Content>
      </div>
    )
  }
}
