import React from 'react'
import { Link } from 'react-router-dom'

import Button from '../button/Button'

export default ({ button, children, to, ...rest }) => {
  const child = button ? <Button>{children}</Button> : children

  return typeof to === 'string' ? (
    <Link to={to} {...rest}>
      {child}
    </Link>
  ) : (
    <a {...rest}>{child}</a>
  )
}
