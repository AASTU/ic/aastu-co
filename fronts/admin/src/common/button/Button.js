import React from 'react'
import { Link } from 'react-router-dom'

import './Button.scss'

export default ({ children, className, primary, to, type, ...rest }) => {
  const button = (
    <button
      {...rest}
      className={`${
        primary || (typeof type === 'string' && type.toLowerCase() === 'submit')
          ? 'button-primary'
          : ''
      }${className ? ' ' + className : ''}`}
      type={type || 'button'}
    >
      {children}
    </button>
  )

  return to ? <Link to={to}>{button}</Link> : button
}
