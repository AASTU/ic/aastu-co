import React, { Component } from 'react'
import axios from 'axios'

import Warning from '../../../common/warning/Warning'
import Loading from '../../../common/loading/Loading'
import Block from '../../../common/block/Block'
import Input from '../../../common/input/Input'
import Button from '../../../common/button/Button'

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      problem: undefined,
      view: undefined
    }

    this.inputView = this.inputView.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount() {
    this.setState({ view: this.inputView })
  }

  render() {
    const { problem, view } = this.state

    return (!problem || (problem && problem.handleInView)) &&
      typeof view === 'function' ? (
      view()
    ) : problem ? (
      <Warning problem={problem} />
    ) : (
      <Loading delay />
    )
  }

  inputView() {
    const { email, serial } = this.props

    return (
      <form
        method={'POST'}
        action={'/data/auth/reset/finish'}
        onSubmit={this.onSubmit}
      >
        <input name={'email'} value={email} type={'hidden'} />
        <input name={'randomKey'} value={serial} type={'hidden'} />

        {this.state.problem ? (
          <Block first>
            <Warning
              bomb
              problem={this.state.problem}
              shy={() => this.setState({ problem: undefined })}
            />
          </Block>
        ) : null}

        <Block first>
          <div className={'font-L bold margin-bottom-normal'}>
            Set a New Password
          </div>
          <div className={'font-S fg-blackish'}>
            Set a new password for {email}.
          </div>
        </Block>

        <Block>
          <Input
            className={'full-width'}
            name={'password'}
            placeholder={'New Password'}
            type={'password'}
          />
        </Block>

        <Block>
          <Input
            className={'full-width'}
            name={'repeatPassword'}
            placeholder={'Repeat Password'}
            type={'password'}
          />
        </Block>

        <Block last className={'right'}>
          <Button type={'submit'}>Set Password</Button>
        </Block>
      </form>
    )
  }

  onSubmit(e) {
    e.preventDefault()

    const { action, email, password, repeatPassword, serial } = e.target

    if (!password.value)
      return this.setState({
        problem: {
          handleInView: true,
          message: 'Password field is required.'
        }
      })
    if (!repeatPassword.value)
      return this.setState({
        problem: {
          handleInView: true,
          message: 'Repeat Password field is required.'
        }
      })
    if (password.value !== repeatPassword.value)
      return this.setState({
        problem: {
          handleInView: true,
          message: 'Repeated password does not match.'
        }
      })

    this.setState({ view: null })

    axios
      .post(action, {
        email: email.value,
        serial: serial.value,
        password: password.value
      })
      .then(resp => resp.data)
      .then(data => {
        if (!data.success)
          this.setState({
            handleInView: true,
            problem: data.problem
          })
        else this.props.history.push('/login')
      })
      .catch(err => this.setState({ problem: err }))
  }
}
