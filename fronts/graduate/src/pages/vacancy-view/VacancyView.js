import React, { Component } from 'react'
import axios from 'axios'

import Page from '../../common/page/Page'
import Card from '../../common/card/Card'
import Content from '../../common/content/Content'
import FlexSpacer from '../../common/flex-spacer/FlexSpacer'
import Button from '../../common/button/Button'

export default class extends Component {
  state = {
    problem: undefined,
    vacancy: {
      company: {},
      applicants: []
    }
  }

  componentDidMount() {
    this.fetchState()
  }

  render() {
    const { account } = this.props
    const { vacancy } = this.state

    const al = vacancy.applicants.length

    let applied = false
    for (const a of vacancy.applicants)
      if (a.applicant === account._id) applied = true

    return (
      <Page className={'center'}>
        <Content transparent size={'M'}>
          <h3 className={'light right'}>Vacancy Detail</h3>
        </Content>

        <Card
          size={'M'}
          title={
            <h2 className={'padding-none margin-none'}>{vacancy.jobTitle}</h2>
          }
          subtitle={[
            `Posted by ${vacancy.company.name} on ${new Date(
              vacancy.createdOn
            ).toLocaleDateString()}.`
          ]}
          actions={[
            <FlexSpacer />,
            <Button onClick={() => this.apply(applied)}>
              {applied ? 'Undo ' : ''}Apply For This Job
            </Button>
          ]}
        >
          <div className={'padding-bottom-normal'}>
            {vacancy.quantity} vacant position{vacancy.quantity !== 1
              ? 's'
              : ''}{' '}
            available in {vacancy.jobLocation}.
            <br />
            There {al === 1 ? 'is' : 'are'} {al} applicant{al === 1 ? '' : 's'},
            so far.
          </div>

          <div className={'padding-normal'}>
            <span className={'light'}>Salary:</span> {vacancy.salary}
          </div>
          <div className={'padding-normal'}>
            <span className={'light'}>Job Type:</span>{' '}
            {vacancy.jobType === 'EMPLOYEE'
              ? 'Employee'
              : vacancy.jobType === 'CONTRACT'
                ? 'Contract'
                : vacancy.jobType === 'INTERN' ? 'Internship' : vacancy.jobType}
          </div>
          <div className={'padding-normal'}>
            <span className={'light'}>Job Status:</span>{' '}
            {vacancy.jobStatus === 'FULL_TIME'
              ? 'Full Time'
              : vacancy.jobStatus === 'PART_TIME'
                ? 'Part Time'
                : vacancy.jobStatus === 'PER_DIEM'
                  ? 'Per Dime'
                  : vacancy.jobStatus}
          </div>

          <div className={'padding-top-big light'}>About the Job:</div>
          {!vacancy.description ? null : (
            <div className={'padding-normal'}>
              {vacancy.description.split('\n').map((line, i) => (
                <div key={i} className={'padding-bottom-small'}>
                  {line}
                </div>
              ))}
            </div>
          )}
        </Card>
      </Page>
    )
  }

  fetchState = () => {
    axios
      .get(`/data/vacancy/${this.props.match.params._id}`)
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success) return this.setState({ problem: data.problem })

        if (data.vacancy)
          this.setState({
            vacancy: data.vacancy
          })
      })
      .catch(err => this.setState({ problem: err }))
  }

  recommend = (vacancy, applicant) => {
    // todo: better UI
    if (
      window.confirm(
        `Are you sure you want to recommend ${applicant.name.first} ${
          applicant.name.last
        }?`
      )
    ) {
      axios
        .put(`/data/vacancy/${this.props.match.params._id}/apply`)
        .then(resp => Promise.resolve(resp.data))
        .then(data => {
          if (!data.success) return this.setState({ problem: data.problem })

          this.fetchState()
        })
        .catch(err => this.setState({ problem: err }))
    }
  }

  apply = applied => {
    // todo: better UI
    if (
      window.confirm(
        `Are you sure you want to ${applied ? 'undo' : 'apply for this job'}?`
      )
    ) {
      axios
        .put(`/data/vacancy/${this.props.match.params._id}/apply`)
        .then(resp => Promise.resolve(resp.data))
        .then(data => {
          if (!data.success) return this.setState({ problem: data.problem })

          this.fetchState()
        })
        .catch(err => this.setState({ problem: err }))
    }
  }
}
