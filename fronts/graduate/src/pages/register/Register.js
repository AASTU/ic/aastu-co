import React, { Component } from 'react'
import axios from 'axios'
import qs from 'qs'

import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Logo from '../../common/logo/Logo'
import Input from '../../common/input/Input'
import TextArea from '../../common/text-area/TextArea'
import Button from '../../common/button/Button'
import Anchor from '../../common/anchor/Anchor'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      problem: undefined,
      view: undefined,
      companies: [],
      departments: [],
      potentialCompany: false,
      status: 'UNEMPLOYED'
    }

    this.inputView = this.inputView.bind(this)
    this.onSurveyStatusChange = this.onSurveyStatusChange.bind(this)
    this.onEmployerChange = this.onEmployerChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount() {
    this.setState({ view: this.inputView })

    axios
      .get('/data/company/all')
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (data.success)
          this.setState({
            companies: this.state.companies.concat(data.companies)
          })
      })
      .catch(console.error)

    axios
      .get('/data/department/all')
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (data.success)
          this.setState({
            departments: this.state.departments.concat(data.departments)
          })
      })
      .catch(console.error)
  }

  render() {
    const { problem, view } = this.state

    return (
      <Page>
        {(!problem || (problem && problem.handleInView)) &&
        typeof view === 'function' ? (
          view()
        ) : problem ? (
          <Warning problem={problem} />
        ) : (
          <Loading delay />
        )}
      </Page>
    )
  }

  inputView() {
    const {
      problem,
      companies,
      departments,
      potentialCompany,
      status
    } = this.state

    return (
      <Content size={'L'} className={'padding-normal'}>
        {problem ? (
          <Block first>
            <Warning
              bomb={() => this.setState({ problem: undefined })}
              problem={problem}
              shy
            />
          </Block>
        ) : null}

        <Block first className={'center'}>
          <Logo size={'M'} />
        </Block>

        <Block>
          <h3 className={'center'}>Register Yourself as a Graduate</h3>
        </Block>

        <form
          method={'POST'}
          action={`/data/auth/register${qs.stringify(
            qs.parse(window.location.search, { ignoreQueryPrefix: true }),
            { addQueryPrefix: true }
          )}`}
          onSubmit={this.onSubmit}
        >
          <Block first className={'font-S light italic'}>
            Basic Information
          </Block>
          <Block>
            <div className={'flex'}>
              <Input
                className={'full-width margin-right-small'}
                name={'nameFirst'}
                placeholder={'First Name *'}
              />
              <Input
                className={'full-width margin-left-small'}
                name={'nameLast'}
                placeholder={'Last Name *'}
              />
            </div>
          </Block>
          <Block>
            <div className={'flex'} style={{ alignItems: 'flex-end' }}>
              <div
                className={
                  'quarter-width font-S fg-primary padding-right-normal'
                }
                style={{ paddingBottom: 3 }}
              >
                Birthdate&nbsp;*:
              </div>
              <Input
                className={'full-width'}
                name={'birthdate'}
                type={'date'}
              />
            </div>
          </Block>
          <Block>
            <div className={'flex'} style={{ alignItems: 'flex-end' }}>
              <div
                className={
                  'quarter-width font-S fg-primary padding-right-normal'
                }
              >
                Gender&nbsp;*:
              </div>
              <select className={'full-width'} name={'gender'}>
                <option value={'F'} selected>
                  Female
                </option>
                <option value={'M'}>Male</option>
              </select>
            </div>
          </Block>
          <Block>
            <div className={'flex'} style={{ alignItems: 'flex-end' }}>
              <div
                className={
                  'quarter-width font-S fg-primary padding-right-normal'
                }
              >
                Nationality&nbsp;*:
              </div>
              <select className={'full-width'} name={'nationality'}>
                <option value={'ET'} selected>
                  Ethiopian
                </option>
                <option value={'OTHER'}>Other</option>
              </select>
            </div>
          </Block>

          <Block first className={'font-S light italic'}>
            Contact Information
          </Block>
          <Block>
            <div className={'flex'}>
              <Input
                name={'phoneCode'}
                placeholder={'Code *'}
                style={{ width: 56 }}
              />
              <Input
                className={'full-width margin-left-normal'}
                name={'phoneNumber'}
                placeholder={'Phone Number *'}
              />
            </div>
          </Block>
          <Block>
            <TextArea
              className={'full-width'}
              name={'contactInfo'}
              placeholder={'Additional Contact Information'}
            />
          </Block>

          <Block first className={'font-S light italic'}>
            Education
          </Block>
          <Block>
            <Input
              className={'full-width'}
              defaultValue={new Date().getFullYear()}
              name={'surveyGraduationYear'}
              placeholder={'Graduation Year *'}
              type={'number'}
            />
          </Block>
          <Block>
            <div className={'flex'} style={{ alignItems: 'flex-end' }}>
              <div
                className={
                  'quarter-width font-S fg-primary padding-right-normal'
                }
              >
                Department&nbsp;*:
              </div>
              <select className={'full-width'} name={'department'}>
                {departments.map((department, i) => (
                  <option key={i} value={department._id}>
                    {department.name}
                  </option>
                ))}
              </select>
            </div>
          </Block>
          <Block>
            <div className={'flex'} style={{ alignItems: 'flex-end' }}>
              <div
                className={
                  'quarter-width font-S fg-primary padding-right-normal'
                }
              >
                After AASTU *:
              </div>
              <select className={'full-width'} name={'surveyLearning'}>
                <option value={'TRUE'}>
                  Taking some more formal education now or soon.
                </option>
                <option value={'FALSE'} selected>
                  Not taking any more formal education anytime soon.
                </option>
              </select>
            </div>
          </Block>

          <Block first className={'font-S light italic'}>
            Work
          </Block>
          <Block>
            <div className={'flex'} style={{ alignItems: 'flex-end' }}>
              <div
                className={
                  'quarter-width font-S fg-primary padding-right-normal'
                }
              >
                Current Status *:
              </div>
              <select
                className={'full-width'}
                name={'surveyStatus'}
                onChange={this.onSurveyStatusChange}
              >
                <option
                  value={'SELF_EMPLOYED'}
                  selected={status === 'SELF_EMPLOYED'}
                >
                  Self Employed
                </option>
                <option
                  value={'COMPANY_EMPLOYED'}
                  selected={status === 'COMPANY_EMPLOYED'}
                >
                  Company Employed
                </option>
                <option value={'UNEMPLOYED'} selected={status === 'UNEMPLOYED'}>
                  Unemployed
                </option>
              </select>
            </div>
          </Block>

          {status === 'SELF_EMPLOYED' ? (
            <Block>
              <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                <div
                  className={
                    'quarter-width font-S fg-primary padding-right-normal'
                  }
                >
                  Using field? *:
                </div>
                <select
                  className={'full-width'}
                  name={'surveySelfEmployedByField'}
                >
                  <option value={'TRUE'} selected>
                    I'm self employed by my field.
                  </option>
                  <option value={'FALSE'}>
                    I'm using other skills or fields.
                  </option>
                </select>
              </div>
            </Block>
          ) : status === 'COMPANY_EMPLOYED' ? (
            <div>
              <Block>
                <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                  <div
                    className={
                      'quarter-width font-S fg-primary padding-right-normal'
                    }
                  >
                    Job Type *:
                  </div>
                  <select className={'full-width'} name={'surveyJobType'}>
                    <option value={'PERMANENT'} selected>
                      Permanent
                    </option>
                    <option value={'CONTRACT'}>Contract</option>
                    <option value={'INTERN'}>Internship</option>
                  </select>
                </div>
              </Block>
              <Block>
                <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                  <div
                    className={
                      'quarter-width font-S fg-primary padding-right-normal'
                    }
                  >
                    Job Status *:
                  </div>
                  <select className={'full-width'} name={'surveyJobStatus'}>
                    <option value={'FULL_TIME'} selected>
                      Full Time
                    </option>
                    <option value={'PART_TIME'}>Part Time</option>
                    <option value={'PER_DIME'}>Per Dime</option>
                  </select>
                </div>
              </Block>
              <Block>
                <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                  <div
                    className={
                      'quarter-width font-S fg-primary padding-right-normal'
                    }
                  >
                    Employer *:
                  </div>
                  <select
                    className={'full-width'}
                    name={'surveyCompany'}
                    onChange={this.onEmployerChange}
                  >
                    {companies.map((company, i) => (
                      <option key={i} value={company._id}>
                        {company.name}
                      </option>
                    ))}
                    <option value={'OTHER'}>Other...</option>
                  </select>
                </div>
              </Block>
            </div>
          ) : null}

          {/*todo: if (potentialCompany) do_something */}

          <Block first className={'font-S light italic'}>
            Login Credentials
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'email'}
              placeholder={'Email (this is public) *'}
              type={'email'}
            />
          </Block>
          <Block className={'center'}>
            <Input
              className={'full-width'}
              name={'password'}
              placeholder={'Password *'}
              type={'password'}
            />
          </Block>
          <Block className={'center'}>
            <Input
              className={'full-width'}
              name={'passwordConfirmation'}
              placeholder={'Confirm Password *'}
              type={'password'}
            />
          </Block>

          <Block first className={'center'}>
            <Button className={'full-width'} type={'submit'}>
              Register
            </Button>
          </Block>
          <Block last className={'font-S fg-blackish'}>
            Already have an account? <Anchor to={'/login'}>Login</Anchor>
          </Block>
        </form>
      </Content>
    )
  }

  onEmployerChange(e) {
    this.setState({ potentialCompany: e.target.value === 'OTHER' })
  }

  onSurveyStatusChange(e) {
    this.setState({ status: e.target.value })
  }

  onSubmit(e) {
    e.preventDefault()

    const {
      nameFirst,
      nameLast,
      birthdate,
      gender,
      nationality,
      phoneCode,
      phoneNumber,
      contactInfo,
      surveyGraduationYear,
      department,
      surveyLearning,
      surveyStatus,
      surveySelfEmployedByField,
      surveyJobType,
      surveyJobStatus,
      surveyCompany,
      email,
      password,
      passwordConfirmation
    } = e.target

    if (password.value !== passwordConfirmation.value) {
      this.setState({
        problem: {
          handleInView: true,
          message: 'Password does not match confirmation.'
        }
      })
      return false
    }

    axios
      .post(e.target.action, {
        name: {
          first: nameFirst.value,
          last: nameLast.value
        },
        birthdate: birthdate.value,
        gender: gender.value,
        nationality: nationality.value,
        phone: {
          code: phoneCode.value,
          number: phoneNumber.value
        },
        contactInfo: contactInfo.value,
        department: department.value,
        email: email.value,
        password: password.value,
        survey: {
          graduationYear: surveyGraduationYear.value,
          learning: surveyLearning.value === 'TRUE',
          status: surveyStatus.value,
          selfEmployedByField: surveySelfEmployedByField
            ? surveySelfEmployedByField.value === 'TRUE'
            : undefined,
          jobType: surveyJobType ? surveyJobType.value : undefined,
          jobStatus: surveyJobStatus ? surveyJobStatus.value : undefined,
          company:
            surveyCompany && surveyCompany.value !== 'OTHER'
              ? surveyCompany.value
              : undefined
        }
      })
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success)
          return this.setState({
            problem: Object.assign({ handleInView: true }, data.problem),
            view: this.inputView
          })

        this.setState({ problem: null, view: null })
        this.props.history.push('/login')
      })
      .catch(err => this.setState({ problem: err || 'Unknown error.' }))
  }
}
