import React, { Component, createRef } from 'react'
import axios from 'axios'
import FontAwesome from 'react-fontawesome'
import * as fileDownload from 'js-file-download'

import './Account.scss'
import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Input from '../../common/input/Input'
import TextArea from '../../common/text-area/TextArea'
import Button from '../../common/button/Button'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'
import AccountPic from '../../common/account-pic/AccountPic'
import Anchor from '../../common/anchor/Anchor'

export default class extends Component {
  state = {
    problem: undefined,
    view: undefined,
    companies: [],
    departments: [],
    potentialCompany: false,
    status: this.props.account.survey.status,
    picUploading: false,
    cvUploading: false
  }

  picUploadBtn = createRef()
  picUploadInput = createRef()

  componentDidMount = () => {
    this.setState({ view: this.inputView })

    axios
      .get('/data/company/all')
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (data.success)
          this.setState({
            companies: this.state.companies.concat(data.companies)
          })
      })
      .catch(console.error)

    axios
      .get('/data/department/all')
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (data.success)
          this.setState({
            departments: this.state.departments.concat(data.departments)
          })
      })
      .catch(console.error)
  }

  render = () => {
    const { problem, view } = this.state

    return (
      <Page>
        {(!problem || (problem && problem.handleInView)) &&
        typeof view === 'function' ? (
          view()
        ) : problem ? (
          <Warning problem={problem} />
        ) : (
          <Loading delay />
        )}
      </Page>
    )
  }

  inputView = () => {
    const { account } = this.props
    const {
      problem,
      companies,
      departments,
      potentialCompany,
      status
    } = this.state

    return (
      <div>
        <Content size={'L'} className={'padding-normal'}>
          {problem ? (
            <Block first>
              <Warning
                bomb={() =>
                  this.setState({
                    problem: undefined
                  })
                }
                problem={problem}
                shy
              />
            </Block>
          ) : null}

          <div style={{ display: 'none' }}>
            <form
              method={'POST'}
              action={'/data/account/me/picture'}
              encType={'multipart/form-data'}
              onSubmit={this.onPictureUpload}
            >
              <input
                type={'file'}
                name={'picture'}
                onChange={() => this.picUploadBtn.current.click()}
                ref={this.picUploadInput}
              />
              <button type={'submit'} ref={this.picUploadBtn}>
                Upload
              </button>
            </form>
          </div>

          <Block first className={'center'}>
            {this.state.picUploading ? (
              <Loading />
            ) : (
              <AccountPic
                src={`/data/account/me/picture?${new Date().getTime()}`}
                className={'account-pic'}
                size={'X3L'}
              >
                <Anchor
                  onClick={() => this.picUploadInput.current.click()}
                  className={'change-pic-anchor'}
                  title={'Change Picture'}
                >
                  <span>
                    <FontAwesome name={'camera'} />
                  </span>
                </Anchor>
              </AccountPic>
            )}
          </Block>

          <Block first>
            <h3 className={'center'}>Account Settings</h3>
          </Block>

          <form
            method={'PUT'}
            action={`/data/account/me`}
            onSubmit={this.onSubmit}
          >
            <Block first className={'font-S light italic'}>
              Basic Information
            </Block>
            <Block>
              <div className={'flex'}>
                <Input
                  className={'full-width margin-right-small'}
                  name={'nameFirst'}
                  placeholder={'First Name *'}
                  defaultValue={account.name.first}
                />
                <Input
                  className={'full-width margin-left-small'}
                  name={'nameLast'}
                  placeholder={'Last Name *'}
                  defaultValue={account.name.last}
                />
              </div>
            </Block>
            <Block>
              <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                <div
                  className={
                    'quarter-width font-S fg-primary padding-right-normal'
                  }
                  style={{ paddingBottom: 3 }}
                >
                  Birthdate&nbsp;*:
                </div>
                <Input
                  className={'full-width'}
                  name={'birthdate'}
                  type={'date'}
                  defaultValue={`${new Date(
                    account.birthdate
                  ).getFullYear()}-${(
                    '0' +
                    (new Date(account.birthdate).getMonth() + 1)
                  ).slice(-2)}-${(
                    '0' + new Date(account.birthdate).getDate()
                  ).slice(-2)}`}
                />
              </div>
            </Block>
            <Block>
              <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                <div
                  className={
                    'quarter-width font-S fg-primary padding-right-normal'
                  }
                >
                  Gender&nbsp;*:
                </div>
                <select className={'full-width'} name={'gender'}>
                  <option value={'F'} selected={account.gender === 'F'}>
                    Female
                  </option>
                  <option value={'M'} selected={account.gender === 'M'}>
                    Male
                  </option>
                </select>
              </div>
            </Block>
            <Block>
              <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                <div
                  className={
                    'quarter-width font-S fg-primary padding-right-normal'
                  }
                >
                  Nationality&nbsp;*:
                </div>
                <select className={'full-width'} name={'nationality'}>
                  <option value={'ET'} selected={account.nationality === 'ET'}>
                    Ethiopian
                  </option>
                  <option
                    value={'OTHER'}
                    selected={account.nationality === 'OTHER'}
                  >
                    Other
                  </option>
                </select>
              </div>
            </Block>

            <Block first className={'font-S light italic'}>
              Contact Information
            </Block>
            <Block>
              <div className={'flex'}>
                <Input
                  name={'phoneCode'}
                  placeholder={'Code *'}
                  style={{ width: 56 }}
                  defaultValue={account.phone.code}
                />
                <Input
                  className={'full-width margin-left-normal'}
                  name={'phoneNumber'}
                  placeholder={'Phone Number *'}
                  defaultValue={account.phone.number}
                />
              </div>
            </Block>
            <Block>
              <TextArea
                className={'full-width'}
                name={'contactInfo'}
                placeholder={'Additional Contact Information'}
                defaultValue={account.contactInfo}
              />
            </Block>

            <Block first className={'font-S light italic'}>
              Education
            </Block>
            <Block>
              <Input
                className={'full-width'}
                name={'surveyGraduationYear'}
                placeholder={'Graduation Year *'}
                type={'number'}
                defaultValue={account.survey.graduationYear}
              />
            </Block>
            <Block>
              <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                <div
                  className={
                    'quarter-width font-S fg-primary padding-right-normal'
                  }
                >
                  Department&nbsp;*:
                </div>
                <select className={'full-width'} name={'department'}>
                  {departments.map((department, i) => (
                    <option
                      key={i}
                      value={department._id}
                      selected={account.department === department._id}
                    >
                      {department.name}
                    </option>
                  ))}
                </select>
              </div>
            </Block>
            <Block>
              <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                <div
                  className={
                    'quarter-width font-S fg-primary padding-right-normal'
                  }
                >
                  After AASTU *:
                </div>
                <select className={'full-width'} name={'surveyLearning'}>
                  <option
                    value={'TRUE'}
                    selected={account.survey.learning === true}
                  >
                    Taking some more formal education now or soon.
                  </option>
                  <option
                    value={'FALSE'}
                    selected={account.survey.learning === false}
                  >
                    Not taking any more formal education anytime soon.
                  </option>
                </select>
              </div>
            </Block>

            <Block first className={'font-S light italic'}>
              Work
            </Block>
            <Block>
              <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                <div
                  className={
                    'quarter-width font-S fg-primary padding-right-normal'
                  }
                >
                  Current Status *:
                </div>
                <select
                  className={'full-width'}
                  name={'surveyStatus'}
                  onChange={this.onSurveyStatusChange}
                >
                  <option
                    value={'SELF_EMPLOYED'}
                    selected={account.survey.status === 'SELF_EMPLOYED'}
                  >
                    Self Employed
                  </option>
                  <option
                    value={'COMPANY_EMPLOYED'}
                    selected={account.survey.status === 'COMPANY_EMPLOYED'}
                  >
                    Company Employed
                  </option>
                  <option
                    value={'UNEMPLOYED'}
                    selected={account.survey.status === 'UNEMPLOYED'}
                  >
                    Unemployed
                  </option>
                </select>
              </div>
            </Block>

            {status === 'SELF_EMPLOYED' ? (
              <Block>
                <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                  <div
                    className={
                      'quarter-width font-S fg-primary padding-right-normal'
                    }
                  >
                    Using field? *:
                  </div>
                  <select
                    className={'full-width'}
                    name={'surveySelfEmployedByField'}
                  >
                    <option
                      value={'TRUE'}
                      selected={account.survey.selfEmployedByField === true}
                    >
                      I'm self employed by my field.
                    </option>
                    <option
                      value={'FALSE'}
                      selected={account.survey.selfEmployedByField === false}
                    >
                      I'm using other skills or fields.
                    </option>
                  </select>
                </div>
              </Block>
            ) : status === 'COMPANY_EMPLOYED' ? (
              <div>
                <Block>
                  <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                    <div
                      className={
                        'quarter-width font-S fg-primary padding-right-normal'
                      }
                    >
                      Job Type *:
                    </div>
                    <select className={'full-width'} name={'surveyJobType'}>
                      <option
                        value={'PERMANENT'}
                        selected={account.survey.jobType === 'PERMANENT'}
                      >
                        Permanent
                      </option>
                      <option
                        value={'CONTRACT'}
                        selected={account.survey.jobType === 'CONTRACT'}
                      >
                        Contract
                      </option>
                      <option
                        value={'INTERN'}
                        selected={account.survey.jobType === 'INTERN'}
                      >
                        Internship
                      </option>
                    </select>
                  </div>
                </Block>
                <Block>
                  <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                    <div
                      className={
                        'quarter-width font-S fg-primary padding-right-normal'
                      }
                    >
                      Job Status *:
                    </div>
                    <select className={'full-width'} name={'surveyJobStatus'}>
                      <option
                        value={'FULL_TIME'}
                        selected={account.survey.jobStatus === 'FULL_TIME'}
                      >
                        Full Time
                      </option>
                      <option
                        value={'PART_TIME'}
                        selected={account.survey.jobStatus === 'PART_TIME'}
                      >
                        Part Time
                      </option>
                      <option
                        value={'PER_DIME'}
                        selected={account.survey.jobStatus === 'PER_DIME'}
                      >
                        Per Dime
                      </option>
                    </select>
                  </div>
                </Block>
                <Block>
                  <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                    <div
                      className={
                        'quarter-width font-S fg-primary padding-right-normal'
                      }
                    >
                      Employer *:
                    </div>
                    <select
                      className={'full-width'}
                      name={'surveyCompany'}
                      onChange={this.onEmployerChange}
                    >
                      {companies.map((company, i) => (
                        <option
                          key={i}
                          value={company._id}
                          selected={account.survey.company === company._id}
                        >
                          {company.name}
                        </option>
                      ))}
                      <option
                        value={'OTHER'}
                        selected={!account.survey.company}
                      >
                        Other...
                      </option>
                    </select>
                  </div>
                </Block>
              </div>
            ) : null}

            {/*todo: potentialCompany ? view_something ? : null */}

            <Block first className={'font-S light italic'}>
              Login Credentials
            </Block>
            <Block>
              <Input
                className={'full-width'}
                name={'email'}
                placeholder={'Email (this is public) *'}
                type={'email'}
                defaultValue={account.email}
                disabled
              />
            </Block>
            <Block className={'center'}>
              <Input
                className={'full-width'}
                name={'password'}
                placeholder={'New Password'}
                type={'password'}
              />
            </Block>
            <Block className={'center'}>
              <Input
                className={'full-width'}
                name={'passwordConfirmation'}
                placeholder={'Confirm New Password'}
                type={'password'}
              />
            </Block>

            <Block first last className={'right'}>
              <Button to={'/'} type={'button'}>
                Cancel
              </Button>
              <span className={'padding-left-small'} />
              <Button type={'submit'}>Save Changes</Button>
            </Block>
          </form>
        </Content>

        <div className={'padding-top-normal'} />

        <Content size={'L'}>
          {this.state.cvUploading ? (
            <Loading />
          ) : (
            <form
              method={'POST'}
              action={'/data/account/me/cv'}
              encType={'multipart/form-data'}
              onSubmit={this.onCvUpload}
            >
              <Block first>
                <h3 className={'center'}>Curriculum Vitae</h3>
              </Block>

              <Block className={'center font-S'}>
                <Anchor onClick={this.downloadCV}>
                  Download Your Current CV
                </Anchor>
                &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                <Anchor onClick={this.removeCV}>Remove Your CV</Anchor>
              </Block>
              <Block>
                <div className={'flex'} style={{ alignItems: 'flex-end' }}>
                  <div
                    className={
                      'quarter-width font-S fg-primary padding-right-normal'
                    }
                  >
                    Updated CV?
                  </div>
                  <Input className={'full-width'} name={'cv'} type={'file'} />
                </div>
              </Block>

              <Block last className={'right'}>
                <Button type={'submit'}>Upload CV</Button>
              </Block>
            </form>
          )}
        </Content>
      </div>
    )
  }

  onEmployerChange = e => {
    this.setState({ potentialCompany: e.target.value === 'OTHER' })
  }

  onSurveyStatusChange = e => {
    this.setState({ status: e.target.value })
  }

  onSubmit = e => {
    e.preventDefault()

    const {
      nameFirst,
      nameLast,
      birthdate,
      gender,
      nationality,
      phoneCode,
      phoneNumber,
      contactInfo,
      surveyGraduationYear,
      department,
      surveyLearning,
      surveyStatus,
      surveySelfEmployedByField,
      surveyJobType,
      surveyJobStatus,
      surveyCompany,
      email,
      password,
      passwordConfirmation
    } = e.target

    if (password.value !== passwordConfirmation.value) {
      this.setState({
        problem: {
          handleInView: true,
          message: 'Password does not match confirmation.'
        }
      })
      return false
    }

    axios
      .put(
        e.target.action,
        {
          name: {
            first: nameFirst.value,
            last: nameLast.value
          },
          birthdate: birthdate.value,
          gender: gender.value,
          nationality: nationality.value,
          phone: {
            code: phoneCode.value,
            number: phoneNumber.value
          },
          contactInfo: contactInfo.value,
          department: department.value,
          email: email.value,
          password: password.value,
          survey: {
            graduationYear: surveyGraduationYear.value,
            learning: surveyLearning.value === 'TRUE',
            status: surveyStatus.value,
            selfEmployedByField: surveySelfEmployedByField
              ? surveySelfEmployedByField.value === 'TRUE'
              : undefined,
            jobType: surveyJobType ? surveyJobType.value : undefined,
            jobStatus: surveyJobStatus ? surveyJobStatus.value : undefined,
            company:
              surveyCompany && surveyCompany.value !== 'OTHER'
                ? surveyCompany.value
                : undefined
          }
        },
        { withCredentials: true }
      )
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success)
          return this.setState({
            problem: Object.assign({ handleInView: true }, data.problem),
            view: this.inputView
          })

        this.setState({ problem: null, view: null })
        window.location.href = '/'
      })
      .catch(err => this.setState({ problem: err || 'Unknown error.' }))
  }

  onPictureUpload = e => {
    e.preventDefault()

    const { action, picture } = e.target

    const data = new FormData()
    data.append('picture', picture.files[0])

    this.setState({ picUploading: true })

    axios
      .post(action, data, {
        headers: { 'Content-Type': 'multipart/form-data' },
        withCredentials: true
      })
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success)
          return this.setState({
            problem: Object.assign(data.problem, { handleInView: true }),
            view: this.inputView
          })

        this.setState({ picUploading: false })
      })
      .catch(err => this.setState({ problem: err || 'Picture upload error.' }))

    return false
  }

  onCvUpload = e => {
    e.preventDefault()

    const { action, cv } = e.target

    const data = new FormData()
    data.append('cv', cv.files[0])

    this.setState({ cvUploading: true })

    axios
      .post(action, data, {
        headers: { 'Content-Type': 'multipart/form-data' },
        withCredentials: true
      })
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success)
          return this.setState({
            problem: Object.assign(data.problem, { handleInView: true }),
            view: this.inputView
          })

        this.setState({ cvUploading: false })
      })
      .catch(err => this.setState({ problem: err || 'Picture upload error.' }))

    return false
  }

  downloadCV = () => {
    axios({
      url: '/data/account/me/cv/download',
      method: 'GET',
      responseType: 'blob'
    }).then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]))
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', 'cv.pdf')
      document.body.appendChild(link)
      link.click()
    })
  }

  removeCV = () => {
    axios
      .delete('/data/account/me/cv')
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (
          data.success === false &&
          data.problem &&
          data.problem.code === 'NOT_FOUND'
        )
          alert('You no CV uploaded on this system yet.')
      })
      .catch(console.error)
  }
}
