import React from 'react'
import axios from 'axios'

import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Page from '../../common/page/Page'
import Anchor from '../../common/anchor/Anchor'
import Button from '../../common/button/Button'
import Card from '../../common/card/Card'
import FlexSpacer from '../../common/flex-spacer/FlexSpacer'

export default class extends React.Component {
  constructor() {
    super()
    this.state = {
      vacancies: [],
      announcements: []
    }
    this.vacancyList = this.vacancyList.bind(this)
    this.announcementsList = this.announcementsList.bind(this)
  }
  componentDidMount() {
    this.fetchVacancies()
    this.fetchAnnouncements()
  }

  render() {
    let { vacancies } = this.state
    let { announcements } = this.state

    return (
      <Page className={'center'} adaptive>
        {vacancies.map((vac, i) => this.vacancyList(vac))}
        {announcements.map((ann, i) => this.announcementsList(ann))}
      </Page>
    )
  }

  vacancyList(vac) {
    return (
      <Card
        size={'S'}
        className={'inline-block margin-normal'}
        title={
          <h3 className={'fg-primary'}>
            {vac.jobTitle.substring(0, 20).toUpperCase()}
          </h3>
        }
        subtitle={[
          <h4>Location @ {vac.jobLocation}</h4>,
          <h4>Salary {vac.salary}</h4>,
          <h4>Quantity {vac.quantity}</h4>
        ]}
        actions={
          <Button to={`/vacancy/${vac._id}`} primary>
            More
          </Button>
        }
      >
        <p className={'fg-blackish'}>{vac.description.substring(0, 100)}...</p>
        <h3 className={'fg-blackish'}>
          Applicants <span className={'fg-primary'}>&#9733;</span>
          {vac.applicants.length !== 0 ? vac.applicants.length : ''}
        </h3>
      </Card>
    )
  }
  announcementsList(ann) {
    console.log(ann)
    return (
      <Card
        size={'XL'}
        className={'block margin-vertical-normal'}
        title={
          <h3 className={'fg-primary'}>
            {ann.title.substring(0, 20).toUpperCase()}
          </h3>
        }
        subtitle={[
          <h4>{ann.location}</h4>,
          <h4>{new Date(ann.time).toLocaleDateString()}</h4>,
          <span>@{new Date(ann.time).toLocaleTimeString()}</span>
        ]}
        actions={[
          <FlexSpacer />,
          <Button to={`/announcement/${ann._id}`} primary>
            View
          </Button>
        ]}
      >
        <p className={'fg-blackish'}>{ann.description.substring(0, 100)}...</p>
        <h3 className={'fg-blackish'}>
          Interested <span className={'fg-primary'}>&#9733;</span>
          {ann.interestedCount !== 0 ? ann.interestedCount : ''}
        </h3>
      </Card>
    )
  }

  fetchVacancies = () => {
    axios
      .get('data/vacancy/list/10/start/0')
      .then(res => {
        if (res.data.success) {
          this.setState({
            vacancies: this.state.vacancies.concat(res.data.vacancies)
          })
        } else {
          return this.setState({
            problem: Object.assign(res.data.problem, { handleInView: true })
          })
        }
      })
      .catch(e => {
        this.setState({
          problem: e
        })
      })
  }
  fetchAnnouncements = () => {
    axios
      .get('/data/announcement/list/8/start/0')
      .then(res => {
        if (res.data.success) {
          this.setState({
            announcements: this.state.announcements.concat(
              res.data.announcements
            )
          })
        } else {
          return this.setState({
            problem: Object.assign(res.data.problem, { handleInView: true }),
            view: this.inputView
          })
        }
      })
      .catch(e => {
        this.setState({
          problem: e
        })
      })
  }
}
