import React, { Component } from 'react'
import axios from 'axios'
import FontAwesome from 'react-fontawesome'

import './view.css'

import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Button from '../../common/button/Button'

import FlexSpacer from '../../common/flex-spacer/FlexSpacer'
import Anchor from '../../common/anchor/Anchor'

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      interested: false,
      numberOfInterested: undefined,
      announcement: {},
      problem: undefined,
      file: {}
    }
    this.fetchAnnounce = this.fetchAnnounce.bind(this)
    this.fetchFile = this.fetchFile.bind(this)
  }

  componentDidMount() {
    this.fetchAnnounce()
    this.fetchFile()
  }

  handleInterest = () => {
    axios
      .put(`/data/announcement/${this.props.match.params._id}/interested`)
      .then(res => {
        if (res.data.success) {
          this.setState({
            numberOfInterested: res.data.numberOfInterested
          })
        } else {
          this.setState({
            problem: res.data.problem
          })
        }
      })
      .catch(e => {
        this.setState({
          problem: e
        })
      })
  }

  render() {
    return (
      <Page className={'center'}>
        <Content size={'L'} className={''}>
          <Block first last className={'full-width'}>
            <div>
              <img
                src={`/data/announcement/${
                  this.props.match.params._id
                }/attachment`}
                alt="cannot load picture"
              />
            </div>
          </Block>
          <Block>
            <h1 className={'fg-primary '}> {this.state.announcement.title} </h1>
            <div>
              <h3 className={'fg-blackish'}>
                {' '}
                {this.state.announcement.location}
              </h3>
              <h5>
                {new Date(this.state.announcement.time).toLocaleDateString()}
              </h5>
              <h5>
                {new Date(this.state.announcement.time).toLocaleTimeString()}
              </h5>
            </div>
          </Block>
          <Block>
            <div>
              <p className={'fg-blackish'}>
                {this.state.announcement.description}
              </p>
            </div>
          </Block>
          <Block className={'center'} last>
            <div className={'flex'}>
              <h3 className={'light'}>
                Interested &nbsp; &#9733;{this.state.numberOfInterested}
              </h3>
              <FlexSpacer />
              <Anchor onClick={this.handleInterest}>
                <FontAwesome name={'thumbs-up'} size={'3x'} />
              </Anchor>
            </div>
          </Block>
        </Content>
      </Page>
    )
  }
  //&#x1f44d;
  fetchAnnounce() {
    axios
      .get(`/data/announcement/${this.props.match.params._id}`)
      .then(response => {
        if (response.data.success) {
          this.setState({
            announcement: response.data.announcements,
            numberOfInterested: response.data.announcements.interestedCount
          })
        } else {
          this.setState({
            problem: response.data.problem
          })
        }
      })
      .catch(err => {
        this.setState({
          problem: err
        })
      })
  }

  fetchFile() {
    axios
      .get(`/data/announcement/${this.props.match.params._id}/attachment`)
      .then(response => {
        if (response.data.success) {
          this.setState({
            file: response.data
          })
        } else {
          this.setState({
            problem: response.data.problem
          })
        }
      })
      .catch(err => {
        this.setState({
          problem: err
        })
      })
  }
}
