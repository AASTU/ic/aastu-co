import React, { Component } from 'react'
import Loadable from 'react-loadable'
import axios from 'axios'
import { BrowserRouter, Switch } from 'react-router-dom'

import './App.scss'
import Loading from '../common/loading/Loading'
import BigHead from './big-head/BigHead'
import NavBar from './nav-bar/NavBar'
import PageRoute from './page-route/PageRoute'
import LittleFinger from './little-finger/LittleFinger'

const Register = Loadable({
  loader: () => import('../pages/register/Register'),
  loading: Loading
})
const Login = Loadable({
  loader: () => import('../pages/login/Login'),
  loading: Loading
})
const Reset = Loadable({
  loader: () => import('../pages/reset/Reset'),
  loading: Loading
})
const Account = Loadable({
  loader: () => import('../pages/account/Account'),
  loading: Loading
})
const Home = Loadable({
  loader: () => import('../pages/home/Home'),
  loading: Loading
})
const Announcements = Loadable({
  loader: () => import('../pages/announcements/Announcements'),
  loading: Loading
})
const AnnouncementView = Loadable({
  loader: () => import('../pages/announcement-view/AnnouncementView'),
  loading: Loading
})
const Vacancies = Loadable({
  loader: () => import('../pages/vacancies/Vacancies'),
  loading: Loading
})
const VacancyView = Loadable({
  loader: () => import('../pages/vacancy-view/VacancyView'),
  loading: Loading
})
const NotFound = Loadable({
  loader: () => import('../pages/not-found/NotFound'),
  loading: Loading
})

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      problem: null,
      account: undefined,
      isNavOpen: false
    }

    this.reset = this.reset.bind(this)
    this.openNav = this.openNav.bind(this)
    this.closeNav = this.closeNav.bind(this)
    this.logout = this.logout.bind(this)
  }

  componentDidMount() {
    axios
      .get('/data/account/me', { withCredentials: true })
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (data.success && data.account)
          this.setState({ account: data.account })
        else return Promise.reject(data.problem)
      })
      .catch(err => {
        if (err && err.code === 'NOT_LOGGED_IN')
          this.setState({ account: null })
        else this.setState({ problem: err || 'Unknown error.' })
      })
  }

  render() {
    const { account, isNavOpen, problem } = this.state
    const auth = { account: account || {}, exact: true }

    return problem || account === undefined ? (
      <Loading delay problem={problem} />
    ) : (
      <BrowserRouter>
        <div className={'bg'}>
          <BigHead
            account={account}
            onNavOpen={this.openNav}
            onLogout={this.logout}
          />

          <NavBar
            account={account}
            onClose={this.closeNav}
            onNavClose={this.closeNav}
            onLogout={this.logout}
            open={isNavOpen}
          />

          <div
            style={{
              minHeight: window.innerHeight ? window.innerHeight - 140 : 420
            }}
          >
            <Switch>
              <PageRoute exact path={'/register'} component={Register} />
              <PageRoute exact path={'/login'} component={Login} />
              <PageRoute exact path={'/reset'} component={Reset} />
              <PageRoute {...auth} path={'/account'} component={Account} />
              <PageRoute {...auth} path={'/'} component={Home} />
              <PageRoute
                {...auth}
                path={'/announcements'}
                component={Announcements}
              />
              <PageRoute
                {...auth}
                path={'/announcement/:_id'}
                component={AnnouncementView}
              />
              <PageRoute {...auth} path={'/vacancies'} component={Vacancies} />
              <PageRoute
                {...auth}
                path={'/vacancy/:_id'}
                component={VacancyView}
              />
              <PageRoute component={NotFound} />
            </Switch>
          </div>

          <LittleFinger />
        </div>
      </BrowserRouter>
    )
  }

  reset() {
    this.setState({
      problem: null,
      account: undefined
    })
  }

  openNav() {
    this.setState({ isNavOpen: true })
  }

  closeNav() {
    this.setState({ isNavOpen: false })
  }

  logout() {
    this.reset()
    axios
      .get('/data/auth/logout', { withCredentials: true })
      .then(resp => {
        const data = resp.data
        if (data.success) {
          this.setState({ account: undefined })
          window.location.replace('/')
        } else return Promise.reject(data.problem)
      })
      .catch(err => this.setState({ problem: err }))
  }
}
