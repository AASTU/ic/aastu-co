import React from 'react'

import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Button from '../../common/button/Button'
import Anchor from '../../common/anchor/Anchor'

export default ({ location, history }) => (
  <Page top={'10%'}>
    <Content size={'S'}>
      <Block first className={'bold'}>
        404 (Not Found)
      </Block>

      <Block>
        Sorry! The requested path, <code>{location.pathname}</code>, was not
        found.
      </Block>

      <Block last>
        <Button onClick={history.goBack}>Go Back</Button>
        <span className={'padding-left-normal'} />
        <Anchor to={'/'} button>
          Take Me Home
        </Anchor>
      </Block>
    </Content>
  </Page>
)
