import React, { Component } from 'react'
import axios from 'axios'
import qs from 'qs'

import Page from '../../common/page/Page'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Logo from '../../common/logo/Logo'
import Input from '../../common/input/Input'
import TextArea from '../../common/text-area/TextArea'
import Button from '../../common/button/Button'
import Anchor from '../../common/anchor/Anchor'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      problem: undefined,
      view: undefined
    }

    this.inputView = this.inputView.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount() {
    this.setState({ view: this.inputView })
  }

  render() {
    const { problem, view } = this.state

    return (
      <Page>
        {(!problem || (problem && problem.handleInView)) &&
        typeof view === 'function' ? (
          view()
        ) : problem ? (
          <Warning problem={problem} />
        ) : (
          <Loading delay />
        )}
      </Page>
    )
  }

  inputView() {
    const { problem } = this.state

    return (
      <Content size={'M'}>
        {problem ? (
          <Block first>
            <Warning
              bomb={() => this.setState({ problem: undefined })}
              problem={problem}
              shy
            />
          </Block>
        ) : null}

        <Block first className={'center'}>
          <Logo size={'M'} />
        </Block>

        <Block>
          <h3 className={'center'}>Register Your Company</h3>
        </Block>

        <form
          method={'POST'}
          action={`/data/auth/register${qs.stringify(
            qs.parse(window.location.search, { ignoreQueryPrefix: true }),
            { addQueryPrefix: true }
          )}`}
          onSubmit={this.onSubmit}
        >
          <Block first className={'font-S light italic'}>
            Basic Information
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'name'}
              placeholder={'Company Name *'}
            />
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'address'}
              placeholder={'Company Address *'}
            />
          </Block>
          <Block>
            <TextArea
              className={'full-width'}
              name={'links'}
              placeholder={'Web Links (one per line)'}
            />
          </Block>

          <Block first className={'font-S light italic'}>
            Contact Information
          </Block>
          <Block>
            <div className={'flex'}>
              <Input
                name={'phoneCode'}
                placeholder={'Code *'}
                style={{ width: 56 }}
              />
              <Input
                className={'full-width margin-left-normal'}
                name={'phoneNumber'}
                placeholder={'Phone Number *'}
              />
            </div>
          </Block>
          <Block>
            <TextArea
              className={'full-width'}
              name={'contactInfo'}
              placeholder={'Additional Contact Information'}
            />
          </Block>

          <Block first className={'font-S light italic'}>
            More Detail
          </Block>
          <Block>
            <TextArea
              name={'description'}
              placeholder={'Brief Company Description'}
              className={'full-width'}
              style={{ minHeight: 220 }}
            />
          </Block>

          <Block first className={'font-S light italic'}>
            Login Credentials
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'email'}
              placeholder={'Email (this is public) *'}
              type={'email'}
            />
          </Block>
          <Block className={'center'}>
            <Input
              className={'full-width'}
              name={'password'}
              placeholder={'Password *'}
              type={'password'}
            />
          </Block>
          <Block className={'center'}>
            <Input
              className={'full-width'}
              name={'passwordConfirmation'}
              placeholder={'Confirm Password *'}
              type={'password'}
            />
          </Block>

          <Block first className={'center'}>
            <Button className={'full-width'} type={'submit'}>
              Register
            </Button>
          </Block>
          <Block last className={'font-S fg-blackish'}>
            Already have an account? <Anchor to={'/login'}>Login</Anchor>
          </Block>
        </form>
      </Content>
    )
  }

  onSubmit(e) {
    e.preventDefault()

    const {
      name,
      address,
      links,
      phoneCode,
      phoneNumber,
      contactInfo,
      description,
      email,
      password,
      passwordConfirmation
    } = e.target

    if (password.value !== passwordConfirmation.value) {
      this.setState({
        problem: {
          handleInView: true,
          message: 'Password does not match confirmation.'
        }
      })
      return false
    }

    axios
      .post(e.target.action, {
        name: name.value,
        address: address.value,
        links: links.value.split('\n'),
        phone: {
          code: phoneCode.value,
          number: phoneNumber.value
        },
        contactInfo: contactInfo.value,
        description: description.value,
        email: email.value,
        password: password.value
      })
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (!data.success)
          return this.setState({
            problem: Object.assign(data.problem, { handleInView: true }),
            view: this.inputView
          })

        this.setState({ problem: null, view: null })
        this.props.history.push('/login')
      })
      .catch(err => this.setState({ problem: err || 'Unknown error.' }))
  }
}
