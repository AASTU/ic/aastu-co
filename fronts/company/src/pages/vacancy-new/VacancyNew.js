import React, { Component } from 'react'
import FontAwesome from 'react-fontawesome'
import axios from 'axios'

import Page from '../../common/page/Page'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Input from '../../common/input/Input'
import TextArea from '../../common/text-area/TextArea'
import Button from '../../common/button/Button'
import FlexSpacer from '../../common/flex-spacer/FlexSpacer'

// todo: fix targetDepartments selection
// todo: Next -> to add profile pics (use axios instead of <form>)
export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      problem: undefined,
      view: undefined,
      departments: [],
      target: false,
      vacancy: undefined
    }

    this.inputView = this.inputView.bind(this)
    this.onTargetChange = this.onTargetChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount() {
    axios
      .get('/data/department/all')
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (data.success)
          this.setState({
            departments: this.state.departments.concat(data.departments)
          })
      })
      .catch(console.error)

    this.setState({ view: this.inputView })
  }

  render() {
    const { problem, view } = this.state

    return (
      <Page>
        {(!problem || (problem && problem.handleInView)) &&
        typeof view === 'function' ? (
          view()
        ) : problem ? (
          <Warning problem={problem} />
        ) : (
          <Loading delay />
        )}
      </Page>
    )
  }

  inputView() {
    const { problem, target } = this.state

    return (
      <Content size={'M'}>
        {problem ? (
          <Block first>
            <Warning
              bomb={() => this.setState({ problem: undefined })}
              problem={problem}
              shy
            />
          </Block>
        ) : null}

        <Block first>
          <h2>Add a New Vacancy</h2>
        </Block>

        <form
          method={'POST'}
          action={'/data/vacancy/new'}
          onSubmit={this.onSubmit}
        >
          <Block first className={'font-XS bold uppercase'}>
            Basic Information:
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'jobTitle'}
              placeholder={'Job Title *'}
            />
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'quantity'}
              placeholder={'Quantity *'}
              type={'number'}
            />
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'salary'}
              placeholder={'Salary *'}
            />
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'jobLocation'}
              placeholder={'Job Location *'}
            />
          </Block>
          <Block>
            <div className="font-S fg-primary">Job Type *:</div>
            <select className={'full-width'} name={'jobType'}>
              <option value={'EMPLOYEE'} selected>
                Employee
              </option>
              <option value={'CONTRACT'}>Contract</option>
              <option value={'INTERN'}>Intern</option>
            </select>
          </Block>
          <Block>
            <div className="font-S fg-primary">Job Status *:</div>
            <select className={'full-width'} name={'jobStatus'}>
              <option value={'FULL_TIME'} selected>
                Full Time
              </option>
              <option value={'PART_TIME'}>Part Time</option>
              <option value={'PER_DIEM'}>Per Diem</option>
            </select>
          </Block>

          <Block first className={'font-XS bold uppercase'}>
            Specific Target:
          </Block>
          <Block>
            <label>
              <Input
                checked={target}
                onChange={this.onTargetChange}
                type={'checkbox'}
              />
              <span className={'padding-left-small'} />
              Target some departments?
            </label>
            <br />
            {!target || (
              <select
                className={'margin-top-small full-width'}
                name={'targetDepartments'}
                multiple
                style={{ minHeight: 140 }}
              >
                {this.state.departments.map((department, i) => (
                  <option key={i} value={department._id}>
                    {department.name}
                  </option>
                ))}
              </select>
            )}
          </Block>

          <Block first className={'font-XS bold uppercase'}>
            More Detail:
          </Block>
          <Block>
            <TextArea
              className={'full-width'}
              name={'description'}
              placeholder={'Description *'}
              style={{ minHeight: 280 }}
            />
          </Block>

          <Block last>
            <div className={'flex'}>
              <Button type={'reset'}>Clear</Button>

              <FlexSpacer />

              <Button to={'/'}>Cancel</Button>
              <span className={'padding-left-small'} />
              <Button type={'submit'}>
                Add<span className={'padding-left-normal'} />
                <FontAwesome name={'arrow-right'} />
              </Button>
            </div>
          </Block>
        </form>
      </Content>
    )
  }

  onTargetChange(e) {
    this.setState({ target: e.target.checked })
  }

  onSubmit(e) {
    e.preventDefault()

    const {
      jobTitle,
      quantity,
      salary,
      jobLocation,
      jobType,
      jobStatus,
      targetDepartments,
      description
    } = e.target

    this.setState({ view: null })

    axios
      .post(
        e.target.action,
        {
          jobTitle: jobTitle.value,
          quantity: quantity.value,
          salary: salary.value,
          jobLocation: jobLocation.value,
          jobType: jobType.value,
          jobStatus: jobStatus.value,
          targetDepartments: targetDepartments
            ? targetDepartments.value
            : undefined,
          description: description.value
        },
        { withCredentials: true }
      )
      .then(resp => resp.data)
      .then(data => {
        console.log(data)
        if (!data.success)
          this.setState({
            problem: Object.assign(data.problem, { handleInView: true }),
            view: this.inputView
          })
        else
          this.setState({
            problem: null,
            view: this.inputView,
            vacancy: data.vacancy
          })
      })
      .catch(err => this.setState({ problem: err }))
  }
}
