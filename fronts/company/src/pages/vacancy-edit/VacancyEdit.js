import React, { Component } from 'react'
import axios from 'axios'

import Page from '../../common/page/Page'
import Warning from '../../common/warning/Warning'
import Loading from '../../common/loading/Loading'
import Content from '../../common/content/Content'
import Block from '../../common/block/Block'
import Input from '../../common/input/Input'
import TextArea from '../../common/text-area/TextArea'
import Button from '../../common/button/Button'

// todo: fix targetDepartments selection
export default class extends Component {
  state = {
    problem: undefined,
    view: undefined,
    departments: [],
    target: false,
    vacancy: {
      targetDepartments: []
    }
  }

  componentDidMount() {
    this.fetchState()
  }

  render() {
    const { problem, view } = this.state

    return (
      <Page>
        {(!problem || (problem && problem.handleInView)) &&
        typeof view === 'function' ? (
          view()
        ) : problem ? (
          <Warning problem={problem} />
        ) : (
          <Loading delay />
        )}
      </Page>
    )
  }

  inputView = () => {
    const { problem, target, vacancy } = this.state

    return (
      <Content size={'M'}>
        {problem ? (
          <Block first>
            <Warning
              bomb={() => this.setState({ problem: undefined })}
              problem={problem}
              shy
            />
          </Block>
        ) : null}

        <Block first>
          <h2>Edit This Vacancy</h2>
        </Block>

        <form
          method={'PUT'}
          action={`/data/vacancy/${vacancy._id}/edit`}
          onSubmit={this.onSubmit}
        >
          <Block first className={'font-XS bold uppercase'}>
            Basic Information:
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'jobTitle'}
              placeholder={'Job Title *'}
              defaultValue={vacancy.jobTitle}
            />
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'quantity'}
              placeholder={'Quantity *'}
              type={'number'}
              defaultValue={vacancy.quantity}
            />
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'salary'}
              placeholder={'Salary *'}
              defaultValue={vacancy.salary}
            />
          </Block>
          <Block>
            <Input
              className={'full-width'}
              name={'jobLocation'}
              placeholder={'Job Location *'}
              defaultValue={vacancy.jobLocation}
            />
          </Block>
          <Block>
            <div className="font-S fg-primary">Job Type *:</div>
            <select className={'full-width'} name={'jobType'}>
              <option
                value={'EMPLOYEE'}
                selected={vacancy.jobType === 'EMPLOYEE'}
              >
                Employee
              </option>
              <option
                value={'CONTRACT'}
                selected={vacancy.jobType === 'CONTRACT'}
              >
                Contract
              </option>
              <option value={'INTERN'} selected={vacancy.jobType === 'INTERN'}>
                Intern
              </option>
            </select>
          </Block>
          <Block>
            <div className="font-S fg-primary">Job Status *:</div>
            <select className={'full-width'} name={'jobStatus'}>
              <option
                value={'FULL_TIME'}
                selected={vacancy.jobStatus === 'FULL_TIME'}
              >
                Full Time
              </option>
              <option
                value={'PART_TIME'}
                selected={vacancy.jobStatus === 'PART_TIME'}
              >
                Part Time
              </option>
              <option
                value={'PER_DIME'}
                selected={vacancy.jobStatus === 'PER_DIME'}
              >
                Per Dime
              </option>
            </select>
          </Block>

          <Block first className={'font-XS bold uppercase'}>
            Specific Target:
          </Block>
          <Block>
            <label>
              <Input
                checked={target}
                onChange={this.onTargetChange}
                type={'checkbox'}
              />
              <span className={'padding-left-small'} />
              Target some departments?
            </label>
            <br />
            {!target || (
              <select
                className={'margin-top-small full-width'}
                name={'targetDepartments'}
                multiple={true}
                style={{ minHeight: 140 }}
              >
                {this.state.departments.map((department, i) => (
                  <option
                    key={i}
                    selected={vacancy.targetDepartments.includes(department)}
                    value={department._id}
                  >
                    {department.name}
                  </option>
                ))}
              </select>
            )}
          </Block>

          <Block first className={'font-XS bold uppercase'}>
            More Detail:
          </Block>
          <Block>
            <TextArea
              className={'full-width'}
              name={'description'}
              placeholder={'Description *'}
              style={{ minHeight: 280 }}
              defaultValue={vacancy.description}
            />
          </Block>

          <Block last className={'right'}>
            <Button to={'/'}>Cancel</Button>
            <span className={'padding-left-small'} />
            <Button type={'submit'}>Save Changes</Button>
          </Block>
        </form>
      </Content>
    )
  }

  onTargetChange = e => {
    this.setState({ target: e.target.checked })
  }

  fetchState = () => {
    axios
      .get('/data/department/all')
      .then(resp => Promise.resolve(resp.data))
      .then(data => {
        if (data.success)
          this.setState({
            departments: this.state.departments.concat(data.departments)
          })
      })
      .catch(console.error)

    axios.get(`/data/vacancy/${this.props.match.params._id}`).then(res => {
      this.setState({
        view: this.inputView,
        target: this.state.vacancy.targetDepartments.length,
        vacancy: res.data.vacancy
      })
    })
  }

  onSubmit = e => {
    e.preventDefault()

    const {
      jobTitle,
      salary,
      quantity,
      jobLocation,
      jobType,
      jobStatus,
      targetDepartments,
      description
    } = e.target

    this.setState({ view: null })
    axios
      .put(
        `/data/vacancy/${this.props.match.params._id}`,
        {
          jobTitle: jobTitle.value,
          quantity: quantity.value,
          salary: salary.value,
          jobLocation: jobLocation.value,
          jobStatus: jobStatus.value,
          jobType: jobType.value,
          targetDepartments: targetDepartments.value, // todo: fix this
          description: description.value
        },
        { withCredentials: true }
      )
      .then(resp => resp.data)
      .then(data => {
        if (!data.success)
          return this.setState({
            problem: this.setState({ problem: data.problem })
          })

        this.setState({
          problem: null,
          vacancy: data.vacancy,
          view: this.inputView
        })
        this.props.history.push('/vacancies')
      })
      .catch(e => this.setState({ problem: e }))
  }
}
