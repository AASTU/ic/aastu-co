import express from 'express'
import { join } from 'path'

import config from './configs/config'
import './configs/passport.config'
import setup from '../common/configs/express.config'
import mongooseSetup from '../common/configs/mongoose.config'
import { serveFront } from '../common/providers/serveFront'

import '../common/models/admin.model'
import '../common/models/announcement.model'
import '../common/models/company.model'
import '../common/models/department.model'
import '../common/models/graduate.model'
import '../common/models/key.model'
import '../common/models/potential.model'
import '../common/models/vacancy.model'

import { accountRouter } from './routers/account.router'
import { authRouter } from './routers/auth.router'
import { companyRouter } from './routers/company.router'
import { departmentRouter } from './routers/department.router'
import { vacancyRouter } from './routers/vacancy.router'
import { graduateRouter } from './routers/graduate.router'

const companyApp = express()
setup(companyApp, config)
mongooseSetup(config)

companyApp.use('/data/account', accountRouter)
companyApp.use('/data/auth', authRouter)
companyApp.use('/data/company', companyRouter)
companyApp.use('/data/department', departmentRouter)
companyApp.use('/data/vacancy', vacancyRouter)
companyApp.use('/data/graduate', graduateRouter)

serveFront(companyApp, {
  rootDirectory: join(process.cwd(), 'fronts', 'company', 'build')
})

companyApp.listen(config.dev.port, () =>
  console.log(`Listening on http://localhost:${config.dev.port} (company app)`)
)
/*
import { CompanyModel } from '../common/models/company.model'
import { VacancyModel } from '../common/models/vacancy.model'

const company = new CompanyModel() as any
company.name = 'Kelal'
company.description =
  'built on the awesome D3 and React.js plugins. It helps producing responsive charts and they are highly customizable..'
company.address = 'AASTU'
company.links = ['https://kelal.tech/', 'http://facbook.com/kelalTech']
company.email = 'contact@kelal.tech'
company.phone = { code: 251, number: '0923637040' }
company.contactInfo = 'AASTU, Addis Ababa, Ethiopia'
company
  .setPassword('password')
  .then(() => company.save())
  .then(() => console.log('company kelal created'))
  .catch(error => console.log(error))

const company1 = new CompanyModel() as any
company1.name = 'Google'
company1.description =
  'built on the awesome D3 and React.js plugins. It helps producing responsive charts and they are highly customizable..'
company1.address = 'USA'
company1.links = ['https://google.tech/', 'http://facbook.com/google.io']
company1.email = 'contact@google.tech'
company1.phone = { code: 251, number: '2343546576879' }
company1.contactInfo = 'Google, New york, USA'
company1
  .setPassword('password')
  .then(() => company1.save())
  .then(() => console.log('company google created'))
  .catch(error => console.log(error))

const company2 = new CompanyModel() as any
company2.name = 'CNET'
company2.description =
  'built on the awesome D3 and React.js plugins. It helps producing responsive charts and they are highly customizable..'
company2.address = 'ADDIS '
company2.links = ['https://CNET.tech/', 'http://facbook.com/CNET.io']
company2.email = 'contact@CNET.tech'
company2.phone = { code: 251, number: '09347622367' }
company2.contactInfo = 'CNET, Addis Ababa, Ethiopia'
company2
  .setPassword('password')
  .then(() => company2.save())
  .then(() => console.log('company CNET created'))
  .catch(error => console.log(error))

const vacancy = new VacancyModel() as any
vacancy.jobTitle = 'Web Developer'
vacancy.salary = '200000 USD/year'
vacancy.company = '5ad056752c5a4a11740fad0e'
vacancy.targetDepartments = [
  '5ad057b5e620ef3520e0fa44',
  '5ad057b5e620ef3520e0fa46'
]
vacancy.jobLocation = 'Kelal HQ, Addis Ababa'
vacancy.quantity = 10
vacancy.jobType = 'EMPLOYEE'
vacancy.jobStatus = 'FULL_TIME'
vacancy.description =
  "The documents returned from query population become fully functional, removeable, saveable documents unless the lean option is specified. Do not confuse them with sub docs. Take caution when calling its remove method because you'll be removing it from the database, not just the array."
vacancy.applicants = [
  { applicant: '5ad05d7d6d2c74306c155fbb', recommended: true },
  { applicant: '5ad05d7d6d2c74306c155fb9', recommended: false },
  { applicant: '5ad05d7d6d2c74306c155fba', recommended: false }
]
vacancy
  .save()
  .then(() => console.log('success'))
  .catch(console.error)

const vacancy1 = new VacancyModel() as any
vacancy1.jobTitle = 'CEO'
vacancy1.salary = '200000 USD/year'
vacancy1.company = '5ad056752c5a4a11740fad10'
vacancy1.targetDepartments = ['5ad057b5e620ef3520e0fa45']
vacancy1.jobLocation = 'Addis Ababa,Ethiopia'
vacancy1.quantity = 15
vacancy1.jobType = 'CONTRACT'
vacancy1.jobStatus = 'PART_TIME'
vacancy1.description =
  "The documents returned from query population become fully functional, removeable, saveable documents unless the lean option is specified. Do not confuse them with sub docs. Take caution when calling its remove method because you'll be removing it from the database, not just the array."
vacancy1.applicants = [
  { applicant: '5ad05d7d6d2c74306c155fbb', recommended: true },
  { applicant: '5ad05d7d6d2c74306c155fb9', recommended: true },
  { applicant: '5ad05d7d6d2c74306c155fba', recommended: false }
]
vacancy1
  .save()
  .then(() => console.log('success'))
  .catch(console.error)
*/
/*const vacancy2 = new VacancyModel() as any
vacancy2.jobTitle = 'Testing Apply pls work'
vacancy2.salary = '100000 USD/year'
vacancy2.company = '5ad056752c5a4a11740fad0e'
vacancy2.targetDepartments = [
  '5ad057b5e620ef3520e0fa45',
  '5ad057b5e620ef3520e0fa44'
]
vacancy2.jobLocation = 'Mekele,Ethiopia'
vacancy2.quantity = 15
vacancy2.jobType = 'INTERN'
vacancy2.jobStatus = 'PER_DIEM'
vacancy2.description =
  "The documents returned from query population become fully functional, removeable, saveable documents unless the lean option is specified. Do not confuse them with sub docs. Take caution when calling its remove method because you'll be removing it from the database, not just the array."
vacancy2.applicants = [
  { applicant: '5ad05d7d6d2c74306c155fbb', recommended: true },
  { applicant: '5ad05d7d6d2c74306c155fba', recommended: false }
]
vacancy2
  .save()
  .then(() => console.log('success'))
  .catch(console.error)*/
