export default {
  name: 'aastu-co-company',
  dev: {
    port: Number(process.env.SERVER_PORT) || 2222,
    db: process.env.DB_LINK || 'mongodb://127.0.0.1/aastu-co'
  },
  prod: {
    //TODO for production
  }
}
