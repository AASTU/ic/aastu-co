import { serializeUser, deserializeUser, use } from 'passport'
import { Strategy } from 'passport-local'
import { Document } from 'mongoose'

import { CompanyModel } from '../../common/models/company.model'

serializeUser((company: Document, done) => done(null, company._id))
deserializeUser((_id, done) => CompanyModel.findById(_id, done))

use(
  'local-company',
  new Strategy(
    {
      usernameField: 'email'
    },
    (email, password, done) =>
      CompanyModel.findOne({ email: email })
        .then(async company =>
          done(
            null,
            !company || !await (company as any).isPasswordCorrect(password)
              ? false
              : company
          )
        )
        .catch(done)
  )
)
