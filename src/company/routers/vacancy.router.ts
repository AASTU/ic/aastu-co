import { Router } from 'express'

import { successFormat, problemFormat } from '../../common/util'
import { VacancyService } from '../../common/providers/vacancy.service'

const vacancyRouter = Router()

// POST /data/vacancy/new
vacancyRouter.post('/new', async (req, res) => {
  try {
    await VacancyService.add(req.body, req.user ? req.user.id : undefined)
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/:_id
vacancyRouter.get('/:_id', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancy: await VacancyService.getByCompany(
          req.params._id,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/list/:count/start/0
vacancyRouter.get('/list/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.listByCompany(
          req.params.count,
          null,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/list/:count/start/:since
vacancyRouter.get('/list/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.listByCompany(
          req.params.count,
          req.params.since,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/search/:count/start/0?term=:term
vacancyRouter.get('/search/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.searchByCompany(
          req.query.term,
          req.params.count,
          null,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/search/:count/start/:since?term=:term
vacancyRouter.get('/search/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.searchByCompany(
          req.query.term,
          req.params.count,
          req.params.since,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// PUT /data/vacancy/:_id
vacancyRouter.put('/:_id', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancy: await VacancyService.edit(req.params._id, req.body)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// DELETE /data/vacancy/:_id
vacancyRouter.delete('/:_id', async (req, res) => {
  try {
    await VacancyService.remove(req.params._id)
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { vacancyRouter }
