import { Router } from 'express'

import { problemFormat } from '../../common/util'
import { GraduateModel } from '../../common/models/graduate.model'
import { getFile } from '../../common/providers/grid-fs'
import { failIfNotLoggedIn } from '../../common/providers/auth-middleware'

const graduateRouter = Router()

// GET /data/graduate/:_id/cv/download
graduateRouter.get(
  '/:_id/cv/download',
  failIfNotLoggedIn(),
  async (req, res) => {
    try {
      res.set('Content-Type', 'application/octet-stream')
      res.set('Content-Disposition', 'attachment')
      ;(await getFile(GraduateModel, req.params._id, 'cv')).pipe(res)
    } catch (e) {
      res.json(problemFormat(e))
    }
  }
)

export { graduateRouter }
