import { Router } from 'express'
import { authenticate } from 'passport'
import * as qs from 'qs'

import config from '../configs/config'
import { problemFormat, successFormat } from '../../common/util'
import { CompanyModel } from '../../common/models/company.model'
import { KeyModel } from '../../common/models/key.model'
import { CompanyService } from '../../common/providers/company.service'
import {
  startPasswordReset,
  finishPasswordReset
} from '../../common/providers/reset.service'

const authRouter = Router()

// POST /data/auth/register?randomKey&email
authRouter.post('/register', async (req, res) => {
  try {
    /* todo: enable this
    if (!await (KeyModel as any).match(req.query.randomKey, req.query.email)) {
      return res.json(problemFormat('Key expired or not found.'))
    }*/

    await CompanyService.add(req.body)
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// POST /data/auth/login
authRouter.post('/login', (req, res, next) =>
  authenticate('local-company', {
    successRedirect: (req.query && req.query.continue) || '/',
    failureRedirect: `/login?${qs.stringify({
      problem: { code: 'WRONG_CREDENTIALS' }
    })}`
  })(req, res, next)
)

// GET /data/auth/logout
authRouter.get('/logout', async (req, res) => {
  try {
    req.logout()
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// POST /data/auth/reset/start
authRouter.post('/reset/start', async (req, res) => {
  try {
    await startPasswordReset(
      CompanyModel as any,
      req.body.email,
      config.dev.port
    )
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// POST /data/auth/reset/finish
authRouter.post('/reset/finish', async (req, res) => {
  try {
    await finishPasswordReset(
      CompanyModel as any,
      req.body.email,
      req.body.randomKey,
      req.body.password
    )
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { authRouter }
