import { Router } from 'express'

import { successFormat, problemFormat } from '../../common/util'
import { CompanyService } from '../../common/providers/company.service'
import { failIfNotLoggedIn } from '../../common/providers/auth-middleware'

const companyRouter = Router()

// GET /data/company/me
companyRouter.get('/me', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.json(
      successFormat({
        company: await CompanyService.get(req.user ? req.user._id : undefined)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// PUT /data/company/me
companyRouter.put('/me', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.json(
      successFormat({
        company: await CompanyService.edit(
          req.user ? req.user.id : undefined,
          req.body
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// DELETE /data/company/me
companyRouter.delete('/me', failIfNotLoggedIn(), async (req, res) => {
  try {
    await CompanyService.remove(req.user ? req.user._id : undefined)
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { companyRouter }
