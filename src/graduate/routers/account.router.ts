import { Router } from 'express'

import { failIfNotLoggedIn } from '../../common/providers/auth-middleware'
import { bufferToStream, problemFormat, successFormat } from '../../common/util'
import { getFile, setFile, removeFile } from '../../common/providers/grid-fs'
import { GraduateModel } from '../../common/models/graduate.model'
import { GraduateService } from '../../common/providers/graduate.service'

const accountRouter = Router()

// POST /data/account/me/cv
accountRouter.post('/me/cv', failIfNotLoggedIn(), async (req, res) => {
  try {
    await setFile(
      GraduateModel,
      req.user ? req.user._id : undefined,
      'cv',
      bufferToStream(req.files['cv'].data)
    )
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// POST /data/account/me/picture
accountRouter.post('/me/picture', failIfNotLoggedIn(), async (req, res) => {
  try {
    await setFile(
      GraduateModel,
      req.user ? req.user._id : undefined,
      'picture',
      bufferToStream(req.files['picture'].data)
    )
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/account/me
accountRouter.get('/me', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.json(
      successFormat({
        account: await GraduateService.get(req.user ? req.user._id : undefined)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/account/me/cv/download
accountRouter.get('/me/cv/download', failIfNotLoggedIn(), async (req, res) => {
  try {
    ;(await getFile(
      GraduateModel,
      req.user ? req.user._id : undefined,
      'cv'
    )).pipe(res)
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/account/me/picture
accountRouter.get('/me/picture', failIfNotLoggedIn(), async (req, res) => {
  try {
    ;(await getFile(
      GraduateModel,
      req.user ? req.user._id : undefined,
      'picture'
    )).pipe(res)
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// PUT /data/account/me
accountRouter.put('/me', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.json(
      successFormat({
        account: await GraduateService.edit(
          req.user ? req.user._id : undefined,
          req.body
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// DELETE /data/account/me/cv
accountRouter.delete('/me/cv', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.send(
      await removeFile(GraduateModel, req.user ? req.user._id : undefined, 'cv')
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// DELETE /data/account/me/picture
accountRouter.delete('/me/picture', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.send(
      await removeFile(
        GraduateModel,
        req.user ? req.user._id : undefined,
        'picture'
      )
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { accountRouter }
