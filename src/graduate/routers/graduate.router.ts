import { Router } from 'express'
import * as fs from 'fs'
import * as qs from 'qs'

import { successFormat, problemFormat } from '../../common/util'
import { GraduateService } from '../../common/providers/graduate.service'

const graduateRouter = Router()

// GET /data/graduate/me
graduateRouter.get('/me', async (req, res) => {
  try {
    res.json(
      successFormat({
        graduate: await GraduateService.get(req.user ? req.user.id : undefined)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// PUT /data/graduate/me
graduateRouter.put('/me', async (req, res) => {
  try {
    res.json(
      successFormat({
        graduate: await GraduateService.edit(
          req.user ? req.user.id : undefined,
          req.body
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// DELETE /data/graduate/me
graduateRouter.delete('/me', async (req, res) => {
  try {
    await GraduateService.remove(req.user ? req.user.id : undefined)
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// * POST /data/graduate/me/picture
graduateRouter.post('/me/picture', async (req, res) => {
  try {
    if (!req.user) return res.json(problemFormat('User not found.'))

    await GraduateService.addPicture(
      req.user._id,
      fs.createReadStream(req.body.files.picture.path)
    )
    res.json(
      (await GraduateService.hasPicture(req.user._id))
        ? successFormat()
        : problemFormat({
            code: 'STILL_NOT_ADDED',
            message: 'Unable to add picture.'
          })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// * GET /data/graduate/me/picture
graduateRouter.get('/me/picture', async (req, res) => {
  try {
    if (!req.user) return res.json(problemFormat('User not found.'))

    res.send(await GraduateService.getPicture(req.user._id))
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// * DELETE /data/graduate/me/picture
graduateRouter.delete('/me/picture', async (req, res) => {
  try {
    if (!req.user) return res.json(problemFormat('User not found.'))

    await GraduateService.removePicture(
      req.user._id,
      qs.parse(req.query).check !== 'false'
    )
    res.json(
      !await GraduateService.hasPicture(req.user._id)
        ? successFormat()
        : problemFormat({
            code: 'STILL_EXISTS',
            message: 'Unable to remove picture.'
          })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { graduateRouter }
