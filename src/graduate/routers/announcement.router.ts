import { Router } from 'express'

import { successFormat, problemFormat } from '../../common/util'
import { AnnouncementService } from '../../common/providers/announcement.service'
import { AnnouncementModel } from '../../common/models/announcement.model'
import { getFile } from '../../common/providers/grid-fs'

const announcementRouter = Router()

// GET /data/announcement/:_id
announcementRouter.get('/:_id', async (req, res) => {
  try {
    res.json(
      successFormat({
        announcements: await AnnouncementService.getByDepartment(
          req.params._id,
          req.user ? req.user.department : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/announcement/list/:count/start/0
announcementRouter.get('/list/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        announcements: await AnnouncementService.listByDepartment(
          req.params.count,
          null,
          req.user ? req.user.department : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/announcement/list/:count/start/:since
announcementRouter.get('/list/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        announcements: await AnnouncementService.listByDepartment(
          req.params.count,
          req.params.since,
          req.user ? req.user.department : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/announcement/search/:count/start/0?term=:term
announcementRouter.get('/search/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        announcements: await AnnouncementService.searchByDepartment(
          req.query.term,
          req.params.count,
          null,
          req.user ? req.user.department : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/announcement/search/:count/start/:since?term=:term
announcementRouter.get('/search/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        announcements: await AnnouncementService.searchByDepartment(
          req.query.term,
          req.params.count,
          req.params.since,
          req.user ? req.user.department : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// PUT /data/announcement/:_id/interested
announcementRouter.put('/:_id/interested', async (req, res) => {
  try {
    res.json(
      successFormat({
        numberOfInterested: await AnnouncementService.interest(
          req.params._id,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET data/announcement/:_id/attachment
announcementRouter.get('/:_id/attachment/', async (req, res) => {
  try {
    ;(await getFile(AnnouncementModel, req.params._id, 'file')).pipe(res)
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { announcementRouter }
