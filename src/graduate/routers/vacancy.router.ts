import { Router } from 'express'

import { successFormat, problemFormat } from '../../common/util'
import { VacancyService } from '../../common/providers/vacancy.service'

const vacancyRouter = Router()

// GET /data/vacancy/:_id
vacancyRouter.get('/:_id', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancy: await VacancyService.getByDepartmentForGrad(
          req.params._id,
          req.user ? req.user.department : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/list/:count/start/0
vacancyRouter.get('/list/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.listByDepartmentForGrad(
          req.params.count,
          null,
          req.user ? req.user.department : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/list/:count/start/:since
vacancyRouter.get('/list/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.listByDepartmentForGrad(
          req.params.count,
          req.params.since,
          req.user ? req.user.department : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/search/:count/start/0?term=:term
vacancyRouter.get('/search/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.searchByDepartmentForGrad(
          req.query.term,
          req.params.count,
          null,
          req.user ? req.user.department : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/search/:count/start/:since?term=:term
vacancyRouter.get('/search/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.searchByDepartmentForGrad(
          req.query.term,
          req.params.count,
          req.params.since,
          req.user ? req.user.department : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// PUT /data/vacancy/:_id/apply
vacancyRouter.put('/:_id/apply', async (req, res) => {
  try {
    res.json(
      successFormat({
        numberOfApplicants: await VacancyService.apply(
          req.params._id,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { vacancyRouter }
