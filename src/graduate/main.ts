import express from 'express'
import { join } from 'path'

import config from './configs/config'
import setup from '../common/configs/express.config'
import mongooseSetup from '../common/configs/mongoose.config'
import { serveFront } from '../common/providers/serveFront'

import './configs/passport.config'

import '../common/models/admin.model'
import '../common/models/announcement.model'
import '../common/models/company.model'
import '../common/models/department.model'
import '../common/models/graduate.model'
import '../common/models/key.model'
import '../common/models/potential.model'
import '../common/models/vacancy.model'

import { accountRouter } from './routers/account.router'
import { announcementRouter } from './routers/announcement.router'
import { authRouter } from './routers/auth.router'
import { companyRouter } from './routers/company.router'
import { departmentRouter } from './routers/department.router'
import { graduateRouter } from './routers/graduate.router'
import { vacancyRouter } from './routers/vacancy.router'

const graduateApp = express()
setup(graduateApp, config)
mongooseSetup(config)

graduateApp.use('/data/account', accountRouter)
graduateApp.use('/data/announcement', announcementRouter)
graduateApp.use('/data/auth', authRouter)
graduateApp.use('/data/company', companyRouter)
graduateApp.use('/data/department', departmentRouter)
graduateApp.use('/data/graduate', graduateRouter)
graduateApp.use('/data/vacancy', vacancyRouter)

serveFront(graduateApp, {
  rootDirectory: join(process.cwd(), 'fronts', 'graduate', 'build')
})

graduateApp.listen(config.dev.port, () =>
  console.log(
    `Listening on http://localhost:${config.dev.port} (department app)`
  )
)

/*
import { GraduateModel } from '../common/models/graduate.model'

const graduate = new GraduateModel() as any
graduate.name = { first: 'Biruk', last: 'Tesfaye' }
graduate.birthdate = new Date(1997, 5, 22)
graduate.gender = 'M'
graduate.nationality = 'ET'
graduate.email = 'biruktesfayeve@gmail.com'
graduate.phone = { code: 251, number: '0923637040' }
graduate.department = '5ad896619930704b60b1fc0a' // NOTE: MAKE SURE UPDATE THIS!
graduate.survey.status = 'SELF_EMPLOYED'
graduate.survey.learning = true
graduate.survey.jobType = 'PERMANENT'
graduate.survey.jobStatus = 'FULL_TIME'
graduate.survey.selfEmployedByField = true
graduate.survey.graduationYear = 2010
graduate
  .setPassword('happy')
  .then(() => graduate.save())
  .then(() => console.log('success'))
  .catch(console.error)

const graduate1 = new GraduateModel() as any
graduate1.name = { first: 'Kaleab', last: 'Serkebirhan' }
graduate1.birthdate = new Date(1997, 5, 22)
graduate1.gender = 'M'
graduate1.nationality = 'ET'
graduate1.email = 'kaleabmelki@gmail.com'
graduate1.phone = { code: 251, number: '0923772839' }
graduate1.department = '5ad896619930704b60b1fc0a' // NOTE: MAKE SURE UPDATE THIS!
graduate1.survey.status = 'COMPANY_EMPLOYED'
graduate1.survey.learning = false
graduate1.survey.jobType = 'CONTRACT'
graduate1.survey.jobStatus = 'FULL_TIME'
graduate1.survey.company = '5ad056752c5a4a11740fad0f'
graduate1.survey.selfEmployedByField = true
graduate1.survey.graduationYear = 2010
graduate1
  .setPassword('happy')
  .then(() => graduate1.save())
  .then(() => console.log('success'))
  .catch(console.error)
*/

/*
const graduate2 = new GraduateModel() as any
graduate2.name = { first: 'Tolawak', last: 'GElana' }
graduate2.birthdate = new Date(1997, 5, 22)
graduate2.gender = 'M'
graduate2.nationality = 'ET'
graduate2.email = 'tolawak/@gmail.com'
graduate2.phone = { code: 251, number: '0920524846' }
graduate2.department = '5ad057b5e620ef3520e0fa44' // NOTE: MAKE SURE UPDATE THIS!
graduate2.survey.status = 'COMPANY_EMPLOYED'
graduate2.survey.selfEmployedByField = false
graduate2.survey.graduationYear = 2008
graduate2.survey.learning = true
graduate2.survey.jobType = 'PERMANENT'
graduate2.survey.jobStatus = 'FULL_TIME'
graduate2.survey.company = '5ad056752c5a4a11740fad0f'
graduate2
  .setPassword('happy')
  .then(() => graduate2.save())
  .then(() => console.log('success'))
  .catch(console.error)
const graduate = new GraduateModel() as any
graduate.name = { first: 'Bazen', last: 'TEklehaymanot' }
graduate.birthdate = new Date(1997, 5, 22)
graduate.gender = 'M'
graduate.nationality = 'ET'
graduate.email = 'bazen/@gmail.com'
graduate.phone = { code: 251, number: '0920524846' }
graduate.department = '5ad057b5e620ef3520e0fa44' // NOTE: MAKE SURE UPDATE THIS!
graduate.survey.status = 'COMPANY_EMPLOYED'
graduate.survey.selfEmployedByField = false
graduate.survey.graduationYear = 2010
graduate.survey.learning = true
graduate.survey.jobType = 'INTERN'
graduate.survey.jobStatus = 'PART_TIME'
graduate.survey.company = '5ad056752c5a4a11740fad0f'
graduate
  .setPassword('happy')
  .then(() => graduate.save())
  .then(() => console.log('success'))
  .catch(console.error)

const graduate3 = new GraduateModel() as any
graduate3.name = { first: 'Abeni', last: 'Sintayehu' }
graduate3.birthdate = new Date(1997, 5, 22)
graduate3.gender = 'M'
graduate3.nationality = 'ET'
graduate3.email = 'abeni/@gmail.com'
graduate3.phone = { code: 251, number: '0920524846' }
graduate3.department = '5ad057b5e620ef3520e0fa44' // NOTE: MAKE SURE UPDATE THIS!
graduate3.survey.status = 'COMPANY_EMPLOYED'
graduate3.survey.selfEmployedByField = false
graduate3.survey.graduationYear = 2010
graduate3.survey.learning = true
graduate3.survey.jobType = 'CONTRACT'
graduate3.survey.jobStatus = 'PER_DIEM'
graduate3.survey.company = '5ad056752c5a4a11740fad0f'
graduate3
  .setPassword('happy')
  .then(() => graduate3.save())
  .then(() => console.log('success'))
  .catch(console.error)
*/
