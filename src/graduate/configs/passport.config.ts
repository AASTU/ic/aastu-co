import { serializeUser, deserializeUser, use } from 'passport'
import { Strategy } from 'passport-local'
import { Document } from 'mongoose'

import { GraduateModel } from '../../common/models/graduate.model'

serializeUser((graduate: Document, done) => done(null, graduate._id))
deserializeUser((_id, done) => GraduateModel.findById(_id, done))

use(
  'local-graduate',
  new Strategy(
    {
      usernameField: 'email'
    },
    (email, password, done) =>
      GraduateModel.findOne({ email: email })
        .then(async graduate =>
          done(
            null,
            !graduate || !await (graduate as any).isPasswordCorrect(password)
              ? false
              : graduate
          )
        )
        .catch(done)
  )
)
