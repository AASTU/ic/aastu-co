export default {
  name: 'aastu-co-graduate',
  dev: {
    port: Number(process.env.SERVER_PORT) || 4444,
    db: process.env.DB_LINK || 'mongodb://127.0.0.1/aastu-co'
  },
  prod: {
    //TODO for production
  }
}
