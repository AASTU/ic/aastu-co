export default {
  name: 'aastu-co-admin',
  dev: {
    port: Number(process.env.SERVER_PORT) || 1111,
    db: process.env.DB_LINK || 'mongodb://127.0.0.1/aastu-co'
  },
  prod: {
    //TODO for production
  }
}
