import { serializeUser, deserializeUser, use } from 'passport'
import { Strategy } from 'passport-local'
import { Document } from 'mongoose'

import { AdminModel } from '../../common/models/admin.model'

serializeUser((admin: Document, done) => done(null, admin._id))
deserializeUser((_id, done) => AdminModel.findById(_id, done))

use(
  'local-admin',
  new Strategy((useless, password, done) => {
    AdminModel.findOne({})
      .then(async admin =>
        done(
          null,
          !admin || !await (admin as any).isPasswordCorrect(password)
            ? false
            : admin
        )
      )
      .catch(done)
  })
)
