import { Router } from 'express'
import qs from 'qs'

import { successFormat, problemFormat, sendEmail } from '../../common/util'
import { DepartmentService } from '../../common/providers/department.service'
import { KeyModel } from '../../common/models/key.model'

const departmentRouter = Router()

// POST /data/department/new
departmentRouter.post('/new', async (req, res) => {
  try {
    await sendEmail({
      to: req.body.email,
      subject: 'Register Your Department',
      text: `Register your department at http://localhost:3000/register?${qs.stringify(
        {
          email: req.body.email,
          randomKey: await (KeyModel as any).add(req.body.email)
        }
      )}`
    }) // todo: fix the link
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/department/all
departmentRouter.get('/all', checkAuthentication, async (req, res) => {
  try {
    res.json(
      successFormat({
        departments: await DepartmentService.getAll()
      })
    )
  } catch (err) {
    res.json(problemFormat(err))
  }
})

// GET /data/department/:_id
departmentRouter.get('/:_id', async (req, res) => {
  try {
    res.json(successFormat(await DepartmentService.get(req.params._id)))
  } catch (err) {
    res.json(problemFormat(err))
  }
})

// GET /data/department/search/all?term=:term
departmentRouter.get('/search/all', async (req, res) => {
  try {
    res.json(
      successFormat({
        departments: await DepartmentService.searchAll(req.query.term)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// DELETE /data/department/:_id
departmentRouter.delete('/:_id', async (req, res) => {
  try {
    res.json(successFormat(await DepartmentService.remove(req.params._id)))
  } catch (err) {
    res.json(problemFormat(err))
  }
})

function checkAuthentication(req, res, next) {
  if (req.isAuthenticated()) {
    //if user is looged in, req.isAuthenticated() will return true
    next()
  } else {
    console.log('not logged in ADMIN')
    // res.redirect("/login");
  }
}
export { departmentRouter }
