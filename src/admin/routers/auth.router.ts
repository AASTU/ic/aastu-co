import { Router } from 'express'
import { authenticate } from 'passport'
import * as qs from 'qs'

import { problemFormat, successFormat } from '../../common/util'

const authRouter = Router()

// POST /data/auth/login
authRouter.post('/login', (req, res, next) =>
  authenticate('local-admin', {
    successRedirect: (req.query && req.query.continue) || '/',
    failureRedirect: `/login?${qs.stringify({
      problem: { code: 'WRONG_CREDENTIALS' }
    })}`
  })(req, res, next)
)

// GET /data/auth/logout
authRouter.get('/logout', async (req, res) => {
  try {
    req.logout()
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { authRouter }
