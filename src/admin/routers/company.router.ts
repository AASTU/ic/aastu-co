import { Router } from 'express'
import qs from 'qs'

import { successFormat, problemFormat, sendEmail } from '../../common/util'
import { CompanyService } from '../../common/providers/company.service'
import { KeyModel } from '../../common/models/key.model'

const companyRouter = Router()

// POST /data/company/new
companyRouter.post('/new', async (req, res) => {
  try {
    await sendEmail({
      to: req.body.email,
      subject: 'Register Your Company',
      text: `Register your company at http://localhost:3000/register?${qs.stringify(
        {
          email: req.body.email,
          randomKey: await (KeyModel as any).add(req.body.email)
        }
      )}`
    }) // todo: fix the link
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/company/:_id
companyRouter.get('/:_id', async (req, res) => {
  try {
    res.json(successFormat(await CompanyService.get(req.params._id)))
  } catch (err) {
    res.json(problemFormat(err))
  }
})

// GET /data/company/list/:count/start/0
companyRouter.get('/list/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        companies: await CompanyService.list(req.params.count)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/company/list/:count/start/:since
companyRouter.get('/list/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        companies: await CompanyService.list(req.params.count, req.params.since)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/company/search/:count/start/0?term=:term
companyRouter.get('/search/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await CompanyService.search(req.query.term, req.params.count)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/company/search/:count/start/:since?term=:term
companyRouter.get('/search/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await CompanyService.search(
          req.query.term,
          req.params.count,
          req.params.since
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// DELETE /data/company/:_id
companyRouter.delete('/:_id', async (req, res) => {
  try {
    res.json(successFormat(await CompanyService.remove(req.params._id)))
  } catch (err) {
    res.json(problemFormat(err))
  }
})

export { companyRouter }

// todo: add company???
