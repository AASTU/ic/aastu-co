import { Router } from 'express'

import { failIfNotLoggedIn } from '../../common/providers/auth-middleware'
import { problemFormat, successFormat } from '../../common/util'
import { getFile, setFile, removeFile } from '../../common/providers/grid-fs'
import { AdminModel } from '../../common/models/admin.model'
import { AdminService } from '../../common/providers/admin.service'

const accountRouter = Router()

// POST /data/account/me/picture
accountRouter.post('/me/picture', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.send(
      await setFile(
        AdminModel,
        req.user ? req.user._id : undefined,
        'picture',
        req.body.picture // todo: check if this works
      )
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/account/me
accountRouter.get('/me', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.json(
      successFormat({
        account: await AdminService.get(req.user ? req.user._id : undefined)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/account/me/picture
accountRouter.get('/me/picture', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.send(
      await getFile(AdminModel, req.user ? req.user._id : undefined, 'picture')
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// PUT /data/account/me
accountRouter.put('/me', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.json(
      successFormat({
        account: await AdminService.edit(
          req.user ? req.user._id : undefined,
          req.body
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// DELETE /data/account/me/picture
accountRouter.delete('/me/picture', failIfNotLoggedIn(), async (req, res) => {
  try {
    res.send(
      await removeFile(
        AdminModel,
        req.user ? req.user._id : undefined,
        'picture'
      )
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { accountRouter }
