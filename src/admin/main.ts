import express from 'express'
import { join } from 'path'

import config from './configs/config'
import setup from '../common/configs/express.config'
import mongooseSetup from '../common/configs/mongoose.config'
import { serveFront } from '../common/providers/serveFront'

import './configs/passport.config'

import '../common/models/admin.model'
import '../common/models/announcement.model'
import '../common/models/company.model'
import '../common/models/department.model'
import '../common/models/graduate.model'
import '../common/models/key.model'
import '../common/models/potential.model'
import '../common/models/vacancy.model'

import { accountRouter } from './routers/account.router'
import { authRouter } from './routers/auth.router'
import { companyRouter } from './routers/company.router'
import { departmentRouter } from './routers/department.router'

const adminApp = express()
setup(adminApp, config)
mongooseSetup(config)

adminApp.use('/data/account', accountRouter)
adminApp.use('/data/auth', authRouter)
adminApp.use('/data/company', companyRouter)
adminApp.use('/data/department', departmentRouter)

serveFront(adminApp, {
  rootDirectory: join(process.cwd(), 'fronts', 'admin', 'build')
})

adminApp.listen(config.dev.port, () =>
  console.log(`Listening on http://localhost:${config.dev.port} (admin app)`)
)

/*
import { AdminModel } from '../common/models/admin.model'

let admin = new AdminModel() as any
admin
  .setPassword('happy')
  .then(() => admin.save())
  .then(() => console.log('success'))
  .catch(console.error)
*/

/*admin = AdminModel.findOne({})
  .then(admin => (admin ? (admin as any).setPassword('happy') : null))
  .then(() => admin.save())
  .then(() => console.log('success'))
  .catch(console.error)*/
