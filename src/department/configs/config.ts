export default {
  name: 'aastu-co-department',
  dev: {
    port: Number(process.env.SERVER_PORT) || 3333,
    db: process.env.DB_Link || 'mongodb://127.0.0.1/aastu-co'
  },
  prod: {
    //TODO for production time
  }
}
