import { serializeUser, deserializeUser, use } from 'passport'
import { Strategy } from 'passport-local'
import { Document } from 'mongoose'

import { DepartmentModel } from '../../common/models/department.model'

serializeUser((department: Document, done) => done(null, department._id))
deserializeUser((_id, done) => DepartmentModel.findById(_id, done))

use(
  'local-department',
  new Strategy(
    {
      usernameField: 'email'
    },
    (email, password, done) =>
      DepartmentModel.findOne({ email: email })
        .then(async department =>
          done(
            null,
            !department ||
            !await (department as any).isPasswordCorrect(password)
              ? false
              : department
          )
        )
        .catch(done)
  )
)
