import express from 'express'
import { join } from 'path'

import config from './configs/config'
import setup from '../common/configs/express.config'
import mongooseSetup from '../common/configs/mongoose.config'
import { serveFront } from '../common/providers/serveFront'
import './configs/passport.config'

import '../common/models/admin.model'
import '../common/models/announcement.model'
import '../common/models/company.model'
import '../common/models/department.model'
import '../common/models/graduate.model'
import '../common/models/key.model'
import '../common/models/potential.model'
import '../common/models/vacancy.model'

import { accountRouter } from './routers/account.router'
import { announcementRouter } from './routers/announcement.router'
import { authRouter } from './routers/auth.router'
import { companyRouter } from './routers/company.router'
import { graduateRouter } from './routers/graduate.router'
import { vacancyRouter } from './routers/vacancy.router'

const departmentApp = express()
setup(departmentApp, config)
mongooseSetup(config)

departmentApp.use('/data/account', accountRouter)
departmentApp.use('/data/announcement', announcementRouter)
departmentApp.use('/data/auth', authRouter)
departmentApp.use('/data/company', companyRouter)
departmentApp.use('/data/graduate', graduateRouter)
departmentApp.use('/data/vacancy', vacancyRouter)

serveFront(departmentApp, {
  rootDirectory: join(process.cwd(), 'fronts', 'department', 'build')
})

departmentApp.listen(config.dev.port, () =>
  console.log(
    `Listening on http://localhost:${config.dev.port} (department app)`
  )
)

/*import { DepartmentModel } from '../common/models/department.model'
import { AnnouncementModel } from '../common/models/announcement.model'

const department = new DepartmentModel() as any
department.name = 'Computer Science'
department.headName = { first: 'Kaleab', last: 'panda' }
department.collegeName = 'Electrical and Mechanical engineering'
department.email = 'cs@kalat.edu'
department.phone = { code: 251, number: '0116451700' }
department.contactInfo = 'Kalat College, Addis Ababa, Ethiopia'
department
  .setPassword('happy')
  .then(() => department.save())
  .then(() => console.log('success'))
  .catch(error => console.log(error))

const department1 = new DepartmentModel() as any
department1.name = 'Software Engineering'
department1.headName = { first: 'Brook', last: 'K/michael' }
department1.collegeName = 'Electrical and Mechanical Engineering'
department1.email = 'brookk/@kalat.edu'
department1.phone = { code: 251, number: '0116451700' }
department1.contactInfo = 'Brook College, Addis Ababa, Ethiopia'
department1
  .setPassword('happy')
  .then(() => department1.save())
  .then(() => console.log('success'))
  .catch(error => console.log(error))

const department2 = new DepartmentModel() as any
department2.name = 'Mechanical Engineering'
department2.headName = { first: 'Brook', last: 'K/michael' }
department2.collegeName = 'Electrical and Mechanical Engineering'
department2.email = 'Abebe/@kalat.edu'
department2.phone = { code: 251, number: '0116451700' }
department2.contactInfo = 'AASTU, Addis Ababa, Ethiopia'
department2
  .setPassword('happy')
  .then(() => department2.save())
  .then(() => console.log('success'))
  .catch(error => console.log(error))

const vacancy = new AnnouncementModel() as any
vacancy.department = '5ad057b5e620ef3520e0fa45'
vacancy.title = 'Query conditions and other options'
vacancy.collegeName = 'Electrical and Mechanical engineering'
vacancy.description =
  'What if we wanted to populate our fans array based on their age, select just their names, and return at most, any 5 of them?'
vacancy.time = new Date('27 april 2018')
vacancy.location = 'Addis Ababa, Ethiopia'
vacancy.interested = ['5ad05d7d6d2c74306c155fbb', '5ad05d7d6d2c74306c155fb9']
vacancy
  .save()
  .then(() => console.log('success'))
  .catch(err => console.log(err))

const vacancy1 = new AnnouncementModel() as any
vacancy1.department = '5ad057b5e620ef3520e0fa44'
vacancy1.title = 'Refs to children'
vacancy1.collegeName = 'Electrical and Mechanical engineering'
vacancy1.description =
  'What if we wanted to populate our fans array based on their age, select just their names, and return at most, any 5 of them?'
vacancy1.time = new Date('27 april 2018')
vacancy1.location = 'Addis Ababa, Ethiopia'
vacancy1.interested = ['5ad05d7d6d2c74306c155fbb', '5ad05d7d6d2c74306c155fb9']
vacancy1
  .save()
  .then(() => console.log('success'))
  .catch(err => console.log(err))

const vacancy2 = new AnnouncementModel() as any
vacancy2.department = '5ad057b5e620ef3520e0fa45'
vacancy2.title = 'Populating multiple existing documents'
vacancy2.collegeName = 'Electrical and Mechanical engineering'
vacancy2.description =
  'If we have one or many mongoose documents or even plain objects (_like mapReduce output_), we may populate them using the Model.populate() method available in mongoose >= 3.6. This is what document#populate() and query#populate() use to populate documents.'
vacancy2.time = new Date('27 jun 2018')
vacancy2.location = 'Addis Ababa, Ethiopia'
vacancy2.interested = [
  '5ad05d7d6d2c74306c155fb9',
  '5ad05d7d6d2c74306c155fba',
  '5ad05d7d6d2c74306c155fbb'
]
vacancy2
  .save()
  .then(() => console.log('success'))
  .catch(err => console.log(err))*/
