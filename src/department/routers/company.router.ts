import { Router } from 'express'

import { problemFormat, sendEmail, successFormat } from '../../common/util'
import { KeyModel } from '../../common/models/key.model'
import { CompanyService } from '../../common/providers/company.service'

const companyRouter = Router()

//POST: /data/company/new
companyRouter.post('/new', async (req, res) => {
  try {
    // todo
    await (KeyModel as any).add(req.body.email)
    await sendEmail({
      from: req.body.receiver,
      to: req.body.email,
      subject: 'TODO',
      text: 'TODO'
    })

    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})
// GET /data/company/:_id
companyRouter.get('/:_id', async (req, res) => {
  try {
    res.json(successFormat(await CompanyService.get(req.params._id)))
  } catch (err) {
    res.json(problemFormat(err))
  }
})

// GET /data/company/list/:count/start/0
companyRouter.get('/list/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        companies: await CompanyService.list(req.params.count)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/company/list/:count/start/:since
companyRouter.get('/list/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        companies: await CompanyService.list(req.params.count, req.params.since)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/search/:count/start/0?term=:term
companyRouter.get('/search/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await CompanyService.search(req.query.term, req.params.count)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/company/search/:count/start/:since?term=:term
companyRouter.get('/search/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await CompanyService.search(
          req.query.term,
          req.params.count,
          req.params.since
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { companyRouter }
