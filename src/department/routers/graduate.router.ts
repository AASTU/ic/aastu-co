import { Router } from 'express'
import qs from 'qs'

import { successFormat, problemFormat, sendEmail } from '../../common/util'
import { GraduateService } from '../../common/providers/graduate.service'
import { KeyModel } from '../../common/models/key.model'
import { GraduateModel } from '../../common/models/graduate.model'
import { getFile } from '../../common/providers/grid-fs'
import { failIfNotLoggedIn } from '../../common/providers/auth-middleware'

const graduateRouter = Router()

// POST /data/department/new
graduateRouter.post('/new', async (req, res) => {
  try {
    await sendEmail({
      to: req.body.email,
      subject: 'Register Yourself as a Graduate',
      text: `Register yourself as a graduate at http://localhost:3000/register?${qs.stringify(
        {
          email: req.body.email,
          randomKey: await (KeyModel as any).add(req.body.email)
        }
      )}`
    }) // todo: fix the link
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/graduate/:_id
graduateRouter.get('/:_id', async (req, res) => {
  try {
    res.json(
      successFormat({
        graduate: await GraduateService.getByDepartment(
          req.params._id,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (err) {
    res.json(problemFormat(err))
  }
})

// GET /data/graduate/list/:count/start/0
graduateRouter.get('/list/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        graduates: await (GraduateService as any).listByDepartment(
          req.params.count,
          null,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/graduate/list/:count/start/:since
graduateRouter.get('/list/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        graduate: await GraduateService.listByDepartment(
          req.params.count,
          req.params.since,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/graduate/search/:count/start/0?term=:term
graduateRouter.get('/search/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        graduate: await GraduateService.searchByDepartment(
          req.query.term,
          req.params.count,
          null,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/graduate/search/:count/start/:since?term=:term
graduateRouter.get('/search/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        graduate: await GraduateService.searchByDepartment(
          req.query.term,
          req.params.count,
          req.params.since,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

//todo deprecated
//PUT /data/graduate/:_id/graduate
graduateRouter.put('/:_id/graduate', async (req, res) => {
  try {
    res.json(successFormat(await GraduateService.graduate(req.params._id)))
  } catch (err) {
    res.json(problemFormat(err))
  }
})

//todo deprecated
// DELETE /data/graduate/:_id
graduateRouter.delete('/:_id', async (req, res) => {
  try {
    res.json(successFormat(await GraduateService.remove(req.params._id)))
  } catch (err) {
    res.json(problemFormat(err))
  }
})

// GET /data/graduate/:_id/cv/download
graduateRouter.get(
  '/:_id/cv/download',
  failIfNotLoggedIn(),
  async (req, res) => {
    try {
      res.set('Content-Type', 'application/octet-stream')
      res.set('Content-Disposition', 'attachment')
      ;(await getFile(GraduateModel, req.params._id, 'cv')).pipe(res)
    } catch (e) {
      res.json(problemFormat(e))
    }
  }
)

export { graduateRouter }
