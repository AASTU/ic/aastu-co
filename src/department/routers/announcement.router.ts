import { Router } from 'express'

import { successFormat, problemFormat, bufferToStream } from '../../common/util'
import { AnnouncementService } from '../../common/providers/announcement.service'
import * as fs from 'fs'
import { DepartmentModel } from '../../common/models/department.model'
import {
  addFile,
  getFile,
  removeFile,
  setFile
} from '../../common/providers/grid-fs'
import { AnnouncementModel } from '../../common/models/announcement.model'
import { DepartmentService } from '../../common/providers/department.service'
import { failIfNotLoggedIn } from '../../common/providers/auth-middleware'

const announcementRouter = Router()

// POST /data/announcement/new
announcementRouter.post('/new', async (req, res) => {
  try {
    res.json(
      successFormat({
        announcement: await AnnouncementService.add(
          req.user ? req.user._id : undefined,
          req.body
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/announcement/:_id
announcementRouter.get('/:_id', async (req, res) => {
  try {
    res.json(
      successFormat({
        announcement: await AnnouncementService.getByDepartment(
          req.params._id,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/announcement/list/:count/start/:since
announcementRouter.get('/list/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        announcements: await AnnouncementService.listByDepartment(
          req.params.count,
          req.params.since,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/announcement/search/:count/start/:since?term=:term
announcementRouter.get('/search/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        announcements: await AnnouncementService.searchByDepartment(
          req.query.term,
          req.params.count,
          req.params.since,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// PUT /data/announcement/:_id
announcementRouter.put('/:_id', async (req, res) => {
  try {
    res.json(
      successFormat({
        announcement: await AnnouncementService.edit(req.params._id, req.body)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// DELETE /data/announcement/:_id
announcementRouter.delete('/:_id', async (req, res) => {
  try {
    await AnnouncementService.remove(req.params._id)
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

//POST: data/announcement/:_id/attachment/new
announcementRouter.post('/:_id/attachment/new', async (req, res) => {
  try {
    let streamFile = bufferToStream(req.files['attachment'].data)
    await setFile(AnnouncementModel, req.params._id, 'file', streamFile)
    res.json(successFormat())
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET data/announcement/:_id/attachment
announcementRouter.get('/:_id/attachment/', async (req, res) => {
  try {
    ;(await getFile(AnnouncementModel, req.params._id, 'file')).pipe(res)
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET data/announcement/:_id/attachment
announcementRouter.delete('/:_id/attachment/', async (req, res) => {
  try {
    res.send(await removeFile(DepartmentModel, req.params._id, 'file'))
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { announcementRouter }
