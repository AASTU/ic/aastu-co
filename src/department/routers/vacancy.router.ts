import { Router } from 'express'

import { successFormat, problemFormat } from '../../common/util'
import { VacancyService } from '../../common/providers/vacancy.service'

const vacancyRouter = Router()

// GET /data/vacancy/:_id
vacancyRouter.get('/:_id', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancy: await VacancyService.getByDepartment(
          req.params._id,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/list/:count/start/0
vacancyRouter.get('/list/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.listByDepartment(
          req.params.count,
          null,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/list/:count/start/:since
vacancyRouter.get('/list/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.listByDepartment(
          req.params.count,
          req.params.since,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/search/:count/start/0?term=:term
vacancyRouter.get('/search/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.searchByDepartment(
          req.query.term,
          req.params.count,
          null,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/vacancy/search/:count/start/:since?term=:term
vacancyRouter.get('/search/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        vacancies: await VacancyService.searchByDepartment(
          req.query.term,
          req.params.count,
          req.params.since,
          req.user ? req.user.id : undefined
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

//fixme get applicants filtered by job status

// PUT /data/vacancy/:_id/recommend/:graduate_id
vacancyRouter.put('/:_id/recommend/:graduate_id', async (req, res) => {
  try {
    res.json(
      successFormat({
        numberOfRecommendations: await VacancyService.recommend(
          req.params._id,
          req.params.graduate_id
        )
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { vacancyRouter }
