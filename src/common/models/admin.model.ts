import { Schema, model } from 'mongoose'
import { compare, hash } from 'bcrypt'

const AdminSchema = new Schema({
  createdOn: { type: Date, required: true, default: Date.now },
  password: { type: String, required: true }
})

AdminSchema.method({
  async isPasswordCorrect(pass: string): Promise<boolean> {
    try {
      if (!this.password) return Promise.reject('Password not set.')

      return compare(pass, (this as any).password)
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async setPassword(pass: string): Promise<void> {
    try {
      ;(this as any).password = await hash(pass, 12)

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async changePassword(oldPass: string, newPass: string): Promise<void> {
    try {
      if (!await this.isPasswordCorrect(oldPass))
        return Promise.reject('Wrong password.')

      return this.setPassword(newPass)
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

AdminSchema.static({
  async filterFromPublic(data, doc): Promise<void> {
    try {
      if (!data || !doc) return Promise.reject('Data or document not found.')

      // password
      !data.password || (await doc.setPassword(data.password))

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async filterForPublic(doc): Promise<any> {
    try {
      if (!doc) return Promise.reject('Doc parameter not found.')

      const filtered: any = {}

      // _id
      filtered._id = doc._id

      // createdOn
      filtered.createdOn = doc.createdOn

      return Promise.resolve(filtered)
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

const AdminModel = model('admins', AdminSchema)

export { AdminSchema, AdminModel }
