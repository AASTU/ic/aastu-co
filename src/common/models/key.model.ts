import { Schema, model } from 'mongoose'
import { randomBytes } from 'crypto'

const KeySchema = new Schema({
  email: { type: String, required: true },
  randomKey: { type: String, required: true },
  expiry: {
    type: Date,
    required: true,
    default: () => {
      return Date.now() + 1000 * 60 * 60 * 48
    }
  }
})

KeySchema.static({
  async cleanup(): Promise<void> {
    await this.findOneAndRemove({ expiry: { $lt: Date.now() } })

    return Promise.resolve()
  },

  async add(email: string, length = 64, expiry?: Date): Promise<string> {
    try {
      if (!email) return Promise.reject('"email" parameter not found.')

      await this.cleanup()

      const key = new KeyModel() as any
      key.email = email
      key.randomKey = randomBytes(length).toString('hex')
      !expiry || (key.expiry = expiry)

      await key.save()

      return Promise.resolve(key.randomKey)
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async match(randomKey: string, email: string): Promise<boolean> {
    if (!randomKey) return Promise.reject('"randomKey" parameter not found.')
    if (!email) return Promise.reject('"email" parameter not found.')

    await this.cleanup()

    return !!await this.findOne({
      randomKey: randomKey,
      email: email
    })
  },

  async delete(randomKey: string): Promise<void> {
    if (!randomKey) return Promise.reject('"randomKey" parameter not found.')

    await this.refresh()

    await this.findOneAndRemove({ randomKey: randomKey })

    return Promise.resolve()
  }
})

const KeyModel = model('keys', KeySchema)

export { KeyModel, KeySchema }
