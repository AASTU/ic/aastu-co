import { Schema, model } from 'mongoose'
import { compare, hash } from 'bcrypt'

const CompanySchema = new Schema({
  createdOn: { type: Date, required: true, default: Date.now },
  name: { type: String, required: true },
  address: { type: String, required: true },
  links: [String],
  email: { type: String, required: true },
  password: { type: String, required: true },
  phone: {
    code: { type: Number, required: true },
    number: { type: String, required: true }
  },
  contactInfo: String,
  description: { type: String, required: true }
})

CompanySchema.method({
  async isPasswordCorrect(pass: string): Promise<boolean> {
    try {
      if (!this.password) return Promise.reject('Password not set.')

      return compare(pass, (this as any).password)
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async setPassword(pass: string): Promise<void> {
    try {
      ;(this as any).password = await hash(pass, 12)

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async changePassword(oldPass: string, newPass: string): Promise<void> {
    try {
      if (!await this.isPasswordCorrect(oldPass))
        return Promise.reject('Wrong password.')

      return this.setPassword(newPass)
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

CompanySchema.static({
  async filterFromPublic(data, doc): Promise<void> {
    try {
      if (!doc || !data) return Promise.reject('No document or data found.')

      // name
      doc.name = data.name || doc.name

      // address
      doc.address = data.address || doc.address

      // links
      doc.links = data.links || doc.links || []

      // email
      doc.email = data.email || doc.email

      // password
      !data.password || (await doc.setPassword(data.password))

      // phone
      doc.phone =
        data.phone || doc.phone
          ? {
              code: data.phone.code || doc.phone.code,
              number: data.phone.number || doc.phone.number
            }
          : {}

      // contactInfo
      doc.contactInfo = data.contactInfo || doc.contactInfo

      // description
      doc.description = data.description || doc.description

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async filterForPublic(doc): Promise<any> {
    try {
      if (!doc) return Promise.reject('No document found.')

      const filtered: any = {}

      // _id
      filtered._id = doc._id

      // createdOn
      filtered.createdOn = doc.createdOn

      // name
      filtered.name = doc.name

      // address
      filtered.address = doc.address

      // links
      filtered.links = doc.links || []

      // email
      filtered.email = doc.email

      // phone
      filtered.phone = doc.phone
        ? {
            code: doc.phone.code,
            number: doc.phone.number
          }
        : {}

      // contactInfo
      filtered.contactInfo = doc.contactInfo

      // description
      filtered.description = doc.description

      return Promise.resolve(filtered)
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

CompanySchema.index({ '$**': 'text' })

const CompanyModel = model('companies', CompanySchema)

export { CompanySchema, CompanyModel }
