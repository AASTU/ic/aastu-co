import { Schema, model } from 'mongoose'
import { Stream } from 'stream'
import { grid } from '../configs/mongoose.config'

const ObjectId = Schema.Types.ObjectId

const AnnouncementSchema = new Schema({
  createdOn: { type: Date, required: true, default: Date.now },
  department: { type: ObjectId, ref: 'departments', required: true },
  title: { type: String, required: true },
  description: { type: String, required: true },
  time: { type: Date, trim: true },
  location: { type: String, trim: true },
  interested: [{ type: ObjectId, ref: 'graduates' }]
})

AnnouncementSchema.method({
  async count_interested(): Promise<number> {
    return Promise.resolve(
      this.interested !== undefined ? this.interested.length : 0
    )
  },
  async like() {
    return Promise.resolve(
      (this as any).votes.up !== undefined ? (this as any).votes.up.length : 0
    )
  },
  async dislike() {
    return Promise.resolve(
      (this as any).votes.down !== undefined
        ? (this as any).votes.down.length
        : 0
    )
  },
  filePath(): string {
    const doc = this as any
    if (!doc._id) throw new Error('Document _id not found.')

    return `announcement/file/${doc._id}`
  },

  async hasFile(): Promise<boolean> {
    try {
      const doc = this as any

      return new Promise<boolean>((resolve, reject) => {
        const gfs = grid()
        if (!gfs) {
          reject('GridFS not available.')
          return
        }

        gfs.exist({ filename: doc.filePath() }, (err, found) => {
          if (err) {
            reject(err)
            return
          }
          resolve()
        })
      })
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async getFile(): Promise<Stream> {
    try {
      const doc = this as any

      if (!await doc.hasFile())
        return Promise.reject({
          code: 'NOT_FOUND',
          message: 'No picture.'
        })

      const gfs = grid()
      if (!gfs) return Promise.reject('GridFS not available.')

      return Promise.resolve(gfs.createReadStream({ filename: doc.filePath() }))
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async setFile(stream: Stream): Promise<void> {
    try {
      const doc = this as any

      if (await doc.hasFile()) await doc.removeFile(false)

      return new Promise<void>((resolve, reject) => {
        const gfs = grid()
        if (!gfs) {
          reject('GridFS not available.')
          return
        }

        const writeStream = gfs.createWriteStream({
          filename: doc.filePath()
        })
        writeStream.once('finish', () => resolve())
        writeStream.once('error', err =>
          doc
            .removeFile(false)
            .then(() => reject(err))
            .catch(rmvErr => reject(rmvErr))
        )

        stream.pipe(writeStream)
      })
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async removeFile(check = true): Promise<void> {
    try {
      const doc = this as any

      if (check && !await doc.hasFile())
        return Promise.reject('No picture to remove.')

      return new Promise<void>((resolve, reject) => {
        const gfs = grid()
        if (!gfs) {
          reject('GridFS not available.')
          return
        }

        gfs.remove({ filename: doc.filePath() }, err => {
          if (err) {
            reject(err)
            return
          }
          resolve()
        })
      })
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

AnnouncementSchema.static({
  async filterFromPublic(data, doc): Promise<void> {
    try {
      if (!doc || !data) return Promise.reject('No document or data found.')

      // title
      doc.title = data.title || doc.title

      // description
      doc.description = data.description || doc.description

      // time
      doc.time = data.time || doc.time

      // location
      doc.location = data.location || doc.location

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async filterForPublic(doc): Promise<any> {
    try {
      if (!doc) return Promise.reject('No document found.')

      const filtered: any = {}

      // _id
      filtered._id = doc._id

      // createdOn
      filtered.createdOn = doc.createdOn

      // department
      filtered.department = doc.department

      // title
      filtered.title = doc.title

      // description
      filtered.description = doc.description

      // time
      filtered.time = doc.time

      // location
      filtered.location = doc.location

      // interested
      filtered.interestedCount = doc.interested.length

      return Promise.resolve(filtered)
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

AnnouncementSchema.index({ '$**': 'text' })
const AnnouncementModel = model('announcements', AnnouncementSchema)

export { AnnouncementSchema, AnnouncementModel }
