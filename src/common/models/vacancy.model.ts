import { Schema, model, Document, Model } from 'mongoose'
import { CompanyService } from '../providers/company.service'
import { CompanyModel } from './company.model'
import { DepartmentModel } from './department.model'
import { GraduateModel } from './graduate.model'

const ObjectId = Schema.Types.ObjectId

const VacancySchema = new Schema({
  createdOn: { type: Date, required: true, default: Date.now },
  company: { type: Schema.Types.ObjectId, ref: 'companies', required: true },
  jobTitle: { type: String, required: true },
  quantity: { type: Number, required: true },
  salary: { type: String, required: true },
  jobLocation: { type: String, required: true },
  jobType: {
    type: String,
    enum: ['EMPLOYEE', 'CONTRACT', 'INTERN'],
    required: true
  },
  jobStatus: {
    type: String,
    enum: ['FULL_TIME', 'PART_TIME', 'PER_DIEM'],
    required: true
  },
  targetDepartments: [{ type: ObjectId, ref: 'departments' }],
  description: { type: String, required: true },
  applicants: [
    {
      applicant: { type: ObjectId, ref: 'graduates' },
      recommended: { type: Boolean, required: true, default: false }
    }
  ]
})

VacancySchema.static({
  async filterFromPublic(data, doc): Promise<void> {
    try {
      if (!data || !doc) return Promise.reject('Data or document not found.')

      // targetDepartments
      doc.targetDepartments =
        data.targetDepartments || doc.targetDepartments || []

      // jobTitle
      doc.jobTitle = data.jobTitle || doc.jobTitle

      // salary
      doc.salary = data.salary || doc.salary

      // jobLocation
      doc.jobLocation = data.jobLocation || doc.jobLocation

      // quantity
      doc.quantity = data.quantity || doc.quantity

      // jobType
      doc.jobType = data.jobType || doc.jobType

      // description
      doc.description = data.description || doc.description

      // jobStatus
      doc.jobStatus = data.jobStatus || doc.jobStatus

      // applicants.[{_id?, applicant, recommended }]
      if (data.applicants && Array.isArray(data.applicants)) {
        doc.applicants = doc.applicants || []
        for (const dataApplication of data.applicants) {
          if (dataApplication.applicant && dataApplication.recommended) {
            if (dataApplication._id) {
              // update
              for (let i = 0; i < doc.applicants.length; i++) {
                if (
                  String(doc.applicants[i]._id) === String(dataApplication._id)
                ) {
                  doc.applicants[i].applicant =
                    dataApplication.applicant || doc.applicants[i].applicant
                  doc.applicants[i].recommended =
                    dataApplication.recommended || doc.applicants[i].recommended
                }
              }
            } else {
              // new?
              let isNew = true
              for (const application of doc.applicants) {
                if (
                  application.applicant === dataApplication.applicant &&
                  application.recommended === dataApplication.recommended
                ) {
                  isNew = false
                  break
                }
              }
              if (isNew)
                doc.applicants.push({
                  applicant: dataApplication.applicant,
                  recommended: dataApplication.recommended
                })
            }
          }
        }
      }

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async filterForPublic(doc): Promise<any> {
    try {
      if (!doc) return Promise.reject('No document found.')

      const filtered: any = {
        targetDepartments: [],
        applicants: []
      }

      // _id
      filtered._id = doc._id

      // createdOn
      filtered.createdOn = doc.createdOn

      // company
      filtered.company =
        doc.company instanceof CompanyModel
          ? await (CompanyModel as any).filterForPublic(doc.company)
          : doc.company

      // targetDepartments
      for (let i = 0; i < doc.targetDepartments.length; i++) {
        filtered.targetDepartments[i] =
          (await (DepartmentModel as any).filterForPublic(
            doc.targetDepartments[i]
          )) || doc.targetDepartments[i]
      }

      // jobTitle
      filtered.jobTitle = doc.jobTitle

      // salary
      filtered.salary = doc.salary

      // jobLocation
      filtered.jobLocation = doc.jobLocation

      // quantity
      filtered.quantity = doc.quantity

      // jobType
      filtered.jobType = doc.jobType

      // jobStatus
      filtered.jobStatus = doc.jobStatus

      // description
      filtered.description = doc.description

      // applicants.[{ _id, applicant, recommended }]
      if (doc.applicants && Array.isArray(doc.applicants)) {
        filtered.applicants = []
        for (const application of doc.applicants)
          if (
            application._id &&
            application.applicant &&
            application.recommended !== undefined
          )
            filtered.applicants.push({
              _id: application._id,
              applicant:
                application.applicant instanceof GraduateModel
                  ? await (GraduateModel as any).filterForPublic(
                      application.applicant
                    )
                  : application.applicant,
              recommended: application.recommended
            })
      }

      return Promise.resolve(filtered)
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

VacancySchema.index({ '$**': 'text' })
const VacancyModel = model('vacancies', VacancySchema)

export { VacancySchema, VacancyModel }
