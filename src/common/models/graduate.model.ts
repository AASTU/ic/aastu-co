import { Schema, model } from 'mongoose'
import { compare, hash } from 'bcrypt'
import { Stream } from 'stream'

import { grid } from '../configs/mongoose.config'

const ObjectId = Schema.Types.ObjectId

const GraduateSchema = new Schema({
  createdOn: { type: Date, required: true, default: Date.now },
  name: {
    first: { type: String, required: true },
    last: { type: String, required: true }
  },
  birthdate: { type: Date, required: true },
  gender: { type: String, enum: ['F', 'M'], required: true },
  nationality: { type: String, enum: ['ET', 'OTHER'], required: true },
  phone: {
    code: { type: Number, required: true },
    number: { type: String, required: true }
  },
  contactInfo: String,
  department: { type: ObjectId, ref: 'departments' },
  email: { type: String, required: true },
  password: { type: String, required: true },
  survey: {
    graduationYear: { type: Number, required: true },
    learning: { type: Boolean },
    status: {
      type: String,
      enum: ['SELF_EMPLOYED', 'COMPANY_EMPLOYED', 'UNEMPLOYED']
    },
    selfEmployedByField: Boolean, // if SELF_EMPLOYED
    jobType: { type: String, enum: ['PERMANENT', 'CONTRACT', 'INTERN'] }, // if COMPANY_EMPLOYED
    jobStatus: { type: String, enum: ['FULL_TIME', 'PART_TIME', 'PER_DIEM'] }, // if COMPANY_EMPLOYED
    company: { type: ObjectId, ref: 'companies' } // if COMPANY_EMPLOYED
  }
})

GraduateSchema.method({
  async isPasswordCorrect(pass: string): Promise<boolean> {
    try {
      if (!this.password) return Promise.reject('Password not set.')

      return compare(pass, (this as any).password)
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async setPassword(pass: string): Promise<void> {
    try {
      ;(this as any).password = await hash(pass, 12)

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async changePassword(oldPass: string, newPass: string): Promise<void> {
    try {
      if (!await this.isPasswordCorrect(oldPass))
        return Promise.reject('Wrong password.')

      return this.setPassword(newPass)
    } catch (e) {
      return Promise.reject(e)
    }
  },

  // TODO: DELETE THESE...

  picturePath(): string {
    const doc = this as any
    if (!doc._id) throw new Error('Document _id not found.')

    return `graduate/picture/${doc._id}`
  },
  async hasPicture(): Promise<boolean> {
    try {
      const doc = this as any

      return new Promise<boolean>((resolve, reject) => {
        const gfs = grid()
        if (!gfs) {
          reject('GridFS not available.')
          return
        }

        gfs.exist({ filename: doc.picturePath() }, (err, found) => {
          if (err) {
            reject(err)
            return
          }
          resolve(found)
        })
      })
    } catch (e) {
      return Promise.reject(e)
    }
  },
  async getPicture(): Promise<Stream> {
    try {
      const doc = this as any

      if (!await doc.hasPicture())
        return Promise.reject({
          code: 'NOT_FOUND',
          message: 'No picture.'
        })

      const gfs = grid()
      if (!gfs) return Promise.reject('GridFS not available.')

      return Promise.resolve(
        gfs.createReadStream({ filename: doc.picturePath() })
      )
    } catch (e) {
      return Promise.reject(e)
    }
  },
  async setPicture(stream: Stream): Promise<void> {
    try {
      const doc = this as any

      if (await doc.hasPicture()) await doc.removePicture(false)

      return new Promise<void>((resolve, reject) => {
        const gfs = grid()
        if (!gfs) {
          reject('GridFS not available.')
          return
        }

        const writeStream = gfs.createWriteStream({
          filename: doc.picturePath()
        })
        writeStream.once('finish', () => resolve())
        writeStream.once('error', err =>
          doc
            .removePicture(false)
            .then(() => reject(err))
            .catch(rmvErr => reject(rmvErr))
        )

        stream.pipe(writeStream)
      })
    } catch (e) {
      return Promise.reject(e)
    }
  },
  async removePicture(check = true): Promise<void> {
    try {
      const doc = this as any

      if (check && !await doc.hasPicture())
        return Promise.reject('No picture to remove.')

      return new Promise<void>((resolve, reject) => {
        const gfs = grid()
        if (!gfs) {
          reject('GridFS not available.')
          return
        }

        gfs.remove({ filename: doc.picturePath() }, err => {
          if (err) {
            reject(err)
            return
          }
          resolve()
        })
      })
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

GraduateSchema.static({
  async filterFromPublic(data, doc): Promise<void> {
    try {
      if (!data || !doc) return Promise.reject('No data or document found.')

      doc.name =
        data.name || doc.name
          ? {
              first: data.name.first || doc.name.first,
              last: data.name.last || doc.name.last
            }
          : undefined

      // birthdate
      doc.birthdate = data.birthdate || doc.birthdate

      // gender
      doc.gender = data.gender || doc.gender

      // nationality
      doc.nationality = data.nationality || doc.nationality

      // phone
      doc.phone =
        data.phone || doc.phone
          ? {
              code: data.phone.code || doc.phone.code,
              number: data.phone.number || doc.phone.number
            }
          : undefined

      // contactInfo
      doc.contactInfo = data.contactInfo || doc.contactInfo

      // department
      doc.department = data.department || doc.department

      //email
      doc.email = data.email || doc.email

      //password
      !data.password || (await doc.setPassword(data.password))

      // isGraduated
      doc.isGraduated = data.isGraduated || doc.isGraduated

      // survey
      doc.survey = {
        graduationYear: data.survey
          ? data.survey.graduationYear
          : doc.survey ? doc.survey.graduationYear : undefined,
        status: data.survey
          ? data.survey.status
          : doc.survey ? doc.survey.status : undefined,
        learning: data.survey
          ? data.survey.learning
          : doc.survey ? doc.survey.learning : undefined,
        company: data.survey
          ? data.survey.company
          : doc.survey ? doc.survey.company : undefined,
        jobType: data.survey
          ? data.survey.jobType
          : doc.survey ? doc.survey.jobType : undefined,
        jobStatus: data.survey
          ? data.survey.jobStatus
          : doc.survey ? doc.survey.jobStatus : undefined,
        selfEmployedByField: data.survey
          ? data.survey.selfEmployedByField
          : doc.survey ? doc.survey.selfEmployedByField : undefined
      }

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async filterForPublic(doc): Promise<any> {
    try {
      if (!doc) return Promise.reject('No document found.')

      const filtered: any = {}

      // _id
      filtered._id = doc._id

      // createdOn
      filtered.createdOn = doc.createdOn

      // name
      filtered.name = doc.name
        ? {
            first: doc.name.first,
            last: doc.name.last
          }
        : undefined

      // birthdate
      filtered.birthdate = doc.birthdate

      // gender
      filtered.gender = doc.gender

      // nationality
      filtered.nationality = doc.nationality

      // phone
      filtered.phone = doc.phone
        ? {
            code: doc.phone.code,
            number: doc.phone.number
          }
        : undefined

      // contactInfo
      filtered.contactInfo = doc.contactInfo

      // department
      filtered.department = doc.department

      // email
      filtered.email = doc.email

      // isGraduated
      filtered.isGraduated = doc.isGraduated

      // survey
      filtered.survey = doc.survey
        ? {
            graduationYear: doc.survey.graduationYear,
            status: doc.survey.status,
            learning: doc.survey.learning,
            company: doc.survey.company,
            jobType: doc.survey.jobType,
            jobStatus: doc.survey.jobStatus,
            selfEmployedByField: doc.survey.selfEmployedByField
          }
        : undefined

      return Promise.resolve(filtered)
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

GraduateSchema.index({ '$**': 'text' })
const GraduateModel = model('graduates', GraduateSchema)

export { GraduateSchema, GraduateModel }
