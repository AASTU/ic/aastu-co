import { Schema, model } from 'mongoose'
import { compare, hash } from 'bcrypt'

const DepartmentSchema = new Schema({
  createdOn: { type: Date, required: true, default: Date.now },
  name: { type: String, required: true },
  headName: {
    first: { type: String, required: true },
    last: { type: String, required: true }
  },
  collegeName: { type: String, required: true },
  phone: {
    code: { type: Number, required: true },
    number: { type: String, required: true }
  },
  contactInfo: String,
  description: String,
  email: { type: String, required: true },
  password: { type: String, required: true }
})

DepartmentSchema.method({
  async isPasswordCorrect(pass: string): Promise<boolean> {
    try {
      if (!this.password) return Promise.reject('Password not set.')

      return compare(pass, (this as any).password)
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async setPassword(pass: string): Promise<void> {
    try {
      ;(this as any).password = await hash(pass, 12)

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async changePassword(oldPass: string, newPass: string): Promise<void> {
    try {
      if (!await this.isPasswordCorrect(oldPass))
        return Promise.reject('Wrong password.')

      return this.setPassword(newPass)
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

DepartmentSchema.static({
  async filterFromPublic(data, doc): Promise<void> {
    try {
      if (!data || !doc) return Promise.reject('No data or document found.')

      // name
      doc.name = data.name || doc.name

      // headName
      doc.headName =
        data.headName || doc.headName
          ? {
              first: data.headName.first || doc.headName.first,
              last: data.headName.last || doc.headName.last
            }
          : {}

      // collegeName
      doc.collegeName = data.collegeName || doc.collegeName

      // phone
      doc.phone =
        data.phone || doc.phone
          ? {
              code: data.phone.code || doc.phone.code,
              number: data.phone.number || doc.phone.number
            }
          : {}

      // contactInfo
      doc.contactInfo = data.contactInfo || doc.contactInfo

      // description
      doc.description = data.description || doc.description

      // email
      doc.email = data.email || doc.email

      !data.password || (await doc.setPassword(data.password))

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async filterForPublic(doc): Promise<any> {
    try {
      if (!doc) return Promise.reject('No document found.')

      const filtered: any = {}

      // _id
      filtered._id = doc._id

      // createdOn
      filtered.createdOn = doc.createdOn

      // name
      filtered.name = doc.name

      // headName
      filtered.headName = doc.headName
        ? {
            first: doc.headName.first,
            last: doc.headName.last
          }
        : {}

      // collegeName
      filtered.collegeName = doc.collegeName

      // phone
      filtered.phone = doc.phone
        ? {
            code: doc.phone.code,
            number: doc.phone.number
          }
        : {}

      // contactInfo
      filtered.contactInfo = doc.contactInfo

      // description
      filtered.description = doc.description

      // email
      filtered.email = doc.email

      return Promise.resolve(filtered)
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

DepartmentSchema.index({ '$**': 'text' })

const DepartmentModel = model('departments', DepartmentSchema)

export { DepartmentSchema, DepartmentModel }
