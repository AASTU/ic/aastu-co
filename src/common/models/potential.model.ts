import { Schema, model } from 'mongoose'

const PotentialSchema = new Schema({
  createdOn: { type: Date, required: true, default: Date.now },
  name: { type: String, required: true },
  contactInfo: { type: String }
})

PotentialSchema.static({
  async filterFromPublic(data, doc): Promise<void> {
    try {
      if (!data || !doc) return Promise.reject('Data or document not found.')

      // name
      doc.name = data.name || doc.name

      // contactInfo
      doc.contactInfo = data.contactInfo || doc.contactInfo

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async filterForPublic(doc): Promise<any> {
    try {
      if (!doc) return Promise.reject('Doc parameter not found.')

      const filtered: any = {}

      // _id
      filtered._id = doc._id

      // createdOn
      filtered.createdOn = doc.createdOn

      // name
      filtered.name = doc.name

      // contactInfo
      filtered.contactInfo = doc.contactInfo

      return Promise.resolve(filtered)
    } catch (e) {
      return Promise.reject(e)
    }
  }
})

const PotentialModel = model('potentials', PotentialSchema)

export { PotentialSchema, PotentialModel }
