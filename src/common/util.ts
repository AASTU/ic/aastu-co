import { createTransport } from 'nodemailer'
import { Duplex } from 'stream'

/* EMAIL */

// send an email
export function sendEmail(email) {
  if (!email) return Promise.reject("'email' parameter is not found.")
  if (!email.from && !process.env.EMAIL_ADDRESS)
    return Promise.reject('Sender email address not provided.')
  if (!email.pass && !process.env.EMAIL_PASSWORD)
    return Promise.reject('Sender email password not provided.')
  if (!email.to) return Promise.reject('Target email address not provided.')
  if (!email.html && !email.text)
    return Promise.reject("Neither 'email.html' nor 'email.text' is provided.")

  return createTransport({
    service: email.service || process.env.EMAIL_SERVICE || 'gmail',
    auth: {
      user: email.from || process.env.EMAIL_ADDRESS,
      pass: email.pass || process.env.EMAIL_PASSWORD
    }
  }).sendMail({
    from: email.from || process.env.EMAIL_ADDRESS,
    to: email.to,
    subject: email.subject || '',
    html: email.html,
    text: !email.html ? email.text : undefined
  })
}

/* RESPONSE STANDARDS */

// parse "success" response format
export function successFormat(data?) {
  return Object.assign({ success: true }, data)
}

// parse "problem" response format
export function problemFormat(error, data?) {
  return Object.assign(
    {
      success: false,
      problem: {
        code: typeof error === 'object' ? error.code : undefined,
        status: typeof error === 'object' ? error.status : undefined,
        message:
          typeof error === 'object'
            ? error.message || error.errmsg
            : typeof error === 'string' ? error : undefined
      }
    },
    data
  )
}

// string to buffer
export function bufferToStream(buffer) {
  let stream = new Duplex()
  stream.push(buffer)
  stream.push(null)
  return stream
}
