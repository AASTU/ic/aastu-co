import { initialize, session } from 'passport'
import * as bodyParser from 'body-parser'
import * as express from 'express'
import logger from 'morgan'
import cookieParser from 'cookie-parser'
import methodOverride from 'method-override'
import busyBoyBodyParser from 'busboy-body-parser'
import expressSession from 'express-session'

export default (app, config) => {
  app.use(logger('dev'))
  app.use(methodOverride('_method'))
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(express.json())
  app.use(
    expressSession({
      secret: 'aastu-co', // todo
      resave: false,
      saveUninitialized: true,
      name: config.name,
      cookie: {
        maxAge: 7 * 24 * 60 * 60 * 1000,
        signed: true
      }
    })
  )

  app.use(cookieParser())
  app.use(initialize())
  app.use(session())
  app.use(busyBoyBodyParser())
  /*
  // todo: research more about this code below, regrading security...
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,')
    res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    next()
  })
  */

  /*  app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Credentials', true)
    res.header('Access-Control-Allow-Origin', req.headers.origin)
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    res.header(
      'Access-Control-Allow-Headers',
      'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
    )
    next()
  })*/
}
