import gridFS from 'gridfs-stream'
import { connect, Connection, mongo } from 'mongoose'

export let connection: Connection

export function grid(): gridFS.Grid {
  return gridFS(connection.db, mongo)
}

export default async function connectDb(config): Promise<Connection> {
  try {
    const dbURI = config.dev.db

    connection = (await connect(dbURI)).connection
    console.log('mongoose connected to ' + dbURI)

    connection.on('disconnected', () =>
      console.log('mongoose disconnected from ' + dbURI)
    )
    connection.on('error', err => console.error('mongoose error: ' + err))

    return Promise.resolve(connection)
  } catch (e) {
    console.error('mongoose connection problem:', e)
    return Promise.reject(e)
  }
}
