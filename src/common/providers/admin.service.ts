import { AdminModel } from '../models/admin.model'

class AdminService {
  static async add(data) {
    try {
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = new AdminModel()
      await (AdminModel as any).filterFromPublic(data, doc)

      await doc.save()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async get(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await AdminModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      return (AdminModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async edit(_id, data) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = await AdminModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')
      await (AdminModel as any).filterFromPublic(data, doc)

      await doc.save()

      return (AdminModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async remove(_id) {
    try {
      if (!_id) return Promise.reject('_id not found')

      const doc = await AdminModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      await doc.remove()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

export { AdminService }
