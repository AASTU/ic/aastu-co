import { GraduateModel } from '../models/graduate.model'
import { Schema } from 'mongoose'

type ObjectId = Schema.Types.ObjectId | string

export class StatsService {
  /* config... */
  constructor(
    public gender?: 'F' | 'M',
    public department_id?: ObjectId,
    public company_id?: ObjectId
  ) {}

  /* util... */

  private static removeUndefined(obj: any): any {
    for (const key in obj)
      if (obj.hasOwnProperty(key) && obj[key] === undefined) delete obj[key]

    return obj
  }

  /* SELF_EMPLOYED... */

  async se_total(): Promise<number> {
    try {
      return Promise.resolve(
        await GraduateModel.find(
          StatsService.removeUndefined({
            gender: this.gender,
            department: this.department_id,
            'survey.status': 'SELF_EMPLOYED'
          })
        ).count()
      )
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async se_learning(): Promise<number> {
    try {
      return Promise.resolve(
        await GraduateModel.find(
          StatsService.removeUndefined({
            gender: this.gender,
            department: this.department_id,
            'survey.status': 'SELF_EMPLOYED',
            'survey.learning': true
          })
        ).count()
      )
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async se_byField(): Promise<number> {
    try {
      return Promise.resolve(
        await GraduateModel.find(
          StatsService.removeUndefined({
            gender: this.gender,
            department: this.department_id,
            'survey.status': 'SELF_EMPLOYED',
            'survey.selfEmployedByField': true
          })
        ).count()
      )
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /* COMPANY_EMPLOYED... */

  async ce_total(): Promise<number> {
    try {
      return Promise.resolve(
        await GraduateModel.find(
          StatsService.removeUndefined({
            gender: this.gender,
            department: this.department_id,
            'survey.company': this.company_id,
            'survey.status': 'COMPANY_EMPLOYED'
          })
        ).count()
      )
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async ce_learning(): Promise<number> {
    try {
      return Promise.resolve(
        await GraduateModel.find(
          StatsService.removeUndefined({
            gender: this.gender,
            department: this.department_id,
            'survey.company': this.company_id,
            'survey.status': 'COMPANY_EMPLOYED',
            'survey.learning': true
          })
        ).count()
      )
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async ce_jobType(
    jobType: 'PERMANENT' | 'CONTRACT' | 'INTERN'
  ): Promise<number> {
    try {
      return Promise.resolve(
        await GraduateModel.find(
          StatsService.removeUndefined({
            gender: this.gender,
            department: this.department_id,
            'survey.company': this.company_id,
            'survey.status': 'COMPANY_EMPLOYED',
            'survey.jobType': jobType
          })
        ).count()
      )
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async ce_jobStatus(
    jobStatus: 'FULL_TIME' | 'PART_TIME' | 'PER_DIME'
  ): Promise<number> {
    try {
      return Promise.resolve(
        await GraduateModel.find(
          StatsService.removeUndefined({
            gender: this.gender,
            department: this.department_id,
            'survey.company': this.company_id,
            'survey.status': 'COMPANY_EMPLOYED',
            'survey.jobStatus': jobStatus
          })
        ).count()
      )
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /* UNEMPLOYED... */

  async ue_total(): Promise<number> {
    try {
      return Promise.resolve(
        await GraduateModel.find(
          StatsService.removeUndefined({
            gender: this.gender,
            department: this.department_id,
            'survey.status': 'UNEMPLOYED'
          })
        ).count()
      )
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async ue_learning(): Promise<number> {
    try {
      return Promise.resolve(
        await GraduateModel.find(
          StatsService.removeUndefined({
            gender: this.gender,
            department: this.department_id,
            'survey.status': 'UNEMPLOYED',
            'survey.learning': true
          })
        ).count()
      )
    } catch (e) {
      return Promise.reject(e)
    }
  }
}
