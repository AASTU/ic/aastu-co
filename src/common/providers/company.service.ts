import { CompanyModel } from '../models/company.model'

class CompanyService {
  static async add(data) {
    try {
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = new CompanyModel()
      await (CompanyModel as any).filterFromPublic(data, doc)

      await doc.save()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async get(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await CompanyModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      return (CompanyModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async getAll() {
    try {
      const docs = await (CompanyModel as any).find({})
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (CompanyModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async list(count, since = null) {
    try {
      if (!count) return Promise.reject('Count parameter not found.')

      const docs = await (CompanyModel as any)
        .find({})
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (CompanyModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async search(term, count, since = null) {
    try {
      if (!term) return Promise.reject('Search term parameter not found.')
      if (!count) return Promise.reject('Count parameter not found.')

      const docs = await CompanyModel.find({ $text: { $search: term } })
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (CompanyModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async edit(_id, data) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = await CompanyModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')
      await (CompanyModel as any).filterFromPublic(data, doc)

      await doc.save()

      return (CompanyModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async remove(_id) {
    try {
      if (!_id) return Promise.reject('_id not found')

      const doc = await CompanyModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      await doc.remove()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

export { CompanyService }
