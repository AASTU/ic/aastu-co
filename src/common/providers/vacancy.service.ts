import { VacancyModel } from '../models/vacancy.model'

class VacancyService {
  static async add(data, id) {
    try {
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = new VacancyModel()
      await (VacancyModel as any).filterFromPublic(data, doc)
      ;(doc as any).company = id

      await doc.save()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  //todo deprecated
  static async get(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await VacancyModel.findById(_id)
        .populate('applicants.applicant', '')
        .populate('company')
        .populate('targetDepartments')

      if (!doc) return Promise.reject('Document not found.')

      return Promise.resolve((VacancyModel as any).filterForPublic(doc))
    } catch (e) {
      return Promise.reject(e)
    }
  }

  //todo deprecated
  static async list(count, since = null) {
    try {
      if (!count) return Promise.reject('Count parameter not found.')
      const docs = await (VacancyModel as any)
        .find({})
        .populate('applicants.applicant')
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++) {
        docs[i] = await (VacancyModel as any).filterForPublic(docs[i])
      }
      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  //todo deprecated
  static async search(term, count, since = null) {
    try {
      if (!term) return Promise.reject('Search term parameter not found.')
      if (!count) return Promise.reject('Count parameter not found.')

      const docs = await VacancyModel.find({ $text: { $search: term } })
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (VacancyModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async edit(_id, data) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = await VacancyModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')
      await (VacancyModel as any).filterFromPublic(data, doc)

      await doc.save()
      return (VacancyModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async apply(_id, graduate_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')
      if (!graduate_id)
        return Promise.reject('graduate_id parameter not found.')

      const doc = await VacancyModel.findOne({
        _id: _id
      })

      if (!doc) return Promise.reject('Applicant not found.')

      if ((doc as any).applicants.length == 0) {
        ;(doc as any).applicants.push({
          applicant: graduate_id,
          recommended: false
        })
      } else {
        let flag = false
        for (let i = 0; i < (doc as any).applicants.length; i++) {
          if ((doc as any).applicants[i].applicant.equals(graduate_id)) {
            ;(doc as any).applicants.splice(i, 1)
            flag = true
            break
          }
        }
        for (let i = 0; i < (doc as any).applicants.length; i++) {
          if (!flag) {
            if (!(doc as any).applicants[i].applicant.equals(graduate_id)) {
              ;(doc as any).applicants.push({
                applicant: graduate_id,
                recommended: false
              })
              break
            }
          }
        }
      }

      await doc.save()

      return Promise.resolve((doc as any).applicants.length)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async recommend(_id, graduate_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')
      if (!graduate_id)
        return Promise.reject('graduate_id parameter not found.')

      const doc = await VacancyModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      for (let i = 0; i < (doc as any).applicants.length; i++) {
        if ((doc as any).applicants[i].applicant.equals(graduate_id))
          (doc as any).applicants[i].recommended = !(doc as any).applicants[i]
            .recommended
      }

      await doc.save()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async remove(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await VacancyModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      await doc.remove()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /* Filtering Mechanisms for each Apps*/

  //===for department===//
  //getByCompany
  static async getByCompany(vacancyId, companyId) {
    try {
      if (!vacancyId) return Promise.reject('vacancyId parameter not found.')
      if (!companyId) return Promise.reject('companyId parameter not found.')

      const doc = await VacancyModel.findOne({
        _id: vacancyId,
        company: companyId
      })
        .populate('applicants.applicant', '')
        .populate('company')
        .populate('targetDepartments')

      if (!doc) return Promise.reject('Document not found.')

      return Promise.resolve((VacancyModel as any).filterForPublic(doc))
    } catch (err) {
      return Promise.reject(err)
    }
  }

  //listByCompany
  static async listByCompany(count, since = null, companyId) {
    try {
      if (!count) return Promise.reject('Count parameter not found.')
      if (!companyId) return Promise.reject('companyId parameter not found.')

      const docs = await VacancyModel.find({ company: companyId })
        .populate('targetDepartments')
        .populate('applicants.applicant')
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))

      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++) {
        docs[i] = await (VacancyModel as any).filterForPublic(docs[i])
      }

      return Promise.resolve(docs)
    } catch (err) {
      return Promise.reject(err)
    }
  }

  // searchByCompany
  static async searchByCompany(term, count, since = null, companyId) {
    try {
      if (!term) return Promise.reject('Search term parameter not found.')
      if (!count) return Promise.reject('Count parameter not found.')
      if (!companyId) return Promise.reject('companyId parameter not found.')

      const docs = await VacancyModel.find({
        $text: { $search: term },
        company: companyId
      })
        .populate('targetDepartments')
        .populate('applicants.applicant')
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))

      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (VacancyModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  //===for department====//
  //getByDepartment ->targetDepartment

  static async getByDepartment(vacancyId, departmentId) {
    try {
      if (!vacancyId) return Promise.reject('vacancyId parameter not found.')
      if (!departmentId) return Promise.reject('companyId parameter not found.')

      const doc = await VacancyModel.findOne({
        _id: vacancyId,
        targetDepartments: departmentId
      })
        .populate('applicants.applicant', '')
        .populate('company')
        .populate('targetDepartments')

      if (!doc) return Promise.reject('Document not found.')

      let temp: any[] = []
      for (let i = 0; i < (doc as any).applicants.length; i++) {
        if (
          (doc as any).applicants[i].applicant.department.equals(departmentId)
        ) {
          temp.push((doc as any).applicants[i])
        }
      }

      ;(doc as any).applicants = temp

      return Promise.resolve((VacancyModel as any).filterForPublic(doc))
    } catch (err) {
      return Promise.reject(err)
    }
  }

  //listByDepartment
  static async listByDepartment(count, since = null, departmentId) {
    try {
      if (!count) return Promise.reject('Count parameter not found.')
      if (!departmentId)
        return Promise.reject('departmentId parameter not found.')

      const docs = await VacancyModel.find({ targetDepartments: departmentId })
        .populate('targetDepartments')
        .populate('applicants.applicant')
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))

      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++) {
        let temp: any[] = []
        for (let j = 0; j < (docs[i] as any).applicants.length; j++) {
          if (
            (docs[i] as any).applicants[j].applicant.department.equals(
              departmentId
            )
          ) {
            temp.push((docs[i] as any).applicants[j])
          }
        }
        ;(docs[i] as any).applicants = temp
        docs[i] = await (VacancyModel as any).filterForPublic(docs[i])
      }

      return Promise.resolve(docs)
    } catch (err) {
      return Promise.reject(err)
    }
  }

  //searchByDepartment
  static async searchByDepartment(term, count, since = null, departmentId) {
    try {
      if (!term) return Promise.reject('Search term parameter not found.')
      if (!count) return Promise.reject('Count parameter not found.')
      if (!departmentId)
        return Promise.reject('departmentId parameter not found.')

      const docs = await VacancyModel.find({
        $text: { $search: term },
        targetDepartments: departmentId
      })
        .populate('targetDepartments')
        .populate('applicants.applicant')
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))

      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++) {
        let temp: any[] = []
        for (let j = 0; j < (docs[i] as any).applicants.length; j++) {
          if (
            (docs[i] as any).applicants[j].applicant.department.equals(
              departmentId
            )
          ) {
            temp.push((docs[i] as any).applicants[j])
          }
        }
        ;(docs[i] as any).applicants = temp
        docs[i] = await (VacancyModel as any).filterForPublic(docs[i])
      }

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  //===for graduates====//

  //getByDepartment without populating applicant list
  static async getByDepartmentForGrad(vacancyId, departmentId) {
    try {
      if (!vacancyId) return Promise.reject('vacancyId parameter not found.')
      if (!departmentId) return Promise.reject('companyId parameter not found.')

      const doc = await VacancyModel.findOne({
        _id: vacancyId,
        targetDepartments: departmentId
      })
        .populate('company')
        .populate('targetDepartments')

      if (!doc) {
        return Promise.reject('Document not found.')
      }

      return Promise.resolve((VacancyModel as any).filterForPublic(doc))
    } catch (err) {
      return Promise.reject(err)
    }
  }

  //listByDepartment without populating applicant list
  static async listByDepartmentForGrad(count, since = null, departmentId) {
    try {
      if (!count) return Promise.reject('Count parameter not found.')
      if (!departmentId)
        return Promise.reject('departmentId parameter not found.')

      const docs = await VacancyModel.find({ targetDepartments: departmentId })
        .populate('targetDepartments')
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))

      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++) {
        docs[i] = await (VacancyModel as any).filterForPublic(docs[i])
      }

      return Promise.resolve(docs)
    } catch (err) {
      return Promise.reject(err)
    }
  }

  //searchBYDepartment without populating applicant list
  static async searchByDepartmentForGrad(
    term,
    count,
    since = null,
    departmentId
  ) {
    try {
      if (!term) return Promise.reject('Search term parameter not found.')
      if (!count) return Promise.reject('Count parameter not found.')
      if (!departmentId)
        return Promise.reject('departmentId parameter not found.')

      const docs = await VacancyModel.find({
        $text: { $search: term },
        targetDepartments: departmentId
      })
        .populate('targetDepartments')
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))

      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++) {
        docs[i] = await (VacancyModel as any).filterForPublic(docs[i])
      }

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

export { VacancyService }
