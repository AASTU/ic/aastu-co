import { Handler } from 'express'

import { problemFormat } from '../util'

// fail if not logged in // todo: implement this
export function failIfNotLoggedIn(
  problem = { code: 'NOT_LOGGED_IN', message: 'Not logged in.' },
  data?: any
): Handler {
  return async (req, res, next) => {
    if (req.isAuthenticated()) await next()
    else res.json(problemFormat(problem, data))
  }
}
