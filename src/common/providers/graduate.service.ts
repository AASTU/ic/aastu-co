import { Schema } from 'mongoose'
import { Stream } from 'stream'
import { stringify } from 'qs'

import { GraduateModel } from '../models/graduate.model'
import { VacancyModel } from '../models/vacancy.model'
import { CompanyModel } from '../models/company.model'

class GraduateService {
  static async add(data) {
    try {
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = new GraduateModel()
      await (GraduateModel as any).filterFromPublic(data, doc)

      await doc.save()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  //===For Departments===//
  static async getByDepartment(id, departmentId) {
    try {
      if (!id) return Promise.reject('_id parameter not found.')
      if (!departmentId)
        return Promise.reject('departmentId parameter not found.')

      const doc = await GraduateModel.findOne({
        _id: id,
        department: departmentId
      })
      if (!doc) return Promise.reject('Document not found.')

      return (GraduateModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async listByDepartment(count, since = null, departmentId) {
    try {
      if (!count) return Promise.reject('Count parameter not found.')
      if (!departmentId)
        return Promise.reject('departmentId parameter not found.')

      const docs = await (GraduateModel as any)
        .find({ department: departmentId })
        .populate('department')
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (GraduateModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async searchByDepartment(term, count, since = null, departmentId) {
    try {
      if (!term) return Promise.reject('Search term parameter not found.')
      if (!count) return Promise.reject('Count parameter not found.')
      if (!departmentId)
        return Promise.reject('departmentId parameter not found.')

      const docs = await GraduateModel.find({
        $text: { $search: term },
        department: departmentId
      })
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (GraduateModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  //===For Graduate ===//
  static async get(id) {
    try {
      if (!id) return Promise.reject('_id parameter not found.')

      const doc = await GraduateModel.findOne({ _id: id })
      if (!doc) return Promise.reject('Document not found.')

      return (GraduateModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async edit(_id, data) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = await GraduateModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')
      await (GraduateModel as any).filterFromPublic(data, doc)

      await doc.save()

      return (GraduateModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  //todo deprecated
  static async graduate(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await VacancyModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')
      if ((doc as any).isGraduated) return Promise.reject('Already graduated.')
      ;(doc as any).isGraduated = true

      await doc.save()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  //this is for the graduates only
  static async remove(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await GraduateModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      await doc.remove()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async addPicture(
    _id: Schema.Types.ObjectId,
    data: Stream
  ): Promise<void> {
    try {
      if (!_id) return Promise.reject('"_id" parameter not found.')
      if (!data) return Promise.reject('"data" parameter not found.')

      const doc = await GraduateModel.findById(_id).exec()
      if (!doc) return Promise.reject(`Document not found.`)

      return Promise.resolve(await (doc as any).setPicture(data))
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async hasPicture(_id: Schema.Types.ObjectId): Promise<boolean> {
    try {
      if (!_id) return Promise.reject('"_id" parameter not found.')

      const p = await GraduateModel.findById(_id).exec()
      if (!p) return Promise.reject(`Document not found.`)

      return (p as any).hasPicture()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async getPicture(_id: Schema.Types.ObjectId): Promise<Stream> {
    try {
      if (!_id) return Promise.reject('"_id" parameter not found.')

      const p = await GraduateModel.findById(_id).exec()
      if (!p) return Promise.reject(`Document not found.`)

      return (p as any).getPicture()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async removePicture(
    _id: Schema.Types.ObjectId,
    check = true
  ): Promise<void> {
    try {
      if (!_id) return Promise.reject('"_id" parameter not found.')

      const doc = await GraduateModel.findById(_id).exec()
      if (!doc) return Promise.reject(`Document not found.`)

      return (doc as any).removePicture(check)
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

export { GraduateService }
