import { PotentialModel } from '../models/potential.model'

class PotentialService {
  static async add(data) {
    try {
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = new PotentialModel()
      await (PotentialModel as any).filterFromPublic(data, doc)

      await doc.save()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async get(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await PotentialModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      return (PotentialModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async edit(_id, data) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = await PotentialModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')
      await (PotentialModel as any).filterFromPublic(data, doc)

      await doc.save()

      return (PotentialModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async remove(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await PotentialModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      await doc.remove()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

export { PotentialService }
