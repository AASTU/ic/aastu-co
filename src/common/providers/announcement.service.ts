import { AnnouncementModel } from '../models/announcement.model'
import { Document, Schema } from 'mongoose'
import { Stream } from 'stream'
import { VacancyModel } from '../models/vacancy.model'

class AnnouncementService {
  static async add(department_id: string, data: any): Promise<Document> {
    try {
      if (!department_id)
        return Promise.reject('"department_id" parameter not found.')
      if (!data) return Promise.reject('"data" parameter not found.')

      const doc = new AnnouncementModel({ department: department_id })
      await (AnnouncementModel as any).filterFromPublic(data, doc)

      await doc.save()

      return Promise.resolve(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async get(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await AnnouncementModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      return (AnnouncementModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async list(count, since = null) {
    try {
      if (!count) return Promise.reject('Count parameter not found.')

      const docs = await (AnnouncementModel as any)
        .find({})
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (AnnouncementModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async search(term, count, since = null) {
    try {
      if (!term) return Promise.reject('Search term parameter not found.')
      if (!count) return Promise.reject('Count parameter not found.')

      const docs = await AnnouncementModel.find({ $text: { $search: term } })
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (AnnouncementModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async edit(_id, data) {
    try {
      if (!_id) return Promise.reject('_id parameter not found,')
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = await AnnouncementModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')
      await (AnnouncementModel as any).filterFromPublic(data, doc)

      await doc.save()

      return (AnnouncementModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async interest(id, graduate_id) {
    try {
      if (!id) return Promise.reject('_id parameter not found.')
      if (!graduate_id)
        return Promise.reject('graduate_id parameter not found.')

      const doc = await AnnouncementModel.findOne({
        _id: id
      })

      if (!doc) return Promise.reject('Applicant not found.')

      if ((doc as any).interested.length == 0) {
        ;(doc as any).interested.push(graduate_id)
      } else {
        let flag = false
        for (let i = 0; i < (doc as any).interested.length; i++) {
          if ((doc as any).interested[i].equals(graduate_id)) {
            ;(doc as any).interested.splice(i, 1)
            flag = true
            break
          }
        }
        for (let i = 0; i < (doc as any).interested.length; i++) {
          if (!flag) {
            if (!(doc as any).interested[i].equals(graduate_id)) {
              ;(doc as any).interested.push(graduate_id)
              break
            }
          }
        }
      }

      await doc.save()

      return Promise.resolve((doc as any).interested.length)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async remove(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await AnnouncementModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      await doc.remove()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  //=== for ==//

  static async getByDepartment(id, departmentId) {
    try {
      if (!id) return Promise.reject('_id parameter not found.')

      const doc = await AnnouncementModel.findOne({
        _id: id,
        department: departmentId
      })
      if (!doc) return Promise.reject('Document not found.')

      return (AnnouncementModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async listByDepartment(count, since = null, departmentId) {
    try {
      if (!count) return Promise.reject('Count parameter not found.')
      if (!departmentId)
        return Promise.reject('departmentId parameter not found.')

      const docs = await (AnnouncementModel as any)
        .find({ department: departmentId })
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (AnnouncementModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async searchByDepartment(term, count, since = null, departmentId) {
    try {
      if (!term) return Promise.reject('Search term parameter not found.')
      if (!count) return Promise.reject('Count parameter not found.')
      if (!departmentId)
        return Promise.reject('departmentId parameter not found.')

      const docs = await AnnouncementModel.find({
        $text: { $search: term },
        department: departmentId
      })
        .sort({ createdOn: -1 })
        .where('createdOn')
        .lt(Number(since) || Date.now())
        .limit(Number(count))
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (AnnouncementModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /*=============== File ==================*/
  static async addFile(
    _id: Schema.Types.ObjectId,
    data: Stream
  ): Promise<void> {
    try {
      if (!_id) return Promise.reject('"_id" parameter not found.')
      if (!data) return Promise.reject('"data" parameter not found.')

      const doc = await AnnouncementModel.findById(_id).exec()
      if (!doc) return Promise.reject(`Document not found.`)

      return Promise.resolve(await (doc as any).setFile(data))
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async hasFile(_id: Schema.Types.ObjectId): Promise<boolean> {
    try {
      if (!_id) return Promise.reject('"_id" parameter not found.')

      const p = await AnnouncementModel.findById(_id).exec()
      if (!p) return Promise.reject(`Document not found.`)

      return (p as any).hasFile()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async getFile(_id: Schema.Types.ObjectId): Promise<Stream> {
    try {
      if (!_id) return Promise.reject('"_id" parameter not found.')

      const p = await AnnouncementModel.findById(_id).exec()
      if (!p) return Promise.reject(`Document not found.`)

      return (p as any).getFile()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async removeFile(
    _id: Schema.Types.ObjectId,
    check = true
  ): Promise<void> {
    try {
      if (!_id) return Promise.reject('"_id" parameter not found.')

      const doc = await AnnouncementModel.findById(_id).exec()
      if (!doc) return Promise.reject(`Document not found.`)

      return (doc as any).removeFile(check)
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

export { AnnouncementService }
