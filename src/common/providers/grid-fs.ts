import { Schema, Model, Document } from 'mongoose'
import { Stream } from 'stream'

import { grid } from '../configs/mongoose.config'

type M = Model<Document>
type ObjectId = Schema.Types.ObjectId

export function filePath(model: M, _id: ObjectId, type: string, file: string) {
  return `${model.modelName.toLowerCase()}/${_id}/${type}/${file}`
}

export async function hasFile(
  model: M,
  _id: ObjectId,
  type: string,
  file = 'default'
): Promise<boolean> {
  try {
    if (!_id) return Promise.reject('"_id" parameter not found.')

    const doc = await model.findById(_id).exec()
    if (!doc) return Promise.reject(`No document found using _id '${_id}'.`)

    return new Promise<boolean>((resolve, reject) => {
      const gfs = grid()
      if (!gfs) return reject('GridFS not available.')

      gfs.exist(
        { filename: filePath(model, _id, type, file) },
        (err, found) => {
          if (err) {
            reject(err)
            return
          }
          resolve(found)
        }
      )
    })
  } catch (e) {
    return Promise.reject(e)
  }
}

export async function setFile(
  model: M,
  _id: ObjectId,
  type: string,
  data: Stream
) {
  await removeFile(model, _id, type, 'default', false)

  return addFile(model, _id, type, data, 'default')
}

export async function addFile(
  model: M,
  _id: ObjectId,
  type: string,
  data: Stream,
  file: string
): Promise<void> {
  try {
    if (!_id) return Promise.reject('"_id" parameter not found.')
    if (!data) return Promise.reject('"data" parameter not found.')

    const doc = await model.findById(_id).exec()
    if (!doc) return Promise.reject(`No document found using _id '${_id}'.`)

    return new Promise<void>((resolve, reject) => {
      const gfs = grid()
      if (!gfs) return reject('GridFS not available.')

      const writeStream = gfs.createWriteStream({
        filename: filePath(model, _id, type, file)
      })
      writeStream.once('finish', () => resolve())
      writeStream.once('error', err =>
        removeFile(model, _id, type, file, false)
          .then(() => reject(err))
          .catch(rmvErr => reject(rmvErr))
      )

      data.pipe(writeStream)
    })
  } catch (e) {
    return Promise.reject(e)
  }
}

export async function getFile(
  model: M,
  _id: ObjectId,
  type: string,
  file = 'default'
): Promise<Stream> {
  try {
    if (!_id) return Promise.reject('"_id" parameter not found.')

    const doc = await model.findById(_id).exec()
    if (!doc) return Promise.reject(`No document found using _id '${_id}'.`)

    if (!await hasFile(model, _id, type, file))
      return Promise.reject({
        code: 'NOT_FOUND',
        message: 'No file.'
      })

    const gfs = grid()
    if (!gfs) return Promise.reject('GridFS not available.')

    return Promise.resolve(
      gfs.createReadStream({ filename: filePath(model, _id, type, file) })
    )
  } catch (e) {
    return Promise.reject(e)
  }
}

export async function removeFile(
  model: M,
  _id: ObjectId,
  type: string,
  file = 'default',
  check = true
): Promise<void> {
  try {
    if (!_id) return Promise.reject('"_id" parameter not found.')

    const doc = await model.findById(_id).exec()
    if (!doc) return Promise.reject(`No document found using _id '${_id}'.`)

    if (check && !await hasFile(model, _id, type, file))
      return Promise.reject('No file to remove.')

    return new Promise<void>((resolve, reject) => {
      const gfs = grid()
      if (!gfs) return reject('GridFS not available.')

      gfs.remove({ filename: filePath(model, _id, type, file) }, err => {
        if (err) return reject(err)

        resolve()
      })
    })
  } catch (e) {
    return Promise.reject(e)
  }
}
