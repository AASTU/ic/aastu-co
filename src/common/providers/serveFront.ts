import { Application } from 'express'
import serveStatic from 'serve-static'

import { problemFormat } from '../util'

export interface IFrontConfig {
  rootDirectory: string
  spaFileName?: string
  gzip?: boolean // todo: add compression
}

export const serveFront = (app: Application, config: IFrontConfig): void => {
  // serve root directory files
  app.use(serveStatic(config.rootDirectory, { index: false }))

  // finally, SPA...
  app.use(async (req, res) => {
    try {
      await new Promise<void>((resolve, reject) => {
        res.sendFile(
          config.spaFileName || 'index.html',
          { root: config.rootDirectory },
          err => {
            if (err) return reject(err)
            resolve()
          }
        )
      })
    } catch (e) {
      res.json(problemFormat(e))
    }
  })
}
