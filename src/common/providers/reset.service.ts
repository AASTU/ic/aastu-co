import { Model, Document } from 'mongoose'
import { stringify } from 'qs'

import { KeyModel } from '../models/key.model'
import { sendEmail } from '../util'

type ModelThatCanSetPassword = Model<
  Document & {
    setPassword(password: string): Promise<void>
  }
>

export async function startPasswordReset(
  model: ModelThatCanSetPassword,
  email: string,
  serverPort = 80
): Promise<void> {
  if (!email) return Promise.reject('Email parameter not found.')

  const doc = await model.findOne({ email: email.toLowerCase() })
  if (!doc) return Promise.reject(`Document not found.`)

  const randomKey = await (KeyModel as any).add(email)

  return sendEmail({
    to: email,
    subject: 'Reset Your Password',
    // todo...
    text:
      `You can reset your password at http://localhost:${serverPort}/reset?` +
      stringify({ view: 'finish', email: email, randomKey: randomKey })
  })
}

export async function finishPasswordReset(
  model: ModelThatCanSetPassword,
  email: string,
  randomKey: string,
  password: string
): Promise<void> {
  if (!email) return Promise.reject('"email" parameter not found.')
  if (!randomKey) return Promise.reject('"randomKey" parameter not found.')
  if (!password) return Promise.reject('"password" parameter not found.')

  if (!await (KeyModel as any).match(email, randomKey))
    return Promise.reject('Password reset key expired or not found.')

  const doc = await model.findOne({ email: email })
  if (!doc) return Promise.reject('Document not found.')

  await doc.setPassword(password)

  return (KeyModel as any).delete(randomKey)
}
