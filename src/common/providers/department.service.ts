import { DepartmentModel } from '../models/department.model'

class DepartmentService {
  static async add(data) {
    try {
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = new DepartmentModel()
      await (DepartmentModel as any).filterFromPublic(data, doc)

      await doc.save()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async get(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await DepartmentModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      return (DepartmentModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async getAll() {
    try {
      const docs = await DepartmentModel.find({})
      if (!docs) return Promise.reject('Documents not found.')

      for (let i = 0; i < docs.length; i++)
        docs[i] = await (DepartmentModel as any).filterForPublic(docs[i])
      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async searchAll(term) {
    try {
      if (!term) return Promise.reject('Search term parameter not found.')

      const docs = await DepartmentModel.find({ $text: { $search: term } })
      if (!docs) return Promise.reject('Documents not found.')

      console.log(docs.length)
      for (let i = 0; i < docs.length; i++)
        docs[i] = await (DepartmentModel as any).filterForPublic(docs[i])

      return Promise.resolve(docs)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async edit(_id, data) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')
      if (!data) return Promise.reject('Data parameter not found.')

      const doc = await DepartmentModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')
      await (DepartmentModel as any).filterFromPublic(data, doc)

      await doc.save()

      return (DepartmentModel as any).filterForPublic(doc)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  static async remove(_id) {
    try {
      if (!_id) return Promise.reject('_id parameter not found.')

      const doc = await DepartmentModel.findById(_id)
      if (!doc) return Promise.reject('Document not found.')

      await doc.remove()

      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

export { DepartmentService }
