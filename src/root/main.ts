import express from 'express'
import { join } from 'path'

import config from './configs/config'
import setup from '../common/configs/express.config'
import mongooseSetup from '../common/configs/mongoose.config'
import { serveFront } from '../common/providers/serveFront'

import '../common/models/admin.model'
import '../common/models/announcement.model'
import '../common/models/company.model'
import '../common/models/department.model'
import '../common/models/graduate.model'
import '../common/models/key.model'
import '../common/models/potential.model'
import '../common/models/vacancy.model'

import { companyRouter } from './routers/company.router'
import { statsRouter } from './routers/stats.router'
import { departmentRouter } from './routers/department.router'

const rootApp = express()
setup(rootApp, config)
mongooseSetup(config)

rootApp.use('/data/company', companyRouter)
rootApp.use('/data/department', departmentRouter)
rootApp.use('/data/stats', statsRouter)

serveFront(rootApp, {
  rootDirectory: join(process.cwd(), 'fronts', 'root', 'build')
})

rootApp.listen(config.dev.port, () =>
  console.log(`Listening on http://localhost:${config.dev.port} (root app)`)
)
