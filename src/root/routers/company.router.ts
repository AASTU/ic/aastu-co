import { Router } from 'express'

import { problemFormat, successFormat } from '../../common/util'
import { CompanyService } from '../../common/providers/company.service'
import { getFile } from '../../common/providers/grid-fs'
import { CompanyModel } from '../../common/models/company.model'

const companyRouter = Router()

// GET /data/company/all
companyRouter.get('/all', async (req, res) => {
  try {
    res.json(successFormat({ companies: await CompanyService.getAll() }))
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/company/:_id
companyRouter.get('/:_id', async (req, res) => {
  try {
    res.json(
      successFormat({ company: await CompanyService.get(req.params._id) })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/company/:_id/picture
companyRouter.get('/:_id/picture', async (req, res) => {
  try {
    ;(await getFile(CompanyModel, req.params._id, 'picture')).pipe(res)
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/company/list/:count/start/0
companyRouter.get('/list/:count/start/0', async (req, res) => {
  try {
    res.json(
      successFormat({ companies: await CompanyService.list(req.params.count) })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

// GET /data/company/list/:count/start/:since
companyRouter.get('/list/:count/start/:since', async (req, res) => {
  try {
    res.json(
      successFormat({
        companies: await CompanyService.list(req.params.count, req.params.since)
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { companyRouter }
