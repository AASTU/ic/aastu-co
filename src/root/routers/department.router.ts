import { Router } from 'express'

import { problemFormat, successFormat } from '../../common/util'
import { DepartmentService } from '../../common/providers/department.service'
const departmentRouter = Router()

// GET /data/department/all
departmentRouter.get('/all', async (req, res) => {
  try {
    res.json(
      successFormat({
        departments: await DepartmentService.getAll()
      })
    )
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { departmentRouter }
