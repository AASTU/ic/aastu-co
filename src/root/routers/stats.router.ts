import { Router } from 'express'

import { problemFormat, successFormat } from '../../common/util'
import { StatsService } from '../../common/providers/stats.service'

const statsRouter = Router()

interface IStats {
  selfEmployed: {
    total: number
    learning: number
    byField: number
  }
  companyEmployed: {
    total: number
    learning: number
    jobType: {
      permanent: number
      contract: number
      intern: number
    }
    jobStatus: {
      fullTime: number
      partTime: number
      perDime: number
    }
  }
  unemployed: {
    total: number
    learning: number
  }
}

// GET /data/stats?gender&department_id&company_id // todo: customize by year, like: /data/stats/:year ...
statsRouter.get('/', async (req, res) => {
  try {
    const statsService = new StatsService(
      req.query.gender,
      req.query.department_id,
      req.query.company_id
    )

    const stats: IStats = {
      selfEmployed: {
        total: await statsService.se_total(),
        learning: await statsService.se_learning(),
        byField: await statsService.se_byField()
      },
      companyEmployed: {
        total: await statsService.ce_total(),
        learning: await statsService.ce_learning(),
        jobType: {
          permanent: await statsService.ce_jobType('PERMANENT'),
          contract: await statsService.ce_jobType('CONTRACT'),
          intern: await statsService.ce_jobType('INTERN')
        },
        jobStatus: {
          fullTime: await statsService.ce_jobStatus('FULL_TIME'),
          partTime: await statsService.ce_jobStatus('PART_TIME'),
          perDime: await statsService.ce_jobStatus('PER_DIME')
        }
      },
      unemployed: {
        total: await statsService.ue_total(),
        learning: await statsService.ue_learning()
      }
    }
    res.json(successFormat({ stats: stats }))
  } catch (e) {
    res.json(problemFormat(e))
  }
})

export { statsRouter }
